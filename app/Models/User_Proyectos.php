<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users_Proyectos extends Model
{
    //
    protected $table = 'usuarios_proyectos';
    protected $filable = ['user_id', 'proyecto_id'];
}
