<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
    protected $table = 'clasificacion';
    protected $primaryKey = 'clasificacion_id';
    public $timestamps = false;
}
