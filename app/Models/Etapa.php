<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $table = 'etapa';
    protected $primaryKey = 'etapa_id';
    public $timestamps = false;
}
