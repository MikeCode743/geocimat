<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyecto';
    protected $primaryKey = 'proyecto_id';
    protected $fillable = ['proyecto_id','nombre','fechainicio','descripcion','longitud','latitud','bitacora'];
    
    public $incrementing = false;
    public $timestamps = false;

    /* RELACION MUCHOS A MUCHOS ENTRE PROYECTOS-USERS */
    public function users(){
    	return $this->belongsToMany(User::class, 'usuarios_proyectos', 'proyecto_id', 'user_id');
    }

    /* RELACION UNO A MUCHOS ENTRE PROYECTOS-USERS */
    public function visitas(){
    	return $this->hasMany(Visita::class, 'proyecto_id', 'proyecto_id');
    }
}
