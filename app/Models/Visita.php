<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $table = 'visita';    
    protected $primaryKey = 'visita_id';
    protected $fillable = ['visita_id','proyecto_id','fecha_inicio','fecha_fin','descripcion', 'etapa_visita'];
    
    public $incrementing = true;
    public $timestamps = false;

    /* RELACION INVERSA UNO A MUCHOS ENTRE PROYECTOS-VISITAS */
    public function visitas(){
    	return $this->belongTo(Proyecto::class,'proyecto_id','proyecto_id');
    }
}
