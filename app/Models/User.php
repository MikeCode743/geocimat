<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
/* CLASE DE NOTIFICACION DE RESTABLECIMIENTO DE CONTRASEÑA */
use App\Notifications\RestablecerContraseniaNotificacion;
//Seguridad
use Illuminate\Support\Facades\Auth;
//Modelos
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $primaryKey = 'user_id';
        
    protected $table = 'user';

    /* METODO PARA ENVIO DE TOKEN PARA RESETEO DE CONTRASEÑA */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new RestablecerContraseniaNotificacion($token));
    }

    /* RELACION UNO A UNO CON USER */
    public function personal(){
        return $this->hasOne(Personal::class, 'user_id', 'user_id');
    }

    /* RELACION DE MUCHOS A MUCHOS CON USER-ROLES */
    public function roles(){
        return $this->belongsToMany(Rol::class, 'usuarios_roles', 'user_id', 'rol_id');
    }

    /* RELACION DE MUCHOS A MUCHOS USER-PROYECTOS */
    public function proyectos(){
        return $this->belongsToMany(Proyecto::class, 'usuarios_proyectos', 'user_id', 'proyecto_id');
    }

    /* LOGICA DE VALIDADCION */
    public function authorizeRoles($roles){
        if($this->hasAnyRole($roles)){
            return true;
        }
        abort(401,'No estas autorizados');
    }

    /* METODO PARA VERIFICAR SI TIENE UN ARREGLO DE ROLES */
    public function hasAnyRole($roles){
        if(is_array($roles)){ 
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    /* METODO PARA VERIFICAR SI TIENE ROLES */
    public function hasRole($role){
        if($this->roles()->where('nombre_corto',$role)->first()){
            return true;
        }
        return false;
    }


    //----------------------------------------------------------------------------------------------------------------------------
    public function permisos()
    {
        return $this->belongsToMany(Permiso::class, 'permisos_usuarios', 'user_id', 'permiso_id');
    }
        // return $this->belongsToMany(Rol::class, 'usuarios_roles', 'user_id', 'rol_id');


    public function getPermisos(){
        $permisos=null;
        //-----------Se obtine los permisos individuales
        $permisosIndividual=$this->permisos;
        foreach($permisosIndividual as $permiso){
            $permisos[$permiso->permiso_id]=$permiso;
        }
        //------------se obtinen los permisos segun los roles
        $roles = $this->roles;
        foreach ($roles as $rol) {
            $permisosByRol=$rol->permisos;
            foreach($permisosByRol as $permiso){
                if(!$permisos){
                    $permisos[$permiso->permiso_id]=$permiso;
                }else{
                    $permisos[$permiso->permiso_id]=$permiso;
                }
            }
        }
        return $permisos;

    }

    public function hasPermisos(... $nombrescorto){
        $permisos=$this->getPermisos();
        if($permisos){
            foreach ($nombrescorto as $nombrecorto) {
                foreach ($permisos as $permiso) {
                    if($permiso->nombre_corto==$nombrecorto){
                        return true;
                    }
                }
            }
        }
    }

    public function proyectoarchivos($id){
        if($this->roles()->where('nombre_corto','admin')->first()){
            return true;
        }
        else
        {
          $proyecto = DB::table('usuarios_proyectos')->where('proyecto_id',$id)->first();
        //   dd($proyecto->user_id);
            if($proyecto->user_id==Auth::user()->user_id){
                return true;
            }
        }
        return false;
    }


}
