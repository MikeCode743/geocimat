<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personal_Especialidad extends Model
{
    protected $table = 'personal_especialidades';
    
    
    protected $fillable = ['personal_id','especialidad_id'];
 
}
