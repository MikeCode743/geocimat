<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'permiso';
    protected $primaryKey = 'permiso_id';
    public $timestamps = false;

    public function roles()
    {
        return $this->belongsToMany('App\Models\Rol', 'permisos_roles', 'permiso_id', 'rol_id');
    }


    public function usuarios()
    {
        return $this->belongsToMany(User::Class, 'permisos_usuarios', 'permiso_id', 'user_id');
    }
}
