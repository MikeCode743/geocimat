<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users_Roles extends Model
{
    //
    protected $table = 'usuarios_roles';
    protected $fillable = ['user_id','rol_id'];

}
