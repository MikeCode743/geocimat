<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
	protected $table = 'rol';
	protected $fillable = ['rol_id', 'nombre'];
	public $timestamps = false;
	protected $primaryKey = 'rol_id';

	/* RELACION DE MUCHOS A MUCHOS CON USERs */
	public function users(){
		return $this->belongsToMany(User::class, 'usuarios_roles', 'rol_id', 'user_id');
	}

	public function permisos()
	{
		return $this->belongsToMany('App\Models\Permiso', 'permisos_roles', 'rol_id', 'permiso_id');
	}
}
