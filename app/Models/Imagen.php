<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $table = 'imagen';
    protected $primaryKey = 'imagen_id';
    protected $fillable = ['nombre', 'ruta_imagen','descripcion'];
    
    public $timestamps = false;
}
