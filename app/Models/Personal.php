<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Especialidad;

class Personal extends Model
{
    protected $table = 'personal';
    protected $primaryKey = 'personal_id';
    protected $fillable = ['personal_id', 'nombres', 'apellidos','telefono'];
    public $timestamps = false;
    /* RELACION INVERSA DE UNO A UNO CON USER */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    /* RELACION DE MUCHOS A MUCHOS CON PERSONAL-ESPECIALIDAD */
    public function especialidadesf(){
        return $this->belongsToMany(Especialidad::class, 'personal_especialidades', 'personal_id', 'especialidad_id');
    }
}
