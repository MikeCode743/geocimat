<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anotacion extends Model
{
    protected $table = 'anotacion';
    protected $primaryKey = 'anotacion_id';
    public $timestamps = false;
}
