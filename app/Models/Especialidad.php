<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = 'especialidad';
    protected $primaryKey = 'especialidad_id';
    public $timestamps = false;
    
    
    protected $fillable = ['nombre'];
    
    /* RELACION DE MUCHOS A MUCHOS PERSONAL-ESPECIALIDAD */
    public function personales(){
        return $this->belongsToMany(Personal::class, 'personal_especialidades', 'especialidad_id', 'personal_id');
    }

}
