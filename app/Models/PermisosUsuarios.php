<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermisosUsuarios extends Model
{
    protected $table = 'permisos_usuarios';
	
	public $timestamps = false;

}
