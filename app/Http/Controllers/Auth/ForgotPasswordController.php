<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;   /* METODO POR DEFECTO PARA ENVIO DE CORREOS PARA REESTABLECER CONTRASEÑA */

    /* METODO PARA VERIFICAR SI ESTA AUTENTIFICADO */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
