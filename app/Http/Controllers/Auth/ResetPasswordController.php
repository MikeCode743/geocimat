<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    
    use ResetsPasswords; /* METODO PARA RESETEO DE CONTRASEÑA POR DEFECTO */

    /* METODO PARA REDIRIGIR */
    protected $redirectTo = '/home';

    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
