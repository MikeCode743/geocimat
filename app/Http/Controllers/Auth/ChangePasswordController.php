<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* REQUEST PERSONZALIZADO */
use App\Http\Requests\Auth\ChangePasswordRequest;
/* CLASE PARA UTILIZAR EL METODO DE ENCRYPTADO */
use Illuminate\Support\Facades\Hash; 
/* MODELO */
use App\Models\User;


class ChangePasswordController extends Controller
{
    /* METODO PARA MOSTRAR FORMULARIO PARA REESTABLECER NUEVA CONTRASEÑA */
    public function ShowForm()
    {
        return view('auth.changepassword');
    }
    /* METODO PARA ACTUALIZAR NUEVA CONTRASEÑA */
    public function ChangePassword(ChangePasswordRequest $request){
        $id=auth()->id();
        $user=User::find($id);
        $user->password=Hash::make($request['password']); /* HASH-> METODO PARA ENCRYPTAR LA CONTRASEÑA */
        $user->save();
        return view('home');
    }

    /* FOMRULARIO PARA CAMBIO DE CONTRASEÑA */
    public function mostrarFormularioCambio()
    {
        return view('auth.cambiarContrasenia');
    }

    /* METODO PARA ACTUALIZAR NUEVA CONTRASEÑA POR DEFECTO*/
    public function cambiarContrasenia(Request $request)
    {
        // dump($request->all());
        $id = auth()->id();
        $user = User::find($id);
        // dump($user->password,$request->actual);

        if (Hash::check($request->actual, $user->password)) {

            $user->password = Hash::make($request->password); /* HASH-> METODO PARA ENCRYPTAR LA CONTRASEÑA */
            $user->save();
            return response()->json(['message' => 'Nueva contraseña almacenada']);
        }
        return response()->json(['message' => 'La contraseña actual no coincide con nuestros registros'], 401);
    }
}
