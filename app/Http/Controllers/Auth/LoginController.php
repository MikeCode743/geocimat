<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers; /* METODOS PARA LA AUTENTIFICACION DE USUARIOS */

    /* METODO PARA REDIRIGIR A LA PAGINA DE INICIO */
    protected $redirectTo = '/home';

    /* METODO PARA AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}