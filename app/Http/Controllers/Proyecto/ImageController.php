<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Imagen;
use App\Models\Proyecto;
use Illuminate\Support\Facades\Storage;

//Seguridad
use Illuminate\Support\Facades\Auth;
/* Modelos */
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function listadoImagen($id, Request $request)
    {

        //dd(DB::table('proyecto')->findOrFail($id));            
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $listado = Imagen::where('proyecto_id', $id)
                ->where('ruta_imagen', 'LIKE', '%imagenes%')
                ->get();
            return view('proyecto.imagenes', [
                'id' => $id,
                'listaImagenes' => $listado,
            ]);
        } else {
            return ('Este proyecto no existe :(');
        }
    }

    public function agregarImagen(Request $request)
    {

        function obtenerNombre($ruta, $nombre, $extension)
        {
            $i = 1;
            $nombreConExtension =  $nombre . "." . $extension;
            $nombreTemporal = $nombre;
            while (Storage::disk('repository')->exists($ruta . '/' . $nombre . "." . $extension)) {
                $nombre = (string)$nombreTemporal . ' (' . $i . ')';
                $nombreConExtension = $nombre . "." . $extension;
                $i++;
            }
            return $nombreConExtension;
        }

        $proyecto = $request->idProyecto;
        $descripcion = $request->descripcion;
        $dirProject = $proyecto . '/imagenes';
        $files = $request->file('file');
        $elementosAgregados = array();
        try {
            if (!empty($files) && !empty($proyecto)) :
                foreach ($files as $file) :
                    $archivoConExtension = $file->getClientOriginalName();
                    $nombreArchivo = pathinfo($archivoConExtension, PATHINFO_FILENAME);
                    $extension = $file->extension();

                    $archivoConExtension = obtenerNombre($dirProject, $nombreArchivo, $extension);
                    $rutaAlmacenada = Storage::disk('repository')->putFileAs($dirProject, $file, $archivoConExtension);
                    $elementosAgregados[] = $rutaAlmacenada;
                    $imagen = new Imagen();
                    /* VERIFICA SI EL PROYECTO EXISTE */
                    if (!Proyecto::find($proyecto)->exists()) {
                        return response()->json([
                            'message' => 'Ooopss! El proyecto al que quiere subir la imagen no existe',
                            'status' => 500
                        ]);
                    }
                    $imagen->proyecto_id = $proyecto;
                    $imagen->ruta_imagen = $rutaAlmacenada;
                    $imagen->descripcion = $descripcion;
                    $imagen->nombre = $archivoConExtension;
                    $imagen->save();
                endforeach;
            endif;
            $message = sizeof($elementosAgregados) . ' Archivo Agregado';
            return response()->json([
                'message' => $message,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Ooopss! Parece que los archivos no se pudieron guardar',
                'status' => 500
            ]);
        }
    }
    public function eliminarImagen(Request $request)
    {
        $proyecto = $request->idProyecto;
        $nombreimagen = $request->nombreimagen;
        $directorioActual = $proyecto . '/imagenes/' . $nombreimagen;
        $papelera = $proyecto . '/papelera/' . $nombreimagen;

        if (!empty($proyecto) && !empty($nombreimagen)) {
            $archivoEnPapelera = Storage::disk('repository')->exists($papelera);
            if ($archivoEnPapelera) {
                Storage::disk('repository')->delete($papelera);
            }

            $fileExists = Storage::disk('repository')->exists($directorioActual);
            if ($fileExists) {

                /* VERIFICA SI LA IMAGEN SE ELIMINO */
                if (!Storage::disk('repository')->move($directorioActual, $papelera)) {
                    return back()->with(["error_eliminar_imagen" => "Hubo un error al eliminar la imagen."]);
                }
                Imagen::where('nombre', $nombreimagen)->where('proyecto_id', $proyecto)
                    ->update(['ruta_imagen' => $papelera]);
                /* LINEA A ESCRIBIR */
                $line = date("Y-m-d H:i:s") . ";" . Auth::user()->name . ";" . $proyecto . ";" . $nombreimagen . ";img;";
                /* AÑADE EL REGISTRO AL LOG DE PAPELERA DEL PROYECTO ACTUAL*/
                $rutalog = '/public/proyectos/' . $proyecto . '/papelera.log';
                (Storage::exists($rutalog)) ? Storage::append($rutalog, $line) : Storage::put($rutalog, $line);
            }
        }
        return back();
    }

    public function papeleraImagen($id)
    {
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $listado = Imagen::where('proyecto_id', $id)
                ->where('ruta_imagen', 'LIKE', '%papelera%')
                ->get();
            return view('proyecto.papeleraimagenes', [
                'id' => $id,
                'listaImagenes' => $listado,
            ]);
        } else {
            return ('Este proyecto no existe :(');
        }
    }

    public function papeleraEliminarImagen(Request $request)
    {
        //dd($request->all());
        $proyecto = $request->idProyecto;
        $nombreimagen = $request->nombreimagen;
        $directorioActual = $proyecto . '/papelera/' . $nombreimagen;

        if (!empty($proyecto) && !empty($nombreimagen)) {

            $fileExists = Storage::disk('repository')->exists($directorioActual);
            if ($fileExists) {
                Storage::disk('repository')->delete($directorioActual);
                Imagen::where('nombre', $nombreimagen)->where('proyecto_id', $proyecto)->delete();
            }
        }
        return back();
    }
    public function restaurarImagen(Request $request)
    {
        // dd($request->all());
        $proyecto = $request->idProyecto;
        $nombreimagen = $request->nombreimagenRestaurar;
        $directorioActual = $proyecto . '/papelera/' . $nombreimagen;
        $papelera = $proyecto . '/imagenes/' . $nombreimagen;

        if (!empty($proyecto) && !empty($nombreimagen)) {
            $archivoEnPapelera = Storage::disk('repository')->exists($papelera);
            if ($archivoEnPapelera) {
                Storage::disk('repository')->delete($papelera);
            }

            $fileExists = Storage::disk('repository')->exists($directorioActual);
            if ($fileExists) {
                $imagen = Imagen::where('nombre', $nombreimagen)->where('proyecto_id', $proyecto)
                    ->update(['ruta_imagen' => $papelera]);
                // ->get();
                // dd($imagen);
                //Retorna bool si se ha movido
                Storage::disk('repository')->move($directorioActual, $papelera);
            }
        }
        return back();
    }
}
