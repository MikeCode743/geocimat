<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Modelos */
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Proyecto;
use App\Models\Archivo;
use App\Models\Grupo;
//Seguridad
use Illuminate\Support\Facades\Auth;

/* Modelos */
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function listaArchivo($id, Request $request)
    {
        // $proyecto = DB::table('proyecto')
        // ->join('archivo', 'proyecto.proyecto_id', '=', 'archivo.proyecto_id')
        // ->join('grupo', 'archivo.grupo_id', '=', 'grupo.grupo_id')
        // ->select('proyecto.*','archivo.*','grupo.*','archivo.nombre AS nombre_archivo')
        //     ->where('proyecto.proyecto_id','=',$id)
        //     ->first();
        // dd($proyecto);
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $listado = Archivo::where('proyecto_id', $id)->join('grupo', 'archivo.grupo_id', '=', 'grupo.grupo_id')
            ->where('ruta_archivo','LIKE','%/archivos/%')
            ->get();
            $grupo = Grupo::get();
            $listaIconos = array("pdf" => "far fa-file-pdf", "txt" => "fas fa-book", 'doc' => 'far fa-file-word', 'docx' => 'far fa-file-word', 'rar'=>'far fa-file-archive', 'zip'=>'far fa-file-archive', 'pptx'=>'far fa-file-powerpoint', 'xlsx'=>'far fa-file-excel');

            return view('proyecto.archivos', [
                'id' => $id,
                'listaArchivos' => $listado,
                'listaGrupo' => $grupo,
                'listaIconos' => $listaIconos,
            ]);
        } else {
            return ('Este proyecto no existe :(');
        }
    }

    public function agregarArchivo(Request $request)
    {

        function obtenerNombre($ruta, $nombre, $extension)
        {
            $i = 1;
            $nombreConExtension =  $nombre . "." . $extension;
            $nombreTemporal = $nombre;
            while (Storage::disk('repository')->exists($ruta . '/' . $nombre . "." . $extension)) {
                $nombre = (string)$nombreTemporal . ' (' . $i . ')';
                $nombreConExtension = $nombre . "." . $extension;
                $i++;
            }
            return $nombreConExtension;
        }

        $proyecto = $request->idProyecto;
        $grupo_id = $request->idGrupo;
        $dirProject = $proyecto . '/archivos';
        $files = $request->file('file');
        $newDirectory = array();
        /* VERIFICA SI EL PROYECTO EXISTE */
        if(!Proyecto::find($proyecto)->exists()){
            return response()->json([
                'message' => 'Ooopss! El proyecto al que quiere subir los archivos no existe',
                'status' => 500
            ]);
        }
        try {            
            if (!empty($files) && !empty($proyecto)) {
                foreach ($files as $file) {
                    $archivoConExtension = $file->getClientOriginalName();
                    $nombreArchivo = pathinfo($archivoConExtension, PATHINFO_FILENAME);
                    $extension = $file->extension();

                    $archivoConExtension = obtenerNombre($dirProject, $nombreArchivo, $extension);
                    $storePath = Storage::disk('repository')->putFileAs($dirProject, $file, $archivoConExtension);
                    //$storePath = Storage::disk('repository')->putFileAs($dirProject, $file, $nombreArchivo.$extension);
                    $newDirectory[] = $storePath;
                    $archivo = new Archivo();
                    
                    $archivo->proyecto_id = $proyecto;
                    $archivo->grupo_id = $grupo_id;
                    $archivo->nombre = $archivoConExtension;
                    $archivo->ruta_archivo = $storePath;
                    $archivo->extension = $file->getClientOriginalExtension();
                    $archivo->save();
                }
            }
            $message = sizeof($newDirectory) . ' Archivo Agregado';
            return response()->json([
                'message' => $message,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Ooopss! Parece que los archivos no se pudieron guardar',
                'status' => 500
            ]);
        }
    }

    public function eliminarArchivo(Request $request)
    {
        

        $proyecto = $request->idProyecto;
        $nombreDocumento = $request->nombreDocumento;
        $directorioActual = $proyecto . '/archivos/' . $nombreDocumento;
        $papelera = $proyecto . '/papelera/' . $nombreDocumento;
        if (!empty($proyecto) && !empty($nombreDocumento)) {
            $fileExists = Storage::disk('repository')->exists($directorioActual);
            if ($fileExists) {

                /* VERIFICA SI EL ARCHIVO SE ELIMINO */
                if(!Storage::disk('repository')->move($directorioActual, $papelera)){
                    return back()->with(["error_eliminar_archivo" => "Hubo un error al eliminar el archivo."]);
                }
                Archivo::where('nombre', $nombreDocumento)->where('proyecto_id',$proyecto)
                ->update(['ruta_archivo' => $papelera]);
                /* LINEA A ESCRIBIR */
                $line = date("Y-m-d H:i:s").";".Auth::user()->name.";".$proyecto.";".$nombreDocumento.";file;";
                /* AÑADE EL REGISTRO AL LOG DE PAPELERA DEL PROYECTO ACTUAL*/
                $rutalog = '/public/proyectos/'.$proyecto.'/papelera.log';     
                (Storage::exists($rutalog)) ? Storage::append($rutalog, $line) : Storage::put($rutalog, $line);
            }
        }
        return back();
    }

    public function papeleraArchivo($id){
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $listado = Archivo::where('proyecto_id', $id
            )->join('grupo', 'archivo.grupo_id', '=', 'grupo.grupo_id')
            ->where('ruta_archivo','LIKE','%/papelera/%')
            ->get();
            // dd($listado);
            $grupo = Grupo::get();
            $listaIconos = array("pdf" => "far fa-file-pdf", "txt" => "fas fa-book", 'doc' => 'far fa-file-word', 'docx' => 'far fa-file-word', 'rar'=>'far fa-file-archive', 'zip'=>'far fa-file-archive', 'pptx'=>'far fa-file-powerpoint', 'xlsx'=>'far fa-file-excel');

            return view('proyecto.papeleraarchivos', [
                'id' => $id,
                'listaArchivos' => $listado,
                'listaGrupo' => $grupo,
                'listaIconos' => $listaIconos,
            ]);
        } else {
            return ('Este proyecto no existe :(');
        }
    }

    public function papeleraEliminarArchivo(Request $request){
       $proyecto = $request->idProyecto;
        $nombreDocumento = $request->nombreDocumentoeliminar;
        $directorioActual = $proyecto . '/papelera/' . $nombreDocumento;

        if (!empty($proyecto) && !empty($nombreDocumento)) {

            $fileExists = Storage::disk('repository')->exists($directorioActual);        
            if ($fileExists) {
                // dd('si existe');
                if(Storage::disk('repository')->delete($directorioActual)){
                    Archivo::where('nombre', $nombreDocumento)->where('proyecto_id',$proyecto )->delete();
                }
            }
        }
        return back(); 
    }
    public function restaurarArchivo(Request $request)
    {
        // dd($request->all());
        $proyecto = $request->idProyecto;
        $nombreDocumento = $request->nombreDocumentorestaurar;
        $directorioActual = $proyecto . '/papelera/' . $nombreDocumento;
        $archivos = $proyecto . '/archivos/' . $nombreDocumento;
        if (!empty($proyecto) && !empty($nombreDocumento)) {
            $fileExists = Storage::disk('repository')->exists($directorioActual);
            if ($fileExists) {
                Archivo::where('nombre', $nombreDocumento)->where('proyecto_id', $proyecto)
                ->update(['ruta_archivo' => $archivos]);
                // ->delete();
                //Retorna bool si se ha movido
                Storage::disk('repository')->move($directorioActual, $archivos);
            }
        }
        return back();
    }


}
