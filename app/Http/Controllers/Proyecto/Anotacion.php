<?php

namespace App\Http\Controllers\Proyecto;

use App\Http\Controllers\Controller;
use App\Http\Resources\anotacion as ResourcesAnotacion;
use App\Models\Anotacion as ModelsAnotacion;
use App\Models\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\Facades\DB;

class anotacion extends Controller
{
    //Anotacion 
    public function ProyectoAnotacion()
    {
        $listadoProyecto = DB::table('proyecto AS p')
            ->join('usuarios_proyectos AS u', 'u.proyecto_id', '=', 'p.proyecto_id')
            ->join('user AS us', 'us.user_id', '=', 'u.user_id')
            ->where('p.estado', '=', true) //Proyectos Activos
            ->orderByDesc('p.fecha_inicio')
            ->select('p.proyecto_id', 'p.nombre', 'p.fecha_inicio', 'us.name', 'us.user_id')
            ->get();
        return response()->json($listadoProyecto);
        dd($listadoProyecto);
    }

    //Anotacion 
    public function verAnotacion()
    {
        return view('anotacion.anotacion');
    }

    public function listarAnotacion(Request $request)
    {

        $idUsuario = Auth::id();
        $id = $request->id;
        Proyecto::findOrFail($id);
        $historial = ModelsAnotacion::where('proyecto_id', $id)
            ->join('user', 'user.user_id', '=', 'anotacion.user_id')
            ->select('anotacion.anotacion_id', 'anotacion.user_id', 'user.name', 'anotacion.fecha', 'anotacion.contenido', 'anotacion.imagen', 'anotacion.editado')
            ->orderBy('anotacion.fecha', 'asc')
            ->get();

        foreach ($historial as $h) {
            if ($h->imagen) {
                $h->contenido = asset($h->contenido);
            }
            if ($idUsuario == $h->user_id) {
                $h->user_id = true;
            } else {
                $h->user_id = false;
            }
        }
        return ResourcesAnotacion::collection($historial);
    }

    public function operacionAnotacion(Request $request)
    {
        $operacion = $request->operacion;
        $proyecto = $request->proyecto;
        $usuario = $request->usuario;
        $contenido = $request->contenido;
        $id_anotacion = $request->id_anotacion;

        //Agregar
        if ($operacion == 0) {
            $anotacion = new ModelsAnotacion;
            $anotacion->proyecto_id = $proyecto;
            $anotacion->user_id = Auth::id();
            $anotacion->contenido = $contenido;
            $anotacion->imagen = false;
            $anotacion->editado = false;
            $anotacion->fecha = date("Y/m/d h:i:sa");
            $anotacion->save();
            return response()->json([
                'mensaje' => 'Comentario agregado',
            ]);
        }

        //Actualizar
        if ($operacion == 1) {
            ModelsAnotacion::where('anotacion_id', $id_anotacion)
                ->update(['anotacion_id' => $id_anotacion, 'contenido' => $contenido, 'editado' => true]);
            return response()->json([
                'mensaje' => 'Comentario actualizado',
            ]);
        }

        //Eliminar
        if ($operacion == 2) {
            ModelsAnotacion::where('anotacion_id', $id_anotacion)->delete();
            return response()->json([
                'mensaje' => 'Comentario eliminado',
            ]);
        }
        if ($operacion == 4) {
            $file = $request->file;
            $archivoConExtension = $file[0]->getClientOriginalName();
            $nombreArchivo = pathinfo($archivoConExtension, PATHINFO_FILENAME);
            $extension = $file[0]->extension();
            $nombreArchivo = $nombreArchivo . '-' . time() . '.' . $extension;
            $storePath = Storage::disk('anotacion')->putFileAs('/', $file[0], $nombreArchivo);
            $storePath = 'storage/anotacion/' . $storePath;

            $anotacion = new ModelsAnotacion;
            $anotacion->proyecto_id = $proyecto;
            $anotacion->user_id = Auth::id();
            $anotacion->contenido = $storePath;
            $anotacion->imagen = true;
            $anotacion->editado = false;
            $anotacion->fecha = date("Y/m/d h:i:sa");
            $anotacion->save();

            return response()->json([
                'mensaje' => 'Imagen almacenada',
            ]);
        }

        return response()->json([
            'mensaje' => 'Oooppss',
        ], 401);
    }
}
