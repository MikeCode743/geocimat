<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;
use App\Http\Requests\Proyecto\ProyectoRequest;
use App\Http\Controllers\Controller;
/* Modelos */
use Illuminate\Support\Facades\DB;
use App\Models\Proyecto;
use App\Models\Clasificacion;
use App\Models\Image;
use App\Models\File;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProyectoController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $clasificaciones = Clasificacion::get();
        return view('proyecto.formulario', [
            'listaclasificacion' => $clasificaciones
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectoRequest $request)
    {
        // function createId($nombre)
        // {
        //     $date = join("", getdate());
        //     $idMD5 = md5($date);
        //     $idMD5 =  substr($idMD5, 0, 5);
        //     $titulo = preg_replace('/\s+/', ' ', $nombre);
        //     $titulo = str_replace(" ", "-", $titulo);
        //     return $titulo . '-' . $idMD5;
        // }

        // $titulo = createId($request->titulo);
        
        // /* COMPRUEBA QUE NO HAYA UN REGISTRO CON EL MISMO TITULO EN PK  PARA CREAR LA CARPETA DEL PROYECTO*/
        // if(Proyecto::where('proyecto_id', '=', $titulo)->exists())
        // {
        //     return back()->withInput()->with(["error_existe" => "Hubo un error al crear proyecto intente enviar los datos nuevamente de nuevo."]);
        // }else
        // {
        //     // Storage::disk('repository')->makeDirectory($titulo);
        //     // $ruta_proyectos = Storage::disk('repository')->getDriver()->getAdapter()->getPathPrefix();
        //     // if(PHP_OS === "WINNT") $ruta_proyectos = str_replace("/","\\",$ruta_proyectos);
        // }

        // $proyecto = new Proyecto;
        // $proyecto->proyecto_id = $titulo;
        // $proyecto->clasificacion_id = $request->clasificacion;
        // $proyecto->nombre = $request->titulo;
        // $proyecto->fecha_inicio = $request->fecha_inicio;
        // $proyecto->descripcion = $request->informacionAdicional;
        // $proyecto->longitud = $request->lng;
        // $proyecto->latitud = $request->lat;
        // $proyecto->ruta = $ruta_proyectos.$titulo;
        // $proyecto->save();

        // /* RELACIONAR EL PROYECTO CON EL USUARIO ACTUAL */
        // $users = User::where('user_id', Auth::user()->user_id)->get();
        
        // foreach ($users as $user) {
        //     $proyecto->users()->attach($user);
        // }

        // return redirect(route('proyecto.lista'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function misproyectos(){
        // $proyectos = Auth::user()->proyectos()->get();
        // return view('proyecto.misproyectos',['proyectos'=>$proyectos]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function lista()
    {
        //     //    dd(DB::table('proyecto')->findOrFail($id)); 
        // $proyecto = DB::table('proyecto')
        // ->join('archivo', 'proyecto.proyecto_id', '=', 'archivo.proyecto_id')
        // ->join('grupo', 'archivo.grupo_id', '=', 'grupo.grupo_id')
        // ->select('proyecto*','archivo.*','grupo.*','archivo.nombre AS nombre_archivo')
        //     ->where('proyecto.proyecto_id','=',$id)
        //     ->first();
        // dd($proyecto);
        // //dd($proyectos);
        // return view('proyecto.listado', [
        //     'proyectos' => $proyectos
        // ]);
    }

    public function bitacora($id)
    {
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $proyecto = Proyecto::where('proyecto_id', $id)->select('nombre', 'bitacora')->get();            
            $nombre = $proyecto->nombre;
            $bitacora = $proyecto->bitacora;
            return view('proyecto.bitacora', [
                'proyecto' => $nombre,
                'proyecto_id' => $id,
                'detalle' => str_replace("'",'"',$bitacora),
            ]);
        } else {
            return false;
        }
    }

    public function editarBitacora(Request $request)
    {
        $idProyecto = $request->idProyecto;
        $descripcion = $request->descripcion;
        $proyecto = Proyecto::find($idProyecto);
        if ($proyecto) {
            try {
               Proyecto::where('proyecto_id', $idProyecto)                    
                    ->update(['bitacora' => $descripcion]);
                return response()->json([
                    'message' => 'Bitácora actualizada',
                ]);
            } catch (\Exception $e) {                
                return response()->json([
                    'message' => 'Ooopss! Parece que hubo un error al actualizar, revisa la petición.',
                    'error' => 'Puede ser la consulta a la base, revisar los campos.',
                ], 500);
            }
        } else {
            return false;
        }
    }
}
