<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;
use App\Http\Requests\Proyecto\ProyectoRequest;
use App\Http\Controllers\Controller;
/* Modelos */
use Illuminate\Support\Facades\DB;
use App\Models\Proyecto;
use App\Models\Clasificacion;
use App\Models\Image;
use App\Models\File;
use App\Models\User;
use App\Models\Etapa;
use App\Models\Grupo;




use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class ProyectoController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $clasificaciones = Clasificacion::get();
        return view('proyecto.formulario', [
            'listaclasificacion' => $clasificaciones
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectoRequest $request)
    {
        function createId($nombre)
        {
            $date = join("", getdate());
            $idMD5 = md5($date);
            $idMD5 =  substr($idMD5, 0, 5);
            $titulo = preg_replace('/\s+/', ' ', $nombre);
            $titulo = str_replace(" ", "-", $titulo);
            return $titulo . '-' . $idMD5;
        }

        $titulo = createId($request->titulo);
        
        /* COMPRUEBA QUE NO HAYA UN REGISTRO CON EL MISMO TITULO EN PK  PARA CREAR LA CARPETA DEL PROYECTO*/
        if(Proyecto::where('proyecto_id', '=', $titulo)->exists() || !Storage::disk('repository')->makeDirectory($titulo))
        {
            return back()->withInput()->with(["error_existe" => "Hubo un error al crear proyecto intente enviar los datos nuevamente de nuevo."]);
        }else
        {
            $ruta_proyectos = Storage::disk('repository')->getDriver()->getAdapter()->getPathPrefix();            
            if(PHP_OS === "WINNT") $ruta_proyectos = str_replace("/","\\",$ruta_proyectos);
        }
        $etapa = Etapa::where('nombre_corto','pre-visita')->exists();


        $proyecto = new Proyecto;
        $proyecto->proyecto_id = $titulo;
        $proyecto->clasificacion_id = $request->clasificacion;
        $proyecto->nombre = $request->titulo;
        $proyecto->fecha_inicio = $request->fecha_inicio;
        $proyecto->descripcion = $request->informacionAdicional;
        $proyecto->longitud = $request->lng;
        $proyecto->latitud = $request->lat;
        $proyecto->ruta = $ruta_proyectos.$titulo;
        $proyecto->etapa_id = $etapa; 
        $proyecto->estado = true;       //Proyecto Activo
        $proyecto->save();

        /* RELACIONAR EL PROYECTO CON EL USUARIO ACTUAL */
        $users = User::where('user_id', Auth::user()->user_id)->get();
        
        foreach ($users as $user) {
            $proyecto->users()->attach($user);
        }

        return redirect(route('proyecto.lista'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function misproyectos(){
        $grupo = Grupo::get();
       

        $proyectos = Auth::user()->proyectos()
            ->where('proyecto.estado', '=', true) //Proyectos Activos
            ->orderBy('proyecto.fecha_inicio', 'desc')
            ->get();
        $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        return view('proyecto.misproyectos',['proyectos'=>$proyectos, 'etapas' => $etapas, 'listaGrupo' => $grupo]);


    }
    
    public function listadocompleto(){
        /* Permiso para el administrador */
        Auth::user()->authorizeRoles(['admin','administrador']);

        $grupo = Grupo::get();


        $proyectos = DB::table('usuarios_proyectos')
        ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
            ->where('proyecto.estado', '=', true) //Proyectos Activos
            ->select('proyecto.proyecto_id','proyecto.nombre AS titulo','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa', 'etapa.etapa_id', 'proyecto.estado', 'etapa.nombre_corto')            
            ->orderBy('proyecto.fecha_inicio', 'desc')
            ->get();
        // dd($proyectos);
        $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        
          return view('proyecto.tablaproyectos', [
            'proyectos' => $proyectos,
            'etapas' => $etapas,
            'listaGrupo' => $grupo
        ]);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detalle($id)
    {
        $clasificaciones = Clasificacion::all();
        $etapas = Etapa::all();


        $proyecto = DB::table('usuarios_proyectos')
        ->join('proyecto', 'usuarios_proyectos.proyecto_id', '=', 'proyecto.proyecto_id')
        ->join('user', 'usuarios_proyectos.user_id', '=', 'user.user_id')
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id' )
        ->join('personal', 'user.user_id', '=', 'personal.user_id' )
        ->select('proyecto.*', 'personal.nombres', 'personal.apellidos', 'clasificacion.nombre AS clasificacion', 'etapa.nombre AS etapa')
        ->where('proyecto.proyecto_id', '=', $id)
        ->where('proyecto.estado', '=', true)
        ->first();
        // dd($proyecto);
        return view('proyecto.detalle', [
                'proyecto' => $proyecto,
                'clasificaciones' => $clasificaciones,
                'etapas' => $etapas
            ]);
    }


        public function lista()
    {
         $proyectos = DB::table('usuarios_proyectos')
        ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
            ->where('proyecto.estado', '=', true) //Proyectos Activos
            ->select('proyecto.*','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa')
            ->orderBy('proyecto.fecha_inicio', 'desc')
            ->get();
        // dd($proyectos->all());        
        return view('proyecto.listado', [
            'proyectos' => $proyectos
        ]);
    }
    
    // public function lista()
    // {
    //     $proyectos = DB::table('proyecto')
    //     ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
    //         ->select('clasificacion.nombre AS clasificacion', 'proyecto.*')
    //         ->orderBy('proyecto.nombre', 'asc')
    //         ->get();
    //     //dd($proyectos);
    //     return view('proyecto.listado', [
    //         'proyectos' => $proyectos
    //     ]);
    // }

    public function bitacora($id)
    {
        $proyecto = Proyecto::find($id);
        if ($proyecto) {
            $proyecto = Proyecto::where('proyecto_id', $id)->select('nombre', 'bitacora')->get();            
            $nombre = $proyecto[0]['nombre'];
            $bitacora = $proyecto[0]['bitacora'];
            return view('proyecto.bitacora', [
                'proyecto' => $nombre,
                'proyecto_id' => $id,
                'detalle' => str_replace("'",'"',$bitacora),
            ]);
        } else {
            return ('Este proyecto no existe :(');
        }
    }

    public function editarBitacora(Request $request)
    {
        
        $idProyecto = $request->idProyecto;
        $descripcion = $request->descripcion;
        $proyecto = Proyecto::find($idProyecto);
        if ($proyecto) {
            try {
               Proyecto::where('proyecto_id', $idProyecto)                    
                    ->update(['bitacora' => $descripcion]);
                return response()->json([
                    'message' => 'Bitácora actualizada',
                ]);
            } catch (\Exception $e) {                
                return response()->json([
                    'message' => 'Ooopss! Parece que hubo un error al actualizar, revisa la petición.',
                    'error' => 'Puede ser la consulta a la base, revisar los campos.',
                ], 500);
            }
        } else {
            return ('Este proyecto no existe :(');
        }
    }


    public function editarProyecto(Request $request, $id){
            // dd($request->all());
        if(Proyecto::findOrFail($id)){
            Proyecto::where('proyecto_id','=',$id)
            ->update([
                'clasificacion_id'=>$request->clasificacion,
                'etapa_id' => $request->etapa,
                'descripcion'=>$request->informacionAdicional
            ]);

        }
        return redirect()->back()->with('message-store', 'Actualizado con exito');
    }

    public function deshabilitar(Request $request, $id)
    {
        if (Proyecto::findOrFail($id)) {
            Proyecto::where('proyecto_id', '=', $id)
            ->update(['estado' => $request->estado,]);
            return response()->json(['message' => 'Proyecto Eliminado y Archivado']);
        }
        return response()->json(['message' => '!pararece que ocurrio un error!'], 401);
    }

    //metodos para cambiar el estado y la etapa de los proyectos, se optimizaran mas adelante
    public function cambiarEtapaMisProyectos(Request $request, $id){        
        //dd($request);
        /*try {
            Proyecto::where('proyecto_id', $id)->get()[0]->update(['etapa_id' => $request->idEtapa]);
            return response()->json(["message" => "Etapa cambiada con exito", "status" => 200]);
        } catch (\Exception $e) {                
            return response()->json(['message' => 'Ooopss! Parece que hubo un error, revisa la petición.', "status" => 500]);
        }*/
        // Proyecto::where('proyecto_id', $id)->update(['etapa_id' => $request->etapa]);
        // // $proyectos = Auth::user()->proyectos()->get();
        // // $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        // // return view('proyecto.misproyectos', ['proyectos'=>$proyectos, 'etapas' => $etapas]);                

        // return redirect()->back()->with('sucess','Cambiado de estado con exito');
    }

    public function cambiarEstadoMisProyectos(Request $request, $id){
        // $estadoTemp = ($request->estado == 1) ? true : false;
        
        // Proyecto::where('proyecto_id', $id)->update(['estado' => $estadoTemp]);
        // $proyectos = Auth::user()->proyectos()->get();
        // $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();        

        // return view('proyecto.misproyectos', ['proyectos'=>$proyectos, 'etapas' => $etapas]);
    }

    public function cambiarEtapaTodosProyectos(Request $request, $id){
        // Proyecto::where('proyecto_id', $id)->update(['etapa_id' => $request->etapa]);

        // $proyectos = DB::table('usuarios_proyectos')
        // ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        // ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        // ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        // ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        // ->join('personal', 'user.user_id', '=', 'personal.user_id')
        //     ->where('proyecto.estado', '=', true) //Proyectos Activos
        // ->select('proyecto.proyecto_id','proyecto.nombre AS titulo','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa', 'etapa.etapa_id', 'proyecto.estado', 'etapa.nombre_corto')            
        // ->orderBy('proyecto.nombre', 'asc')
        // ->get();
        // $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        
        // return view('proyecto.tablaproyectos', [
        //     'proyectos' => $proyectos,
        //     'etapas' => $etapas
        // ]);    
    }

    public function cambiarEstadoTodosProyectos(Request $request, $id){
        // $estadoTemp = ($request->estado == 1) ? true : false;        
        // Proyecto::where('proyecto_id', $id)->update(['estado' => $estadoTemp]);

        // $proyectos = DB::table('usuarios_proyectos')
        // ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        // ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        // ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        // ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        // ->join('personal', 'user.user_id', '=', 'personal.user_id')
        //     ->where('proyecto.estado', '=', true) //Proyectos Activos
        // ->select('proyecto.proyecto_id','proyecto.nombre AS titulo','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa', 'etapa.etapa_id', 'proyecto.estado', 'etapa.nombre_corto')            
        // ->orderBy('proyecto.nombre', 'asc')
        // ->get();
        // $etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        
        // return view('proyecto.tablaproyectos', [
        //     'proyectos' => $proyectos,
        //     'etapas' => $etapas
        // ]);    
    }

    public function mapa(){
        
        $proyectos = DB::table('usuarios_proyectos')
        ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
            ->where('proyecto.estado', '=', true) //Proyectos Activos
            ->select('proyecto.proyecto_id','proyecto.nombre AS titulo','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa', 'etapa.etapa_id', 'proyecto.estado', 'etapa.nombre_corto', 'proyecto.longitud', 'proyecto.latitud', 'proyecto.descripcion')            
            ->orderBy('proyecto.nombre', 'asc')
            ->get();
        // dd($proyectos);
        //$etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        
          return view('proyecto.mapa', [
            'proyectos' => $proyectos,
            //'etapas' => $etapas
        ]);    
    }
    
    public function informes()
    {

        $proyectos = DB::table('usuarios_proyectos')
        ->join('user', 'usuarios_proyectos.user_id', '=', 'user.user_id')
        ->join('proyecto', 'usuarios_proyectos.proyecto_id', '=', 'proyecto.proyecto_id')
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
        ->where('proyecto.estado', '=', true) //Proyectos Activos
        ->select('proyecto.proyecto_id', 'proyecto.nombre AS titulo', 'proyecto.fecha_inicio AS fecha', 'personal.nombres', 'personal.apellidos', 'clasificacion.nombre AS clasificacion', 'etapa.nombre AS etapa', 'proyecto.estado')
            ->orderBy( 'proyecto.fecha_inicio', 'desc')
        ->get()
        ->toArray();


        $etapas = Etapa::all();
        $clasificaciones = Clasificacion::all();

        return view('proyecto.informes',
            compact('proyectos', 'etapas', 'clasificaciones')
        );
    }


    public function estadisticas(){

        $clasificaciones = collect([]);
        $etapas = collect([]);
        $cantProyectos = collect([]);
        $contenido = collect([]);
        $fechas_proyectos = collect([]);
        $hoy = date('Y-m-d');

        $cla = Clasificacion::select('clasificacion_id AS id','nombre')->get();
        $eta = Etapa::select('etapa_id AS id', 'nombre')->get();

        $proyectos = DB::table('proyecto')
            ->select('proyecto.fecha_inicio as fecha','proyecto.estado', 'proyecto.clasificacion_id AS clasificacion', 'proyecto.etapa_id AS etapa')
            ->orderBy('proyecto.fecha_inicio', 'desc')
            ->get();
        // dd($proyectos);


        $archivos = DB::table('archivo')->select('grupo_id')->get();
        $grupos =  Grupo::select('grupo_id AS id', 'coleccion')->get();
        $imagenes = DB::table('imagen')->count();
            
            
        $proyectosActivos = $proyectos->where('estado' ,'=' , true );
        $proyectosCancelados = $proyectos->where('estado', '=', false);


        // $fechas = $proyectos->where('estado', '=' , true )->sortByDesc('fecha');

        $var = collect(['nombre','cantidad']); //Variable temporal para el nombre y la cantidad
        
        foreach ($cla as $c => $clasificacion) {
            $cantidad = $proyectosActivos->where('clasificacion', $clasificacion->id)->count();            
            $filtered = $var->combine([$clasificacion->nombre,$cantidad]);
            $clasificaciones->push($filtered);
        }

        foreach ($eta as $e => $etapa) {
            $cantidad = $proyectosActivos->where('etapa', $etapa->id)->count();
            $filtered = $var->combine([$etapa->nombre, $cantidad]);
            $etapas->push($filtered);
        }

        foreach ($grupos as $g => $grupo) {
            $cantidad = $archivos->where('grupo_id','=', $grupo->id)->count();
            $contenido->push(['nombre'=> $grupo->coleccion, 'cantidad' => $cantidad]);
        }
        $contenido->push(['nombre' => 'Imagenes', 'cantidad' => $imagenes]);


        foreach ($proyectosActivos as $p => $pro) {
            $fecha = $pro->fecha;
            $fechas_proyectos->push(['fecha'=>$fecha]);
        }

        //Numeros de proyectos
        $cantProyectos->push(['nombre' => 'Total de proyectos:','cantidad'=> $proyectos->count()]);
        $cantProyectos->push(['nombre' => 'Cantidad de proyectos activos:', 'cantidad' => $proyectosActivos->count()]);
        $cantProyectos->push(['nombre' => 'Cantidad de proyectos eliminados:', 'cantidad' => $proyectosCancelados->count()]);
        
        $clasificaciones->toArray();
        $etapas->toArray();
        $cantProyectos->toArray();
        $fechas_proyectos->toArray();
        
        // dd($fechas_proyectos);


        return view('proyecto.estadisticas',compact('clasificaciones','etapas','cantProyectos','contenido','fechas_proyectos'));
    }

}
