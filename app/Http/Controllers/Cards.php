<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class Cards extends Controller
{

    public function index(){
        $d1 = array('id' => "519c97a7-d0fb-4728-b867-7ba557ef98c5",
        'type' => '519c97a7-d0fb-4728-b867-7ba557ef98c5',
        'created_at' => 'Mon Jul 20 21:13:47 UTC 2020',
        'company' => 'SoleVenture, Inc.',
        'location' => 'Remote (US)',
        'title' => 'Senior Full Stack Software Developer',
        'description' => '<p>Company Summary:\nPuritan Life Insurance Company of America (Puritan Life) focuses on insurance products and distribution that make sense for today’s world.  We are one of the only cloud-based insurance carriers, constantly pushing insurance into a new generation of technology.  Headquartered in Scottsdale, Arizona, we offer a wide variety of insurance solutions across life and health lines.</p>\n<p>At Puritan Life, we believe that workplace culture is the underlying values, beliefs and principles that serve as the foundation for our company.  Culture includes the methods we use to interact and work with each other. In short, it is how we get things done. It affects not only how people feel while at work, but also how they develop and grow professionally and personally. Our culture is directly connected to the well being of our company and our employees.  We’ve built the success of our company on interdependent relationships cultivated with vulnerability and collaboration, using agility and a win-win outlook to drive for success, and seeking to understand through humility and open mindedness.</p>\n<p>Responsibilities:</p>\n<ul>\n<li>Design, build and support database objects for custom applications</li>\n<li>Design, build and support integrations and data replication</li>\n<li>Model data for analytics and reporting</li>\n<li>Build and manage database backups</li>\n<li>Build and manage archive storage</li>\n<li>Create and maintain database documentation like data dictionaries and ER diagrams</li>\n<li>Translate business requirements into functional specifications for database projects</li>\n<li>Develop, execute and document testing procedures for database projects</li>\n<li>Work as a member of cross functional teams to help design solutions</li>\n</ul>\n<p>Requirements:\nThis candidate will be instrumental in helping build an insurance group from the ground up.  Loving customer service, really cool technology, and solving problems is a must.  To thrive in our fast paced, start-up like environment, the ideal candidate will need to have:</p>\n<ul>\n<li>5+ years database development experience</li>\n<li>2+ years Microsoft Azure experience</li>\n<li>Automation experience, including Logic Apps and Runbooks</li>\n<li>ETL tools experience, including SQL Service Integration Services and Azure Data Factory</li>\n<li>Data modeling knowledge and skills, including SQL Server Analysis Services</li>\n<li>Insurance industry experience</li>\n</ul>\n');   
        
        $d2 = array('id' => "519c97a7-d0fb-4728-b867-7ba557ef98c5",
        'type' => '519c97a7-d0fb-4728-b867-7ba557ef98c5',
        'created_at' => 'Mon Jul 20 21:13:47 UTC 2020',
        'company' => 'SoleVenture, Inc.',
        'location' => 'Remote (US)',
        'title' => 'Senior Full Stack Software Developer',
        'description' => '<p>Company Summary:\nPuritan Life Insurance Company of America (Puritan Life) focuses on insurance products and distribution that make sense for today’s world.  We are one of the only cloud-based insurance carriers, constantly pushing insurance into a new generation of technology.  Headquartered in Scottsdale, Arizona, we offer a wide variety of insurance solutions across life and health lines.</p>\n<p>At Puritan Life, we believe that workplace culture is the underlying values, beliefs and principles that serve as the foundation for our company.  Culture includes the methods we use to interact and work with each other. In short, it is how we get things done. It affects not only how people feel while at work, but also how they develop and grow professionally and personally. Our culture is directly connected to the well being of our company and our employees.  We’ve built the success of our company on interdependent relationships cultivated with vulnerability and collaboration, using agility and a win-win outlook to drive for success, and seeking to understand through humility and open mindedness.</p>\n<p>Responsibilities:</p>\n<ul>\n<li>Design, build and support database objects for custom applications</li>\n<li>Design, build and support integrations and data replication</li>\n<li>Model data for analytics and reporting</li>\n<li>Build and manage database backups</li>\n<li>Build and manage archive storage</li>\n<li>Create and maintain database documentation like data dictionaries and ER diagrams</li>\n<li>Translate business requirements into functional specifications for database projects</li>\n<li>Develop, execute and document testing procedures for database projects</li>\n<li>Work as a member of cross functional teams to help design solutions</li>\n</ul>\n<p>Requirements:\nThis candidate will be instrumental in helping build an insurance group from the ground up.  Loving customer service, really cool technology, and solving problems is a must.  To thrive in our fast paced, start-up like environment, the ideal candidate will need to have:</p>\n<ul>\n<li>5+ years database development experience</li>\n<li>2+ years Microsoft Azure experience</li>\n<li>Automation experience, including Logic Apps and Runbooks</li>\n<li>ETL tools experience, including SQL Service Integration Services and Azure Data Factory</li>\n<li>Data modeling knowledge and skills, including SQL Server Analysis Services</li>\n<li>Insurance industry experience</li>\n</ul>\n');

        $data = array('1'=>$d1, '2'=>$d2);

        return view('cards.base')->with('data',$data);        
    }

    public function index2(){
        return view('cards.formularioVisita');  
    }

    public function index3(){ 
        return view('cards.formProject');       
    }
    public function index4(){ 
        return view('cards.listProject');       
    }
    public function index5(){ 
        return view('cards.projectFile');       
    }
    
    public function index6(){ 
        return view('cards.projectPhoto');       
    }


}
