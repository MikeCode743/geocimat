<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* METODO INICIO */
    public function index()
    {
        if(Auth::user()->created_at==Auth::user()->updated_at){
            return redirect('password/change');
        }
        return redirect()->action('Proyecto\ProyectoController@lista');
    }
}
