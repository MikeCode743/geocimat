<?php

namespace App\Http\Controllers\Mapa;

use Illuminate\Http\Request;
use App\Http\Requests\Proyecto\ProyectoRequest;
use App\Http\Controllers\Controller;
/* Modelos */
use Illuminate\Support\Facades\DB;
use App\Models\Proyecto;
use App\Models\Clasificacion;
use App\Models\Image;
use App\Models\File;
use App\Models\User;
use App\Models\Etapa;
use App\Models\Grupo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class MapaController extends Controller
{
    
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    

    public function mapa(){
        
        $proyectos = DB::table('usuarios_proyectos')
        ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
            ->select('proyecto.proyecto_id','proyecto.nombre AS titulo','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa', 'etapa.etapa_id', 'proyecto.estado', 'etapa.nombre_corto', 'proyecto.longitud', 'proyecto.latitud', 'proyecto.descripcion')            
            ->orderBy('proyecto.nombre', 'asc')
            ->get();
        // dd($proyectos);
        //$etapas = DB::table('etapa')->select('etapa_id', 'nombre_corto')->get();
        
          return view('proyecto.mapapublico', [
            'proyectos' => $proyectos,
            //'etapas' => $etapas
        ]);    
    }

    /*public function filtro($fecha)
    {
         $proyectos = DB::table('usuarios_proyectos')
        ->join('proyecto','usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')        
        ->join('user','usuarios_proyectos.user_id','=','user.user_id')
        ->join('clasificacion', 'clasificacion.clasificacion_id', '=', 'proyecto.clasificacion_id')
        ->join('etapa', 'etapa.etapa_id', '=', 'proyecto.etapa_id')
        ->join('personal', 'user.user_id', '=', 'personal.user_id')
            ->select('proyecto.*','personal.nombres','personal.apellidos','clasificacion.nombre AS clasificacion','etapa.nombre AS etapa')
            ->orderBy('proyecto.nombre', 'asc')
            ->get();
        // dd($proyectos->all());        
        return view('proyecto.listadofecha', [
            'proyectos' => $proyectos
        ]);
    }*/
}
