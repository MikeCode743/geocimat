<?php

namespace App\Http\Controllers\Personal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* REQUEST PERSONALIZADO */
use App\Http\Requests\Personal\PersonalRequest;
/* MODELOS */
use App\Models\User;
use App\Models\Rol;
use App\Models\Personal;
use App\Models\Especialidad;
/* ENCRYPTACION */
use Illuminate\Support\Facades\Hash;
/*  */
use Validator;

/* CamelCase */
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;



class PersonalController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /* METODO INDEX - PAGINA DE INICIO */
    public function index(Request $request)
    {
        Auth::user()->authorizeRoles(['admin','administrador']);
        /* VERIFICAR SI ES ADMINISTRADOR O DIRECTOR */
        // $request->user()->authorizeRoles(['Admin','Director']);
        /* LISTAR PERSONAL */
        $integrantes = Personal::where('user_id', '!=', auth()->id())->get();
        
        return view('admin.personal.home', ['integrantes' => $integrantes]);
    }

    /* METODO PARA FORMULARIO DE REGISTRO DE PERSONAL */
    public function create()
    {
        /* VARIABLE PARA ACTIVAR EL FORMULARIO DE CREAR */
        $formulario_crear = true;
        /* DESPLEGAR LISTAS */
        $roles = Rol::get();
        $especialidades = Especialidad::get();
        /* VARIABLE A RETORNAR */
        return view('admin.personal.create-personal',[
            'roles' => $roles,
            'especialidades' => $especialidades,
            'formulario_crear' => $formulario_crear
         ]);
    }
    /* METODO PARA REGISTRAR */
    public function store(PersonalRequest $request)
    {   
        $email = Str::lower($request->email);
        $name = Str::title($request->name);
        /* COMPRUEBA QUE EL EMAIL SEA UNICO */
        if(User::select('email')->where('email', $email)->exists()){
            return back()->withInput()->with(["error_email_existe" => "El email ingresado ya existe, porfavor ingrese otro."]);
        }
        /* COMPRUEBA QUE EL USERNAME SEA UNICO */
        if(User::select('name')->where('name', $name)->exists()){
            return back()->withInput()->with(["error_name_existe" => "El nombre de usuario ingresado ya existe, porfavor ingrese otro."]);
        }
        
        /* CREAR EL USUARIO */
        $usuario = new User;
        $usuario->name = $name;
        $usuario->email = $email;
        $usuario->password = Hash::make($request->password);
        $usuario->save();
        
        $nombres = Str::title($request->nombres);
        $apellidos = Str::title($request->apellidos);
        /* CREAR EL PERSONAL */
        $usuario->personal()->create([
             'nombres' => $nombres,
             'apellidos' => $apellidos,
             'telefono' => $request->telefono,
         ]);
        /* ASIGNAR ROLES AL USUARIO */ 
        $personal= Personal::where('user_id',$usuario->user_id)->first();
        if($request->especialidades){            
            foreach ($request->especialidades as $esp) {
                $personal->especialidadesf()->attach($esp);
            }
            /* ASIGNAR ROLES  */
            if($request->roles){            
                foreach ($request->roles as $rol) {
                    $usuario->roles()->attach($rol);
                }    
            }
        $usuario->personal_id=$personal->personal_id;
        $usuario->save();

        
        
        return redirect(route('personal.home'));
        }
    }

    /* MOSTRAR PERSONAL */
    public function show($id)
    {
        Auth::user()->authorizeRoles(['admin','administrador']);
        /* BUSQUEDA DEL PERSONAL Y USUARIO */
        $integrante = Personal::find($id);
        $usuario = User::find($integrante->user_id);            
        /* LISTAS DE DETALLES ASIGNADAS AL PERSONAL */
        $especialidades = $integrante->especialidadesf()->get();
        $roles = $usuario->roles()->get();
        
        /* RETORNAR VARIABLES */
        return view('admin.personal.show-personal', [
                'usuario' => $usuario,
                'integrante' => $integrante,
                'especialidades' => $especialidades,
                'roles' => $roles
        ]);
    }

    /* METODO PARA EDITAR EL PERSONAL */
    public function edit($id)
    {
        /* VARIABLE PARA ACTIVAR FORMULARIO DE EDICION */
        $formulario_crear = false;
        /* BUSQUEDA DEL PERSONAL Y USUARIO */
        $integrante = Personal::find($id);
        $usuario = User::find($integrante->user_id);
        /* DESPLEGAR LISTAS */
        $roles = Rol::get();
        $especialidades= Especialidad::get();
        /* LISTAS DE DETALLES ASIGNADAS AL PERSONAL */
        $user_roles = $usuario->roles()->get();
        $personal_especialidades = $integrante->especialidadesf()->get();

        return view('admin.personal.update-personal',[
            'usuario' => $usuario,
            'roles' => $roles,
            'especialidades'=>$especialidades,
            'user_roles' => $user_roles,
            'personal_especialidades' => $personal_especialidades,
            'integrante' => $integrante,
            'formulario_crear' => $formulario_crear
         ]);
    }
    /* METODO PARA ACTUALIZAR DATOS DEL PERSONAL */
    public function update(PersonalRequest $request, $id)
    {   
        /* BUSQUEDA Y ACTUALIZACION DE NUEVO DATOS DEL PERSONAL */
        $integrante = Personal::find($id);
        $nombres = Str::title($request->nombres);
        $apellidos = Str::title($request->apellidos);
        $email = Str::lower($request->email);

        /* COMPRUEBA QUE EL EMAIL NO SE REPITA A MENOS QUE SEA DEL MISMO USUARIO */
        $email_cmp = User::select('user_id','email')->where('email', $request->email)->first();        
        if($email_cmp && $email_cmp->user_id != $integrante->user_id){
            return back()->withInput()->with(["error_email_existe" => "El email ingresado ya existe, porfavor ingrese otro."]);
        }
        /* COMPRUEBA QUE EL USERNAME NO SE REPITA A MENOS QUE SEA DEL MISMO USUARIO */
        $name_cmp = User::select('user_id','name')->where('name', Str::title($request->name))->first();
        if($name_cmp && $name_cmp->user_id != $integrante->user_id){
            return back()->withInput()->with(["error_name_existe" => "El nombre de usuario ingresado ya existe, porfavor ingrese otro."]);
        }

        $integrante->nombres = $nombres;
        $integrante->apellidos = $apellidos;
        $integrante->telefono = $request->telefono;
        $integrante->save();
        /* ACTUALIZACION DE DATOS DEL USUARIO */
        $usuario = User::find($integrante->user_id);
        $usuario->name = Str::title($request->name);
        /* VERIFICAR SI SUCEDIERON CAMBIOS EN EL CORREO Y CONTRASEÑA */
        if($request->email != $usuario->email){

            $usuario->email = $email;
        }
        if($request->password != "000000"){
            $usuario->password = Hash::make($request->password);
        }
        $usuario->save();
        /* NUEVA LISTAS DE DETALLES */        
        User::find($integrante->user_id)->roles()->sync($request->roles);
        Personal::find($integrante->personal_id)->especialidadesf()->sync($request->especialidades);
        return redirect(route('personal.home'));
    }

    /* METODO PARA ELIMINAR */
    public function destroy($id)
    {
        /* BORRA LOS DATOS DEL PERSONAL Y USUARIOS EN CASCADA */
        Personal::where('personal_id',$id)->delete();
        User::where('personal_id',$id)->delete();

        return redirect(route('personal.home'));
    }
}
