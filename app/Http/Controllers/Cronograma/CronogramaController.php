<?php

namespace App\Http\Controllers\Cronograma;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\Models\Etapa;
use App\Models\Proyecto;
use App\Models\Visita;

class CronogramaController extends Controller
{
    //funcion que corrige la fecha generada por fullcalendar para poder almacenarla en la db
    function generarFechaFinControl($datosVisitas, $suma=true){
        $fechaFinControl = [];
        $operacion = ($suma) ? "+ 1 days" : "- 1 days";

        for ($i = 0; $i < sizeof($datosVisitas); $i++) {
            $fechaFinControl[$i] = date('Y-m-d', strtotime($datosVisitas[$i]->fecha_fin.$operacion));
        }
        return $fechaFinControl;
    }

    function index(){
        $inicioAux = "";
        $finAux = "";
        $datosVisitas = [];
        $fechaFinControl = [];

        if(Auth::user()->hasRole('admin','administrador')){
            $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->get();
        }else{
            if(Auth::user()->hasAnyRole('investigador')){
                $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->where('user.user_id','=', Auth::user()->user_id)                    
                    ->get();
            }
        }

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.cronograma', ['datosVisitas' => json_encode($datosVisitas), 'fechaFinControl' => json_encode($fechaFinControl)]);
    }

    function mostrarTerminadas(){        
        Auth::user()->authorizeRoles(['admin']);
        $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->where('visita.etapa_visita', '=', 'Terminada')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->get();

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.cronograma', ['datosVisitas' => json_encode($datosVisitas), 'fechaFinControl' => json_encode($fechaFinControl)]);
    }

    function mostrarEnProceso(){        
        Auth::user()->authorizeRoles(['admin','administrador']);
        $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->where('visita.etapa_visita', '=', 'En Proceso')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->get();

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.cronograma', ['datosVisitas' => json_encode($datosVisitas), 'fechaFinControl' => json_encode($fechaFinControl)]);
    }

    function mostrarPorVisitar(){
        Auth::user()->authorizeRoles(['admin', 'administrador']);
        $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->where('visita.etapa_visita', '=', 'A Visitar')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->get();

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.cronograma', ['datosVisitas' => json_encode($datosVisitas), 'fechaFinControl' => json_encode($fechaFinControl)]);
    }

    function mostrarCanceladas(){
        Auth::user()->authorizeRoles(['admin', 'administrador']);
        $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->join('clasificacion', 'clasificacion.clasificacion_id','=','proyecto.clasificacion_id')
                    ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                    ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                    ->join('personal', 'personal.user_id','=','user.user_id')
                    ->where('visita.etapa_visita', '=', 'Cancelada')
                    ->select('visita.*', 'proyecto.estado', 'proyecto.nombre', 'proyecto.descripcion as descripcion_proyecto', 'etapa.nombre as etapa_proyecto', 'clasificacion.nombre as clasificacion', 'personal.nombres', 'personal.apellidos')
                    ->get();

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.cronograma', ['datosVisitas' => json_encode($datosVisitas), 'fechaFinControl' => json_encode($fechaFinControl)]);
    }

    function planificarVisitas(){
        Auth::user()->authorizeRoles(['admin','administrador','investigador']);
        $datosVisitas = [];
        $fechaFinControl = [];
        
        if(Auth::user()->hasAnyRole(['admin', 'administrador'])){
            $datosVisitas = DB::table('visita')
                    ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                    ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                    ->select('visita.*')
                    ->get();            
            $proyectos = Proyecto::where('estado', '=', true)->get(); //Proyectos Activos
        }else{
            if(Auth::user()->hasAnyRole('investigador')){
                $datosVisitas = DB::table('visita')
                        ->join('proyecto', 'proyecto.proyecto_id','=','visita.proyecto_id')
                        ->join('etapa', 'etapa.etapa_id','=','proyecto.etapa_id')
                        ->join('usuarios_proyectos', 'usuarios_proyectos.proyecto_id','=','proyecto.proyecto_id')
                        ->join('user', 'user.user_id','=','usuarios_proyectos.user_id')
                        //->join('usuarios_roles', 'usuarios_roles.user_id','=','user.user_id')
                        //->join('rol', 'rol.rol_id','=','usuarios_roles.rol_id')
                        ->select('visita.*', 'proyecto.estado', 'etapa.nombre_corto')
                        ->where('user.user_id','=', Auth::user()->user_id)
                        ->get();
                $proyectos = Auth::user()->proyectos()->where('proyecto.estado', '=', true)->get();            
            }
        }
        //para corregir la fecha para que se muestre bien en el calendario
        /*$finTemp = "";
        for ($i = 0; $i < sizeof($datosVisitas); $i++) {
            $finTemp = date('Y-m-d', strtotime($datosVisitas[$i]->fecha_fin."+ 1 days"));
            $datosVisitas[$i]->fecha_fin = $finTemp;
        }*/

        $fechaFinControl = $this->generarFechaFinControl($datosVisitas);

        return view('cronograma.planificar-visitas', [            
            'proyectos' => $proyectos,
            'datosVisitas' => json_encode($datosVisitas),
            'fechaFinControl' => json_encode($fechaFinControl)
        ]);
    }

    //se encarga de agregar las nuevas visitas
    function agregarVisitas(Request $request){
        $visitas = $request->json()->all();
        //eventos nuevos a agregar
        foreach($visitas["eventosCrear"] as $v) {
            //para corregir la fecha de fin
            $fechaFin = ($v["end"] == null) ? $v["start"] : date('Y-m-d', strtotime($v["end"]."- 1 days"));
            
            Visita::create([
                "proyecto_id" => explode("|", $v["idTemporal"])[0],
                "fecha_inicio" => $v["start"],
                "fecha_fin" => $fechaFin,
                "descripcion" => $v["descripcion_visita"],
                "etapa_visita" => ($v["etapa_visita"]) ? $v["etapa_visita"] : "A Visitar"
            ]);
        }

        //eventos ya creados a modificar
        foreach ($visitas["eventosModificar"] as $v) {
            //para corregir la fecha de fin
            $fechaFin = ($v["end"] == null) ? $v["start"] : date('Y-m-d', strtotime($v["end"]."- 1 days"));

            DB::table('visita')->where('visita_id', $v["idTemporal"])->update([
                //"pro"
                "proyecto_id" => explode("|", $v["title"])[0],
                "fecha_inicio" => $v["start"],
                "fecha_fin" => $fechaFin,
                "descripcion" => $v["descripcion_visita"],
                "etapa_visita" => $v["etapa_visita"]
            ]);
        }

        return response()->json(["menssage" => "guardo correctamente"]);
    }
}
