<?php

namespace App\Http\Controllers\Admin\Grupo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* Modelos */
use App\Models\Grupo; 
/* Request */
use App\Http\Requests\Admin\GrupoRequest;
use App\Http\Requests\Admin\Base\EditRequest;

/* TitleCase */
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;


class GrupoController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin','administrador']);
        $grupos = Grupo::get();
        return view('admin.grupo', [
            'grupos' => $grupos,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrupoRequest $request)
    {
            $grupo = new Grupo;
            $grupo->nombre_corto = Str::lower(Str::slug($request->nombre_corto,'-'));
            $grupo->coleccion = Str::title($request->nombre);
            $grupo->descripcion = $request->descripcion;
            /* METODO TRY-CATCH PARA ERRORES AL GUARDAR */
            try {                        
            $grupo->save();
                return redirect()->back()->with('message-store', 'Guardado con exito');
            } catch (\Illuminate\Database\QueryException $ex) {
                return redirect()->back()->with('message-store', 'Error al registrar valor');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, $id)
    {
         try {
            $affectedRows = Grupo::where('grupo_id', $id)->update([
                'nombre_corto'=> Str::lower(Str::slug($request->edit_nombre_corto,'-')),
                'coleccion' => Str::title($request->edit_nombre),
                'descripcion'=> $request->edit_descripcion
            ]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-info', 'No se pudo actualizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             try {
            /* METODO PARA BUSQUEDA */
            $grupo = Grupo::findOrFail($id);
            if ($grupo) {
                Grupo::where('grupo_id', $id)->delete();

                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-info', 'Error al eliminar');
        }
    }
}
