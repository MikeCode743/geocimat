<?php

namespace App\Http\Controllers\Admin\Permisos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Modelos
use App\Models\Permiso;

/* Request */
use App\Http\Requests\Admin\Permiso\PermisoRequest;
use App\Http\Requests\Admin\Base\EditRequest;

/* TitleCase */
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;

class PermisoController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }

     /* METODO PARA LA PAGINA DE INICIO */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin']);
        $permisos = Permiso::get();
        return view('admin.permiso', [
            'permisos' => $permisos,
            ]);
    }
    /* METODO PARA MOSTRAR */
    public function show($id){
    }
    /* METODO DE REGISTRO */
    public function store(PermisoRequest $request){
        // dd($request->all());
        $permiso = new Permiso;
        $permiso->nombre_corto = Str::lower(Str::slug($request->nombre_corto,'-'));
        $permiso->nombre = Str::title($request->nombre);
        $permiso->descripcion = $request->descripcion;
        // dd($permiso);
        /* METODO TRY-CATCH PARA ERRORES AL GUARDAR */
        try {                        
            $permiso->save();
            return redirect()->back()->with('message-store', 'Guardado con exito');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with('message-info', 'Error al registrar valor');
        }
    }
    /* METODO DE ACTUALIZACION */
    public function update(EditRequest $request,$id){
        // dd($request->all());
        /* METODO TRY-CATCH PARA ERRORES AL ACTUALIZAR */
        try {
            $affectedRows = permiso::where('permiso_id', $id)->update([
                'nombre_corto'=> Str::lower(Str::slug($request->edit_nombre_corto,'-')),
                'nombre' => Str::title($request->edit_nombre),
                'descripcion' => $request->edit_descripcion]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-info', 'No se pudo actualizar'); 
        }
    }
    /* METODO DE ELIMINACION */
    public function destroy($id){
        try {
            /* METODO PARA BUSQUEDA */
            $permiso = permiso::findOrFail($id);
            if ($permiso) {
                $permiso->roles()->detach();
                Permiso::where('permiso_id', $id)->delete();
                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-info', 'Error al eliminar');
        }
    }

}
