<?php

namespace App\Http\Controllers\Admin\Permisos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Modelos
use App\Models\Permiso;


class PermisoControllerr extends Controller
{
     /* METODO PARA LA PAGINA DE INICIO */
    public function index()
    {
        $permisos = Permiso::get();
        return view('admin.permiso', [
            'permisos' => $permisos,
            ]);
    }
    /* METODO PARA MOSTRAR */
    public function show($id){
    }
    /* METODO DE REGISTRO */
    public function store(RolRequest $request){
        $permiso = new Permiso;
        $permiso->short_name = $request->nombrecorto;
        $permiso->nombre = $request->nombre;
        $permiso->descripcion = $request->descripcion;

        /* METODO TRY-CATCH PARA ERRORES AL GUARDAR */
        try {                        
            $permiso->save();
            return redirect()->back()->with('message-store', 'Guardado con exito');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with('message-store', 'Error al registrar valor');
        }
    }
    /* METODO DE ACTUALIZACION */
    public function update(RolRequest $request,$id){
        /* METODO TRY-CATCH PARA ERRORES AL ACTUALIZAR */
        try {
            $affectedRows = permiso::where('id', $id)->update(['nombre' => $request->edit_nombre]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-store', 'No se pudo actualizar');
        }
    }
    /* METODO DE ELIMINACION */
    public function destroy($id){
        try {
            /* METODO PARA BUSQUEDA */
            $permiso = permiso::findOrFail($id);
            if ($permiso) {
                $users_roles = Users_Roles::where('rol_id',$id)->delete();               
                permiso::where('id', $id)->delete();
                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-store', 'Error al eliminar');
        }
    }

}
