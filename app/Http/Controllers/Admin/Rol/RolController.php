<?php

namespace App\Http\Controllers\Admin\Rol;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* MODELOS */
use App\Models\Rol;
use App\Models\Users_Roles; //----------------------------------
/* METODO REQUEST PERSONALIZADO */
use App\Http\Requests\Admin\RolRequest;
use App\Http\Requests\Admin\Base\EditRequest;
//String
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;


class RolController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /* METODO PARA LA PAGINA DE INICIO */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin']);
        $roles = Rol::get();
        return view('admin.rol', [
            'roles' => $roles,
            ]);
    }
    /* METODO PARA MOSTRAR */
    public function show($id){
    }
    /* METODO DE REGISTRO */
    public function store(RolRequest $request){

        $rol = new Rol;
        $rol->nombre_corto = Str::lower(Str::slug($request->nombre_corto,'-'));
        $rol->nombre = Str::title($request->nombre);
        $rol->descripcion = $request->descripcion;
        /* METODO TRY-CATCH PARA ERRORES AL GUARDAR */
        try {                        
            $rol->save();
            return redirect()->back()->with('message-store', 'Guardado con exito');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with('message-info', 'Error al registrar valor');
        }
    }
    /* METODO DE ACTUALIZACION */
    public function update($id,EditRequest $request){
        /* METODO TRY-CATCH PARA ERRORES AL ACTUALIZAR */


        try {
            $affectedRows = Rol::where('rol_id', $id)->update([
                'nombre_corto'=> Str::lower(Str::slug($request->edit_nombre_corto,'-')),
                'nombre' => Str::title($request->edit_nombre),
                'descripcion'=> $request->edit_descripcion
            ]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-info', 'No se pudo actualizar');
        }
    }
    /* METODO DE ELIMINACION */
    public function destroy($id){
        try {
            /* METODO PARA BUSQUEDA */
            $rol = Rol::findOrFail($id);
            if ($rol) {
                $rol->permisos()->detach();
                Rol::where('rol_id', $id)->delete();

                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-info', 'Error al eliminar');
        }
    }

}
