<?php

namespace App\Http\Controllers\Admin\Rol;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Modelos
use Illuminate\Support\Facades\DB;
use App\Models\PermisosRoles;
use App\Models\Permiso;
use App\Models\Rol;
//Seguridad
use Illuminate\Support\Facades\Auth;

class PermisosRolesController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin']);
        $roles = Rol::get();
        $permisos = Permiso::get();

        $permisos_roles = DB::table('permisos_roles')
            ->join('permiso', 'permiso.permiso_id', '=', 'permisos_roles.permiso_id')
            ->join('rol', 'rol.rol_id', '=', 'permisos_roles.rol_id')
            ->select('rol.rol_id','permiso.permiso_id','rol.nombre AS rol_nombre','permiso.nombre_corto AS permiso_nombre_corto','permiso.nombre AS permiso_nombre','permiso.descripcion AS permiso_descripcion')
            ->get();
        return view('admin.permisosroles.home', [
            'permisos' => $permisos,
            'roles'=>$roles,
            'permisos_roles'=>$permisos_roles
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol= Rol::find($request->rol);

        if($request->permisos){     
            foreach ($request->permisos as $permiso) {
                $iguales=DB::table('permisos_roles')->where([['rol_id', '=', $rol->rol_id],['permiso_id', '=', $permiso],])->first();
                if(!$iguales){
                    $rol->permisos()->attach($permiso);
                }              
            }
        }
        return redirect()->back()->with('message-store', 'Guardado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($r,$p)
    {
        $rol= Rol::findOrFail($r);
        $permiso = Permiso::findOrFail($p);
        $rol->permisos()->detach($permiso);
        return redirect()->back()->with('message-delete', 'Eliminado');
    }
}
