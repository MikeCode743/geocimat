<?php

namespace App\Http\Controllers\Admin\Clasificacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* Modelos */
use App\Models\Clasificacion;
/* Request */
use App\Http\Requests\Admin\ClasificacionRequest;
use App\Http\Requests\Admin\Base\EditRequest;
/* TitleCase */
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;

class ClasificacionController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin','administrador']);
        $clasificaciones = Clasificacion::get();
        return view('admin.clasificacion', [
            'clasificaciones' => $clasificaciones,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClasificacionRequest $request)
    {
        $clasificacion = new Clasificacion;
        $clasificacion->nombre_corto = Str::lower(Str::slug($request->nombre_corto,'-'));
        $clasificacion->nombre = Str::title($request->nombre);
        $clasificacion->descripcion = $request->descripcion;
        /* METODO TRY-CATCH PARA ERRORES AL GUARDAR */
        try {                        
        $clasificacion->save();
        return redirect()->back()->with('message-store', 'Guardado con exito');
        } catch (\Illuminate\Database\QueryException $ex) {
        return redirect()->back()->with('message-info', 'Error al registrar valor');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, $id)
    {
        try {
            $affectedRows = Clasificacion::where('clasificacion_id', $id)->update([
                'nombre_corto'=> Str::lower(Str::slug($request->edit_nombre_corto,'-')),
                'nombre' => Str::title($request->edit_nombre),
                'descripcion'=> $request->edit_descripcion
            ]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-info', 'No se pudo actualizar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            /* METODO PARA BUSQUEDA */
            $clasificacion = Clasificacion::findOrFail($id);
            if ($clasificacion) {
                Clasificacion::where('clasificacion_id', $id)->delete();

                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-info', 'Error al eliminar');
        }
    }
}
