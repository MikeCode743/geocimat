<?php

namespace App\Http\Controllers\Admin\Especialidad;

use Validator;
/* REQUEST PERSONALIZADO */
use App\Http\Requests\Admin\EspecialidadRequest;
use App\Http\Requests\Admin\Base\EditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
/* MODELOS */
use App\Models\Especialidad;
use App\Models\Personal_Especialidad;
/* TitleCase */
use Illuminate\Support\Str;
//Seguridad
use Illuminate\Support\Facades\Auth;

class EspecialidadController extends Controller
{
    /* METODO DE AUTENTIFICACION */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /* METODO PARA LA PAGINA DE INICIO */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin','administrador']);
        $especialidades = Especialidad::get();
        $modificar = false;
        return view('admin.especialidad', [
            'especialidades' => $especialidades,
            'modificar' => $modificar,
        ]);
    }
    /* METODO PARA MOSTRAR */
    public function show($id)
    {

    }
    /* METODO DE REGISTRO */
    public function store(EspecialidadRequest $request)
    {
        // dd($request->all());
        $especialidad = new Especialidad;
        $especialidad->nombre_corto = Str::lower(Str::slug($request->nombre_corto,'-'));
        $especialidad->nombre =  Str::title($request->nombre);
        $especialidad->descripcion = $request->descripcion;
        /* TRY-CATCH PARA ERROR AL GUARDAR */
        try {                        
            $especialidad->save();
            return redirect()->back()->with('message-store', 'Guardado con exito');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect()->back()->with('message-info', 'Error al registrar valor');
        }
    }

    public function update(EditRequest $request, $id)
    {
        /* TRY-CATCH PARA ERRORES AL ACTUALIZAR */
        try {
            $affectedRows = Especialidad::where('especialidad_id', $id)->update([
                'nombre_corto'=> Str::lower(Str::slug($request->edit_nombre_corto,'-')),
                'nombre' => Str::title($request->edit_nombre),
                'descripcion'=> $request->edit_descripcion
                ]);            
            return redirect()->back()->with('message-store', 'Registros actualizados ' . $affectedRows);
        } catch (\Illuminate\Database\QueryException $ex) {            
            return redirect()->back()->with('message-info', 'No se pudo actualizar');
        }
    }

    public function destroy($id)
    {
        try {
            /* METODO DE BUSQUEDA */
            $especialidad = Especialidad::findOrFail($id);
            if ($especialidad) {
                $especialidad->personales()->detach();
                Especialidad::where('especialidad_id', $id)->delete();
                return redirect()->back()->with('message-delete', 'Eliminado');
            }
        } catch (\Illuminate\Database\QueryException $ex) {        
            return redirect()->back()->with('message-info', 'Error al eliminar');
        }
    }
}
