<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class anotacion extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'anotacion_id' => $this->anotacion_id,
            'propietario' => $this->user_id,
            'autor' => $this->name,
            'fecha' => $this->fecha,
            'contenido' => $this->contenido,
            'url' => $this->imagen,
            'editado' => $this->editado,
        ];
    }
}
