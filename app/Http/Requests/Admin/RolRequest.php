<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_corto' => 'min:6|max:25|required|string|regex:/^[\pL\s\-]+$/u',
            'nombre' => 'min:6|max:100|string|required|regex:/^[\pL\s\-]+$/u',
            'descripcion' => 'min:6|max:500|required|string|regex:/^[\pL\s\-]+$/u',
        ];
    }
        public function messages()
    {
        return [
            'nombre_corto.string' => 'Este campo solo acepta letras.',
            'nombre_corto.min' => 'Este campo debe tener al menos de 6 caracteres.',
            'nombre_corto.max' => 'Este campo no puede tener mas de 25 caracteres.',
            'nombre_corto.regex' => 'Este campo solo acepta letras.',
            'nombre.string' => 'Este campo solo acepta letras.',
            'nombre.min' => 'Este campo debe tener al menos 6 caracteres.',
            'nombre.max' => 'Este campo no puede tener mas de 100 caracteres.',
            'nombre.regex' => 'Este campo solo acepta letras.',
            'descripcion.string' => 'Este campo solo acepta letras.',
            'descripcion.min' => 'Este campo no puede tener mas de 6 caracteres.',
            'descripcion.max' => 'Este campo no puede tener mas de 500 caracteres.',
            'descripcion.regex' => 'Este campo solo acepta letras.',
            'nombre_corto.required' => 'Este campo es obligatorio.',
            'nombre.required' => 'Este campo es obligatorio.',
            'descripcion.required' => 'Este campo es obligatorio.',            
            ];
    }

}
