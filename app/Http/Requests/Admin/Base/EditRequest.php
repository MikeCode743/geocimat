<?php

namespace App\Http\Requests\Admin\Base;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_nombre_corto' => 'min:6|max:25|required|string|regex:/^[\pL\s\-]+$/u',
            'edit_nombre' => 'min:6|max:100|required|string|regex:/^[\pL\s\-]+$/u',
            'edit_descripcion' => 'min:6|max:500|required|string|regex:/^[\pL\s\-]+$/u',
            
        ];
    }
        public function messages()
    {
        return [
            'edit_nombre_corto.string' => 'Este campo solo acepta letras.',
            'edit_nombre_corto.min' => 'Este campo no puede tener menos de 6 caracteres.',            
            'edit_nombre_corto.max' => 'Este campo no puede tener mas de 25 caracteres.',
            'edit_nombre_corto.regex' => 'Este campo solo acepta letras.',
            'edit_nombre.string' => 'Este campo solo acepta letras.',
            'edit_nombre.min' => 'Este campo no puede tener menos de 6 caracteres.',
            'edit_nombre.max' => 'Este campo no puede tener mas de 100 caracteres.',
            'edit_nombre.regex' => 'Este campo solo acepta letras.',
            'edit_descripcion.string' => 'Este campo solo acepta letras.',
            'edit_descripcion.min' => 'Este campo no puede tener menos de 6 caracteres.',
            'edit_descripcion.max' => 'Este campo no puede tener mas de 500 caracteres.',
            'edit_descripcion.regex' => 'Este campo solo acepta letras.',
            'edit_nombre_corto.required' => 'Este campo es obligatorio.',
            'edit_nombre.required' => 'Este campo es obligatorio.',
            'edit_descripcion.required' => 'Este campo es obligatorio.',
            ];
    }
}
