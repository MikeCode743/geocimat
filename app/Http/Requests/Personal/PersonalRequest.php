<?php

namespace App\Http\Requests\Personal;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class PersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {//$this->request->add(['name' => Str::title($this->request->get('name'))]);        
        $rules_array = [
            'name' => 'required|string|max:255',
            'email' => ['required','string','email','max:255','regex:/^[a-zA-Z0-9.]+@(ues+\.edu\.sv|gmail\.com|yahoo\.com)$/u'],
            'password' => 'required|string|min:6|max:255',
            'nombres' => 'required|regex:/^[\pL\s]+$/u|max:50', 
            'apellidos' => 'required|regex:/^[\pL\s]+$/u|max:50',
            'telefono'=> 'required|max:9|regex:/^[2,6,7]{1}[0-9]{3}(\-)?[0-9]{4}$/u'
        ];//if($this->request->get('namecomp') && ($this->request->get('namecomp') == $this->request->get('name'))){$rules_array['name'] = 'required|string|max:255';}
        return $rules_array;
    }
    public function messages()
    {
        return [
            'usuario.required' => 'Este campo es requerido.',
            'usuario.string' => 'Este campo solo acepta letras.',
            'usuario.max' => 'Este campo no puede tener mas de 255 caracteres.',    
            'email.required' => 'Este campo es requerido.',
            'email.string' => 'Este campo solo acepta letras.',
            'email.email' => 'Utilice el formato de email, ej: jc12004@gmail.com',
            'email.max' => 'Este campo no puede tener mas de 255 caracteres.',
            //'email.unique' => 'Este campo no debe de estar repetido. Revise si ya existe un usuario con este email.',
            'email.regex' => 'Este correo no es valido, Ingrese el correo institucional o personal en Gmail o Yahoo.',
            'name.required' => 'Este campo es requerido.',
            'name.string' => 'Este campo acepta letras y numeros.',            
            'name.max' => 'Este campo solo acepta hasta 255 caracteres.',
            'name.unique' => 'Nombre existente. Ingrese otro nombre de usuario.',
            'password.required' => 'Este campo es requerido.',
            'password.string' => 'Este campo solo acepta letras.',
            'password.min' => 'Este campo no puede tener menos de 6 caracteres',
            'password.max' => 'Este campo no puede tener mas de 255 caracteres.',
    
            'nombres.required' => 'Este campo es requerido.',
            'nombres.regex' => 'Este campo solo acepta letras.',
            'nombres.max' => 'Este campo no puede tener mas de 50 caracteres.',
    
            'apellidos.required' => 'Este campo es requerido.',
            'apellidos.regex' => 'Este campo solo acepta letras.',
            'apellidos.max' => 'Este campo no puede tener mas de 50 caracteres.',

            'telefono.required'=>'Este campo telefonico es requerido.',            
            'telefono.max'=>'Este campo telefonico no debe sobrepasar de los 8 digitos.', 
            'telefono.regex'=>'Este numero telefonico no es permitido.'
            ];
    }
}
