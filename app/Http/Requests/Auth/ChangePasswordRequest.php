<?php

namespace App\Http\Requests\auth;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6',
            'password_confirmed' => 'required|same:password|min:6'
        ];
    }
    public function messages()
    {
        return [
            'same'    => 'La Contraseñas no coinciden, Vuelve a introducir las contraseñas',
            'min'    =>  'La contraseña debe ser mayor a 6 caracteres.'
        ];
    }

}
