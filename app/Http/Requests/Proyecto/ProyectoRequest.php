<?php

namespace App\Http\Requests\Proyecto;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|max:248|regex:/^[ÁÉÍÓÚÑáéíóúñA-Za-z0-9\s]+$/',
            'clasificacion' => 'required|integer',
            'fecha_inicio' => 'required',
            'lng' => 'required|numeric',
            'lat' => 'required|numeric',
            'informacionAdicional' => 'max:10485760'
        ];
    }

    public function messages(){
        return [
            'titulo.required' => 'Este campo es requerido.',
            'titulo.max' => 'Este campo solo permite ingresae 248 caracteres.',
            'titulo.regex' => 'Este campo solo acepta letras, numeros y espacios.',
            'clasificacion.required' => 'Este campo es requerido. Seleccione una clasificacion.',
            'clasificacion.integer' => 'Valor inválido. Seleccione una clasificacion.',
            'fecha_inicio.required' => 'Este campo es requerido.',
            'lng.required' => 'Este campo es requerido.',
            'lng.numeric' => 'Este campo debe ser numerico, por favor intente de nuevo.',
            'lat.required' => 'Este campo es requerido.',
            'lat.numeric' => 'Este campo debe ser numerico, por favor intente de nuevo.',
            'informacionAdicional.max' => 'Estecampo solo permite ingresar 10485760 caracteres.'
        ];
    }
}
