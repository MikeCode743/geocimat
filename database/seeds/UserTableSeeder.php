<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Rol;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol_admin = Rol::where('rol_id','1')->first();

        $user = new User();
        $user->name = "Admin";
        $user->email = "laravel@gmail.com";
        $user->password = Bcrypt('laravel');
        $user->save();
        $user->roles()->attach($rol_admin);
    }
}
