<?php

use Illuminate\Database\Seeder;
use App\Models\Rol;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new Rol();
        $rol->nombre_corto = "admin";
        $rol->nombre = "Administrador";
        $rol->descripcion = "Admininistrador del sistema ";
        $rol->save();

        $rol = new Rol();
        $rol->nombre_corto = "inves";
        $rol->nombre = "Investigador";
        $rol->descripcion = "Investigador General ";
        $rol->save();

        $rol = new Rol();
        $rol->nombre_corto = "invtd";
        $rol->nombre = "Invitado";
        $rol->descripcion = "Admininistrador del sistema ";
        $rol->save();

    }
}
