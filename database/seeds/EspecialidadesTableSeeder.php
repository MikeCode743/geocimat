<?php

use Illuminate\Database\Seeder;
use App\Models\Especialidad;

class EspecialidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidad = new Especialidad();
        $especialidad->nombre = "Física";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Hidrogeología";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Física de Radiaciones";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Vulcanología";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Geofísica";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Estado Solido";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Espectroscopia";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Geología";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Astrofísica";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Electrónica";
        $especialidad->save();

        $especialidad = new Especialidad();
        $especialidad->nombre = "Energia Renovables";
        $especialidad->save();

    }
}
