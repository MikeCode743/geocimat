<?php

use Illuminate\Database\Seeder;

//Modelo
use App\Models\Clasificacion;

class ClasificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "Inspección";
        $Clasificacion->save();

                $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "VLF";
        $Clasificacion->save();

                $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "Magnetometría";
        $Clasificacion->save();

                $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "Aguas Subterraneas";
        $Clasificacion->save();

                $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "Deslizamientos";
        $Clasificacion->save();

                $Clasificacion = new Clasificacion();
        $Clasificacion->nombre = "Geología";
        $Clasificacion->save();
    }
}
