<?php

//--------------------HOME----------------------------------------------------------------------------------------------------

Route::get('/home', 'HomeController@index')->name('home');
//Mapa público
Route::get('/mapa-proyectos',[App\Http\Controllers\Mapa\MapaController::class, 'mapa'])->name('proyecto.mapapublico');

//--------------------RUTAS PARA PERSONAL-------------------------------------------------------------------------------------
Route::group(['prefix' => 'personal', 'namespace' => 'Personal'], function () {
        Route::get('', 'PersonalController@index')->name('personal.home');
        Route::get('create', 'PersonalController@create')->name('personal.create');
        Route::post('store', 'PersonalController@store')->name('personal.store');
        Route::get('show/{id}', 'PersonalController@show')->name('personal.show');
        Route::get('edit/{id}', 'PersonalController@edit')->name('personal.edit');
        Route::post('update/{id}', 'PersonalController@update')->name('personal.update');
        Route::get('destroy/{id}', 'PersonalController@destroy')->name('personal.destroy');
});
//--------------------RUTAS PARA LOGIN----------------------------------------------------------------------------------------
// Authentication Routes...
Route::group(['namespace' => 'Auth'], function () {
        Route::get('/', 'LoginController@showLoginForm')->name('login');
        Route::post('/', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset');
        Route::get('password/change', 'ChangePasswordController@ShowForm')->name('change.password');                     // Password Reset Routes...
        Route::post('password/change', 'ChangePasswordController@ChangePassword');
        Route::get('contrasenia', 'ChangePasswordController@mostrarFormularioCambio')->name('contrasenia.cambiar');
        Route::post('contrasenia', 'ChangePasswordController@cambiarContrasenia');
});

//###########################################  ADMIN  #####################################################


Route::group(['prefix' => 'administracion', 'namespace' => 'Admin'], function () {

        Route::group(['namespace' => 'Administracion'], function () {
                Route::get('', 'AdministracionController@index')->name('administracion');
        });
        //-----------------------ESPECIALIDADES------------------------------------------------------
        Route::group(['prefix' => 'especialidades', 'namespace' => 'Especialidad'], function () {
                Route::get('/', 'EspecialidadController@index')->name('home.especialidades');
                Route::get('/{id}', 'EspecialidadController@show')->name('especialidades.show');
                Route::post('/', 'EspecialidadController@store')->name('especialidad.store');
                Route::patch('/{id}', 'EspecialidadController@update')->name('especialidad.update');
                Route::delete('/{id}', 'EspecialidadController@destroy')->name('especialidad.destroy');
        });
        //----------------------ROLES-------------------------------------------------------------------
        Route::group(['prefix' => 'roles', 'namespace' => 'Rol'], function () {
                Route::get('', 'RolController@index')->name('rol.home');
                Route::get('{id}', 'RolController@show')->name('rol.show');
                Route::post('', 'RolController@store')->name('rol.store');
                Route::patch('{id}', 'RolController@update')->name('rol.update');
                Route::delete('{id}', 'RolController@destroy')->name('rol.destroy');
        });
        //-----------------------PERMISOS-------------------------------------------------------------
        Route::group(['prefix' => 'permisos', 'namespace' => 'Permisos'], function () {
                Route::get('', 'PermisoController@index')->name('permisos.home');
                Route::get('{id}/', 'PermisoController@show')->name('permisos.show');
                Route::post('', 'PermisoController@store')->name('permisos.store');
                Route::patch('{id}/', 'PermisoController@update')->name('permisos.update');
                Route::delete('{id}/', 'PermisoController@destroy')->name('permisos.destroy');
        });
        //-----------------------PERMISOS-ROLES-------------------------------------------------------
        Route::group(['prefix' => 'permisos-roles', 'namespace' => 'Rol'], function () {
                Route::get('', 'PermisosRolesController@index')->name('permisosrol.home');
                Route::get('{id}', 'PermisosRolesController@show')->name('permisosrol.show');
                Route::post('', 'PermisosRolesController@store')->name('permisosrol.store');
                Route::delete('{r}/{p}', 'PermisosRolesController@destroy')->name('permisosrol.destroy');
        });
        //------------------------ETAPA-----------------------------------------------------------
        Route::group(['prefix' => 'etapas', 'namespace' => 'Etapa'], function () {
                Route::get('', 'EtapaController@index')->name('etapa.home');
                Route::get('{id}', 'EtapaController@show')->name('etapa.show');
                Route::post('', 'EtapaController@store')->name('etapa.store');
                Route::patch('{id}', 'EtapaController@update')->name('etapa.update');
                Route::delete('{id}', 'EtapaController@destroy')->name('etapa.destroy');
        });
        //-------------------------CLASIFICACION-----------------------------------------------
        Route::group(['prefix' => 'clasificaciones', 'namespace' => 'Clasificacion'], function () {
                Route::get('', 'ClasificacionController@index')->name('clasificacion.home');
                Route::get('{id}', 'ClasificacionController@show')->name('clasificacion.show');
                Route::post('', 'ClasificacionController@store')->name('clasificacion.store');
                Route::patch('{id}', 'ClasificacionController@update')->name('clasificacion.update');
                Route::delete('{id}', 'ClasificacionController@destroy')->name('clasificacion.destroy');
        });
        //----------------------------GRUPO--------------------------------------------------------------
        Route::group(['prefix' => 'grupo', 'namespace' => 'Grupo'], function () {
                Route::get('', 'GrupoController@index')->name('grupo.home');
                Route::get('{id}', 'GrupoController@show')->name('grupo.show');
                Route::post('', 'GrupoController@store')->name('grupo.store');
                Route::patch('{id}', 'GrupoController@update')->name('grupo.update');
                Route::delete('{id}', 'GrupoController@destroy')->name('grupo.destroy');
        });
});

//###################################  PROYECTO ##############################################################
Route::group(['prefix' => 'proyecto','namespace'=>'Proyecto'], function () {
        Route::get('','ProyectoController@index')->name('proyecto.home');
        Route::get('agregar','ProyectoController@create')->name('proyecto.crear');
        Route::post('store','ProyectoController@store')->name('proyecto.store');
        Route::get('lista','ProyectoController@lista')->name('proyecto.lista');
        Route::get('mis-proyectos','ProyectoController@misproyectos')->name('proyecto.misproyectos');
        Route::get('{id}/detalle','ProyectoController@detalle')->name('proyecto.detalle');
        Route::get('listado','ProyectoController@listadocompleto')->name('proyecto.listado');
        Route::post('{id}/editar','ProyectoController@editarProyecto')->name('proyecto.editar');
        Route::post('{id}/eliminar', 'ProyectoController@deshabilitar')->name('proyecto.eliminar');

        //----------------------------Mapa---------------------------------------------------------------
        Route::get('mapa-proyectos','ProyectoController@mapa')->name('proyecto.mapa');



        //----------------------------Imagenes---------------------------------------------------------------
        Route::get('{id}/imagenes', 'ImageController@listadoImagen')->name('proyecto.imagen');
        Route::post('imagenes/create', 'ImageController@agregarImagen')->name('proyecto.imagenes.crear');
        Route::post('imagenes', 'ImageController@eliminarImagen')->name('proyecto.imagenes.eliminar');
        Route::get('{id}/imagenes/papelera', 'ImageController@papeleraImagen')->name('imagen.papelera');
        Route::post('imagenes/papelera/eliminar', 'ImageController@papeleraEliminarImagen')->name('imagen.papelera.eliminar');
        Route::post('imagenes/papelera/restaurar', 'ImageController@restaurarImagen')->name('imagen.papelera.restaurar');


        //----------------------------Archivos----------------------------------------------------------------
        Route::get('{id}/archivos', 'FileController@listaArchivo')->name('proyecto.archivos');
        Route::post('archivos/crear', 'FileController@agregarArchivo')->name('proyecto.archivos.crear');
        Route::post('archivos', 'FileController@eliminarArchivo')->name('proyecto.archivos.eliminar');
        Route::get('{id}/archivos/papelera', 'FileController@papeleraArchivo')->name('archivo.papelera');
        Route::post('archivo/papelera/eliminar', 'FileController@papeleraEliminarArchivo')->name('archivo.papelera.eliminar');
        Route::post('archivo/papelera/restaurar', 'FileController@restaurarArchivo')->name('archivo.papelera.restaurar');

        //----------------------------Bitacora----------------------------------------------------------------
        Route::get('{id}/bitacora', 'ProyectoController@bitacora')->name('proyecto.bitacora');
        Route::post('/bitacora', 'ProyectoController@editarBitacora')->name('proyecto.bitacora.editar');

        //----------------------------Cambiar Etapa-----------------------------------------------------------
        Route::post('cambiar-etapa-mp/{id}', 'ProyectoController@cambiarEtapaMisProyectos')->name('misproyectos.etapa.cambiar');
        Route::post('cambiar-etapa-tp/{id}', 'ProyectoController@cambiarEtapaTodosProyectos')->name('todosproyectos.etapa.cambiar');
        //----------------------------Cambiar Estado----------------------------------------------------------        
        Route::post('cambiar-estado-mp/{id}', 'ProyectoController@cambiarEstadoMisProyectos')->name('misproyectos.estado.cambiar');
        Route::post('cambiar-estado-tp/{id}', 'ProyectoController@cambiarEstadoTodosProyectos')->name('todosproyectos.estado.cambiar');

        //----------------------------Informes -----------------------------------------------------------------
        Route::get('informes', 'ProyectoController@informes')->name('proyecto.informes');
        //---------------------------- Estadisticas -----------------------------------------------------------------
        Route::get('estadisticas', 'ProyectoController@estadisticas')->name('proyecto.estadisticas');

});

//----------------------------RUTAS PARA EL CRONOGRAMA DE VISITAS AL SITIO------------------------------------
Route::group(['prefix' => 'cronograma','namespace'=>'Cronograma'], function () {
    Route::get('', 'CronogramaController@index')->name('cronograma.home');
    Route::get('terminadas', 'CronogramaController@mostrarTerminadas')->name('cronograma.visitas.terminadas');
    Route::get('proceso', 'CronogramaController@mostrarEnProceso')->name('cronograma.visitas.proceso');
    Route::get('visitar', 'CronogramaController@mostrarPorVisitar')->name('cronograma.visitas.visitar');
    Route::get('canceladas', 'CronogramaController@mostrarCanceladas')->name('cronograma.visitas.canceladas');    

    Route::get('planificar-visitas', 'CronogramaController@planificarVisitas')->name('cronograma.visitas.planificar');
    Route::post('agregar-visitas', 'CronogramaController@agregarVisitas')->name('cronograma.visitas.agregar');    
});



Route::view('/repository/{ruta}', 'URL')->name('repository');

/* Carlos */
Route::get('/cards', 'Cards@index');
Route::get('/visita', 'Cards@index2');
Route::get('/formulario', 'Cards@index3');
Route::get('/proyectos', 'Cards@index4');
Route::get('/proyectos/archivo', 'Cards@index5');
Route::get('/proyectos/image', 'Cards@index6');

Route::view('/navbar', 'layouts.barra-navegacion');

Route::view('/form', 'proyecto.formularioFAIL');

//Anotaciones
Route::get('proyecto/mensajeria', 'Proyecto\Anotacion@verAnotacion')->name('proyecto.mensajeria');
Route::get('chat/proyectos', 'Proyecto\Anotacion@ProyectoAnotacion')->name('chat.proyectos');
//Recurso
Route::get('proyecto/anotacion', 'Proyecto\Anotacion@listarAnotacion');
Route::post('proyecto/anotacion', 'Proyecto\Anotacion@operacionAnotacion');
