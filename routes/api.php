<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/listadoProyectos', 'Proyecto\ProyectoControllerCP@idlistado');
Route::post('/agregarArchivo', 'Proyecto\ProyectoControllerCP@agregarArchivo');
Route::get('/consultarArchivo', 'Proyecto\ProyectoControllerCP@consultarArchivo');
Route::delete('/removerArchivo', 'Proyecto\ProyectoControllerCP@removerArchivo');