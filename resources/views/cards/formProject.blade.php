<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Datepicker -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous"></script>

    <!-- Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

    <!-- MapBox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    
    <style>
        .note-editable {
            height: 250px;
            background-color: #fff;
        }

        body {
            font-family: "proxima-nova", "proxima nova", "helvetica neue", "helvetica", "arial", sans-serif;
            background-color: rgb(226, 228, 229);
        }

        .form-control:focus {
            color: #495057;
            background-color: #fff;
            border-color: #0e2294;
            outline: 0;
            box-shadow: 0 0 0 0rem rgba(0, 123, 255, 0.25);
        }

    </style>
</head>

<body>

    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/formulario">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos">Listar Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/archivo">Archivos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/image">Fotos</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="loading" hidden>
        <div class="d-flex justify-content-center text-info mt-5">
            <div class="spinner-grow" style="width: 8rem; height: 8rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>

    <div class="row bg-light">

        <div class="col-12 col-sm-12 col col-md-12 mx-auto mx-md-auto mt-3">
            <div class="card bg-transparent border-0">
                <div class="card-header mx-auto bg-transparent border-0">
                    <p class="h3">Agregar proyecto</p>
                </div>
                <div class="card-body ">

                    <form id="configform">

                        <div class="form-row">

                            <div class="form-group col-md-8 mx-md-auto">

                                <label>Título del proyecto</label><span> *</span>
                                <input type="text" class="form-control" id="nombre" placeholder="">

                                <label class="mt-3">Clasificación</label><span> *</span>
                                <select class="form-control" id="clasificacion">
                                    <option>Resistividad</option>
                                </select>

                                <label class="mt-3">Fecha de inicio</label><span> *</span>
                                <input type="text" class="form-control d-flex justify-content-center" id="datepicker">
                            </div>

                            <div class="form-group col-md-8 mx-md-auto">

                                <label>Coordenadas<span> *</span></label>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text px-3" id="basic-addon1">Longitud</span>
                                            </div>
                                            <input id="lng" type="text" class="form-control" placeholder="-180 a 180">
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6">
                                        <div class="input-group mb-3 ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text px-3" id="basic-addon1">Latitud</span>
                                            </div>
                                            <input id="lat" type="text" class="form-control" placeholder="-90 a 90">
                                        </div>
                                    </div>
                                </div>

                                <div id="menu" class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <button id="satellite" type="button"
                                        class="btn btn-primary btn-sm">Satellite</button>
                                    <button id="outdoors" type="button"
                                        class="btn btn-secondary btn-sm">Outdoors</button>
                                    <button id="streets" type="button" class="btn btn-secondary btn-sm">Streets</button>
                                </div>

                                <div id="map" style="width: 100%; height: 450px; margin-top: 5px;"></div>
                                <small class="form-text text-muted">
                                    Click en el mapa para agregar las coordenadas.
                                </small>
                            </div>

                            <div class="form-group col-md-8 mx-md-auto">
                                <label>Descripci&oacute;n del proyecto</label>
                                <textarea id="informacionAdicional" name="informacionAdicional"
                                    class="summernote"></textarea>
                            </div>
                            <div class="text-right col-md-8 mx-md-auto">
                                <button type="button" id="close" class="btn btn-secondary">Cancelar</button>
                                <button type="button" id="add" class="btn btn-primary">Agregar</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            const lngInput = document.querySelector('#lng');
            const latInput = document.querySelector('#lat');

            let marker = new mapboxgl.Marker();
            let [lng, lat] = [0, 0];

            const markerInput = () => {

                let [lngValid, latValid] = [false, false];
                let [parseLng, parseLat] = [parseFloat(lngInput.value), parseFloat(latInput.value)]

                //LNG
                if (parseLng == lngInput.value) {
                    if (parseLng >= -180.0 && parseLng <= 180) {
                        lngInput.classList.remove("is-invalid");
                        lngValid = true;
                    } else {
                        lngInput.classList.add("is-invalid");
                    }
                } else {
                    lngInput.classList.add("is-invalid");
                }

                //LAT
                if (parseLat == latInput.value) {
                    if (parseLat >= -90.0 && parseLat <= 90.0) {
                        latInput.classList.remove("is-invalid");
                        latValid = true;
                    } else {
                        latInput.classList.add("is-invalid");
                    }
                } else {
                    latInput.classList.add("is-invalid");
                }

                //Add Marker
                if (lngValid && lngValid) {
                    marker.setLngLat([parseLng, parseLat]).addTo(map);
                } else {
                    marker.remove();
                }

                return lngValid && lngValid;

            }

            //MapBox
            mapboxgl.accessToken =
                'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg';

            let map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/satellite-streets-v11',
                center: [-88.85, 13.82], // starting position
                zoom: 7 // starting zoom
            });

            map.on('click', function(e) {
                latInput.classList.remove("is-invalid");
                lngInput.classList.remove("is-invalid");
                $("#coordenadas").val(JSON.stringify(e.lngLat.wrap()));
                [lng, lat] = [e.lngLat.wrap().lng, e.lngLat.wrap().lat];
                lngInput.value = lng.toFixed(8);
                latInput.value = lat.toFixed(8);
                marker.setLngLat([lng, lat]).addTo(map);
            });

            $('#satellite').click(function() {
                map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
            });
            $('#outdoors').click(function() {
                map.setStyle('mapbox://styles/mapbox/outdoors-v11');
            });
            $('#streets').click(function() {
                map.setStyle('mapbox://styles/mapbox/streets-v11');
            });

            $("#lng").keyup(function(event) {
                markerInput();
            });

            $("#lat").keyup(function() {
                markerInput();
            });

            //Datepicker
            $("#datepicker").datepicker({
                inline: true,
                format: 'dd/mm/yyyy',
                todayBtn: true,
                todayHighlight: true,
                orientation: 'top right',
            }).datepicker("setDate", 'now');

            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                placeholder: 'Ej. Objetivo del proyecto',
            });

            //SweetAlert
            showMessage = (message, icon) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })
            };

        });

    </script>
</body>

</html>
