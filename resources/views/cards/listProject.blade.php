<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>


    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

    <!-- Firebase -->
    <script src="https://www.gstatic.com/firebasejs/7.2.3/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.2.3/firebase-firestore.js"></script>


    <!-- MApBox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />

    <style>
        .note-editable {
            height: 250px;
            background-color: #fff;
        }

        body {
            font-family: "proxima-nova", "proxima nova", "helvetica neue", "helvetica", "arial", sans-serif;
            background-color: rgb(226, 228, 229);
            /*background-color: #314369;*/
        }

        .form-control:focus {
            color: #495057;
            background-color: #fff;
            border-color: #0e2294;
            outline: 0;
            box-shadow: 0 0 0 0rem rgba(0, 123, 255, 0.25);
        }

        #header,
        label {
            color: #444;
        }

        .jumbotron {
            background-color: #eff2f5;
        }

        .panel-heading {
            background-color: rgba(241, 235, 235, 0.699);
        }

        #info {
            display: block;
            position: relative;
            margin: 0px auto;
            width: 50%;
            padding: 10px;
            border: none;
            border-radius: 3px;
            font-size: 12px;
            text-align: center;
            color: #222;
            background: #fff;
        }

    </style>



</head>

<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/formulario">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos">Listar Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/archivo">Archivos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/image">Fotos</a>
                </li>
            </ul>
        </div>
    </nav>

    <!--
    <div id='map' style='width: 400px; height: 300px;'></div>
    <pre id="info"></pre>
    -->

    <div class="row col-md-8 mx-md-auto">
        <div class="col">
            <p class="h1 mt-3">Proyectos</p>
        </div>
    </div>

    <div class="form-row col-md-8 mx-md-auto">
        <div class="col-md-5">
            <!-- <label>T&iacute;tulo</label> -->    
            <label>Nombre del proyecto</label>
            <input id="title" type="text" class="form-control">
        </div>
        <div class="col-md-5">
            <label>Descripci&oacuten</label>
            <input id="desc" type="text" class="form-control">
        </div>
    </div>

    <!-- GET CARDS -->
    <div class="row mx-3">
    </div>

    <!-- NewCards -->
    <div class="col-md-8 mx-md-auto mt-3 rounded">
        <p id="nullFilter" class="h5">Resultado de busqueda 0</p>
        <div class="card text-center">
            <div class="card-header">
                <p class="h5 mb-0">Pellentesque - 10/08/2020</p>
            </div>
            <div class="card-body pt-2">
                <div class="row text-left">
                    <div class="col-md-6">
                        <h4><span class="badge badge-secondary">Resistividad</span></h4>
                        <p class="card-text">
                        <h1>HTML Ipsum Presents</h1>

                        <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames
                            ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet,
                            ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em>
                            Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra.
                            Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi.
                            Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus
                            lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.
                        </p>
                        </p>
                    </div>
                    <div class="col-md-6 mx-md-auto mt-3 text-center">

                        <img src="https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/static/-90.2949,14.2814,14.19,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                            class="img-fluid" alt="Responsive image">

                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <a href="/proyectos/image" class="btn btn-info">Galer&iacute;a</a>
                <a href="/proyectos/archivo" class="btn btn-dark">Ver Archivos</a>
            </div>
        </div>
    </div>

    <div class="col-md-8 mx-md-auto mt-3 rounded">
        <div class="card text-center">
            <div class="card-header">
                <p class="h5 mb-0">Praesent - 10/08/2020</p>
            </div>
            <div class="card-body pt-2">
                <div class="row text-left">
                    <div class="col-md-6">
                        <h4><span class="badge badge-secondary">Termograf&iacute;a</span></h4>
                        <p class="card-text">
                        <ul>
                            <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a,
                                ultricies in, diam. Sed arcu. Cras consequat.</li>
                            <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate
                                magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan
                                porttitor, facilisis luctus, metus.</li>
                            <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula
                                vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit
                                amet, nisi.</li>
                            <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut,
                                elementum vulputate, nunc.</li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-md-6 mx-md-auto text-center mt-3">

                        <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/-89.1288,14.3828,13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                            class="img-fluid" alt="Responsive image">

                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <a href="/proyectos/image" class="btn btn-info">Galer&iacute;a</a>
                <a href="/proyectos/archivo" class="btn btn-dark">Ver Archivos</a>
            </div>
        </div>
    </div>

    <script>
        $(() => {
            $("#nullFilter").hide();
            //Get cards
            let cardsAvailable = $('.card');

            // Search by title
            $(":input").keyup(function() {
                $("#nullFilter").hide();
                let inputTitle = $("#title").val().toLowerCase();
                let inputDesc = $("#desc").val().toLowerCase();

                //String empty show card                
                if (!inputDesc && !inputTitle) {
                    cardsAvailable.children().show();
                }

                let hasCard2Show = false;
                //Cards filter
                cardsAvailable.each(i => {
                    $(cardsAvailable[i]).hide();

                    // Card / Card Header / H5
                    let cardHeader = cardsAvailable[i].children[0].children[0].innerText
                        .toLowerCase();
                    // Card / Card Body / Row  
                    let cardBody = cardsAvailable[i].children[1].children[0].innerText
                        .toLowerCase();

                    if (cardHeader.includes(inputTitle) && cardBody.includes(inputDesc)) {
                        $(cardsAvailable[i]).show();
                        hasCard2Show = true;
                    }
                });

                if (!hasCard2Show) {
                    $("#nullFilter").show();
                }

            });

            // Search by description
            /*
            $("#desc").keyup(function() {
                let inputStr = $("#desc").val().toLowerCase();
                cardsAvailable.each(i => {
                    $(cardsAvailable[i]).hide();
                    // Card / Card Body / Row                    
                    let cardBody = cardsAvailable[i].children[1].children[0].innerText
                        .toLowerCase();

                    if (cardBody.includes(inputStr)) {
                        $(cardsAvailable[i]).show();
                    }
                });
            });
            */

        });


        /*

        <img src="https://firebasestorage.googleapis.com/v0/b/ciencias-naturales-5d25e.appspot.com/o/oval.svg?alt=media&token=3bcbc403-6379-46b4-a81e-d448a42bcf05" />

        var firebaseConfig = {
            //apiKey: "AIzaSyBXlwBqRe-f9WT-8ZXRR4LLRTtCBwGAHV8",
            //authDomain: "ciencias-naturales-5d25e.firebaseapp.com",
            databaseURL: "https://ciencias-naturales-5d25e.firebaseio.com",
            projectId: "ciencias-naturales-5d25e",
            //storageBucket: "ciencias-naturales-5d25e.appspot.com",
            //messagingSenderId: "832297813199",
            //appId: "1:832297813199:web:715dc7f5a89acb94ef23ba"
        };
        // Initialize Firebase
        try {
            firebase.initializeApp(firebaseConfig);
            var db = firebase.firestore();
        } catch {
            console.log("Error de conexion");
        }

        db.collection("project").get().then((querySnapshot) => {            
            var datos = [];
            querySnapshot.forEach((doc) => {                
                let agregar = {};
                agregar.id = doc.id;
                agregar.title = doc.data().title;
                agregar.clasy = doc.data().clasy;
                agregar.desc = doc.data().desc;
                datos.push(agregar);                
            }, function(reason) {
                console.log('Error');
            });            
            console.log(datos);
        });
        */



        /*
            mapboxgl.accessToken =
                'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg';
            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11'
            });
            
            mapboxgl.accessToken =
                'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg';
            var map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [-88.5, 13.5], // starting position
                zoom: 7 // starting zoom
            });

            map.on('click', function(e) {
                document.getElementById('info').innerHTML =
                    // e.point is the x, y coordinates of the mousemove event relative
                    // to the top-left corner of the map
                    JSON.stringify(e.point) +
                    '<br />' +
                    // e.lngLat is the longitude, latitude geographical position of the event
                    JSON.stringify(e.lngLat.wrap());
            });
            
            let data = [];

            $.ajax({
                url: 'https://us-central1-ciencias-naturales-5d25e.cloudfunctions.net/projects',
                success: function(respuesta) {
                    data = respuesta;
                    console.log(data);
                    //addCards();
                },
                error: function() {
                    showMessage('Error inesperado', 'error');
                    console.log("Revisar ruta");
                }
            });

            showMessage = (message, icon) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: message,
                    showConfirmButton: false,
                    timer: 2000
                })
            };

            addCards = () => {
                data.forEach((e) => {
                    insertCard = `<div class="col-md-4">
                    <div class="card mx-2 my-3">
            <div class="card-header">
                ${e.title} - ${e.date} ${e.lng}
            </div>
                <div class="card-body">
                    <h5 class="card-title">${e.clasy}</h5>
                    <p class="card-text">${e.desc}</p>
                    <a href="${e.id}" class="btn btn-primary align-right">Ver archivos</a>
                    <a href="${e.id}" class="btn btn-primary align-right">Ver Fotos</a>
                </div>
            </div>
        </div>`
                    $(".row").append(insertCard);
                });
            };
         */

    </script>
</body>

</html>
