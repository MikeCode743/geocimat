<!DOCTYPE html>
<html>

<head>
    <title>Project Photos</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

    <!-- BULMA -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
</head>

<body>



    <div class="columns is-multiline is-centered">

        <div class="column is-one-quarter mx-3 my-3">
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://d3te2s0dmhk13a.cloudfront.net/-783662502.jpg" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                        <a href="#">#css</a> <a href="#">#responsive</a>
                        <br>
                        <time datetime="2016-1-2">11:09 PM - 1 Jan 2016</time>
                    </div>
                </div>
                <footer class="card-footer">
                    <a href="#" class="card-footer-item">Save</a>
                    <a href="#" class="card-footer-item">Edit</a>
                    <a href="#" class="card-footer-item">Delete</a>
                </footer>
            </div>
        </div>


        <div class="column is-one-quarter mx-3 my-3">
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                            alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                        <a href="#">#css</a> <a href="#">#responsive</a>
                        <br>
                        <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                    </div>
                </div>
                <footer class="card-footer">
                    <a href="#" class="card-footer-item">Save</a>
                    <a href="#" class="card-footer-item">Edit</a>
                    <a href="#" class="card-footer-item">Delete</a>
                </footer>
            </div>
        </div>

        <div class="column is-one-quarter mx-3 my-3">
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">John Smith</p>
                            <p class="subtitle is-6">@johnsmith</p>
                        </div>
                    </div>

                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                        <a href="#">#css</a> <a href="#">#responsive</a>
                        <br>
                        <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                    </div>
                </div>
            </div>
        </div>

        <div class="column is-one-quarter mx-3 my-3">
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">John Smith</p>
                            <p class="subtitle is-6">@johnsmith</p>
                        </div>
                    </div>

                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                        <a href="#">#css</a> <a href="#">#responsive</a>
                        <br>
                        <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                    </div>
                </div>
            </div>
        </div>


    </div>





    <!-- 
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/formulario">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos">Listar Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/archivo">Archivos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/image">Fotos</a>
                </li>
            </ul>
        </div>
    </nav>

NAVBAR -->


    <input type="file" id="files" hidden multiple>

    <!-- Header -->
    <div class="col-md-10 offset-md-1 mt-3">
        <div class="card">
            <!-- Create -->
            <div id="upload" class="card-header" style="border-bottom: 0px;">
                <div class="row">
                    <div class="h6 col text-left mt-2">
                        Fotos del proyecto ...
                    </div>
                    <div class="col text-right">
                        <button id="upFile" class="btn btn-sm mr-1"><i class="fa fa-upload"> Subir Foto</i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row mx-1">
        <!--Base -->
        <div class="col-md-5 mt-3 px-2 ml-md-auto">
            <div class="card">
                <img class="img-fluid" style="width: auto; height: 30rem;"
                    src="https://d3te2s0dmhk13a.cloudfront.net/-783662502.jpg" alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <a id="openFile" href="https://d3te2s0dmhk13a.cloudfront.net/-783662502.jpg" target="_blank"
                            class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 mt-3 px-2 mr-md-auto">
            <div class="card">
                <img class="img-fluid" style="width: auto; height: 30rem;"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <a id="openFile"
                            href="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                            target="_blank" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 mt-3 px-2 ml-md-auto">
            <div class="card">
                <img class="img-fluid" style="width: auto; height: 30rem;"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2F1a%2Fc4%2Fphoto-1410260.jpg!d&f=1&nofb=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <a id="openFile"
                            href="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2F1a%2Fc4%2Fphoto-1410260.jpg!d&f=1&nofb=1"
                            target="_blank" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 mt-3 px-2 mr-md-auto">
            <div class="card">
                <img class="img-fluid" style="width: auto; height: 30rem;"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <a id="openFile"
                            href="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                            target="_blank" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
    <div class="row mx-1 offset-md-1">
        <div class="col-md-5 mt-2">
            <div class="card">
                <img class="card-img-top" src="https://d3te2s0dmhk13a.cloudfront.net/-783662502.jpg"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 mt-2">
            <div class="card">
                <img class="card-img-top"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></button>
                    </div>
                </div>
            </div>
        </div>

    </div>


 
    <div class="col-md-10 offset-md-1 mt-3">

        <div class="col-md-6 mt-2">
            <div class="card mt-1">

                <img class="card-img-top"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mt-2">
            <div class="card mt-1">

                <img class="card-img-top"
                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                    alt="Card image cap">
                <div class="card-body">
                    <div class="col text-right">
                        <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                        <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                                Descargar</i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">

            <img class="card-img-top" src="https://i.ytimg.com/vi/-ogWHhd6CsA/maxresdefault.jpg" alt="Card image cap">
            <div class="card-body">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>

        <div class="card">

            <div class="card-body">
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                    class="img-fluid rounded mx-auto d-block" alt="...">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1 mt-3"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm mr-1 mt-3"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>
    </div>





    <div class="col-md-4 mt-2">
        <div class="card">

            <img class="card-img-top"
                src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F3.bp.blogspot.com%2F-ZUZTdDfEvOM%2FU6XFCiWFLmI%2FAAAAAAAABDo%2FWrIz-pCYr3g%2Fs1600%2Fhermosos%2Bpaisaje%2B9.jpg&f=1&nofb=1"
                alt="Card image cap">
            <div class="card-body">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 mt-2">
        <div class="card">

            <img class="card-img-top" src="https://i.ytimg.com/vi/-ogWHhd6CsA/maxresdefault.jpg" alt="Card image cap">
            <div class="card-body">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 mt-2">
        <div class="card">

            <img class="card-img-top"
                src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                alt="Card image cap">
            <div class="card-body">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>
    </div>
    </div>


    <div class="col-md-10 offset-md-1 mt-3">
        <div class="card">
            
            <div class="card-body">
                <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F3.bp.blogspot.com%2F-ZUZTdDfEvOM%2FU6XFCiWFLmI%2FAAAAAAAABDo%2FWrIz-pCYr3g%2Fs1600%2Fhermosos%2Bpaisaje%2B9.jpg&f=1&nofb=1"
                    class="img-fluid rounded mx-auto d-block" alt="...">
                <div class="col text-right">
                    <button id="deleteFile" class="btn btn-sm mr-1 mt-3"><i class="fa fa-trash-o"></i></button>
                    <button id="upFile" class="btn btn-sm mr-1 mt-3"><i class="fa fa-download">
                            Descargar</i></button>
                </div>
            </div>
        </div>
    </div>

-->

    <script>
        $(document).ready(function() {

            //Click
            $("button").click(function() {
                console.log($(this).attr('data-action'));
            });

            //Click File
            $("#upFile").click(function() {
                $("#files").click();
            });

            //Open, Delete File 
            $('#table').on('click', '.clickable-row', function(event) {
                //console.log($(this));
                //console.log($(this).siblings());
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $("#upload").show();
                    $("#delete").hide();
                } else {
                    $(this).addClass('active').siblings().removeClass('active');
                    $("#upload").hide();
                    $("#delete").show();
                }
            });

            $('#openFile').on('click', function(event) {
                //console.log($('.clickable-row.active').attr('data-href'));
                //const url = $('.clickable-row.active').attr('data-href');
                //window.open(url, '_blank');
            });


        });

    </script>

</body>

</html>
