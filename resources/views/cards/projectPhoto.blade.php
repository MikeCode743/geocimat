<!DOCTYPE html>
<html>

<head>
    <title>Project Photos</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <!---->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

    <style>
        body {
            background-color: rgba(247, 250, 252);
            font-family: Roboto, Noto Sans, -apple-system, BlinkMacSystemFont, sans-serif;
        }

        :root {
            --fire-color-grey-50: #fafafa;
            --fire-color-grey-100: #f5f5f5;
            --fire-color-grey-200: #eee;
            --fire-color-grey-300: #e0e0e0;
            --fire-color-grey-400: #bdbdbd;
            --fire-color-grey-500: #9e9e9e;
            --fire-color-grey-600: #757575;
            --fire-color-blue-gray-20: #f5f7f8;
            --fire-color-blue-gray-100: #cfd8dc;
            --fire-color-blue-gray-300: #90a4ae;
            --fire-color-blue-gray-400: #78909c;
            --fire-color-blue-gray-600: #546e7a;
            --fire-color-amber-800: #ff8f00;
            --fire-color-orange-900: #e65100;
            --fire-color-red-600: #e53935;
            --fire-color-light-blue-600: #039be5;
            --fire-color-old-navy-50: #c5cad9;
            --fire-color-old-navy-100: #a5b0c4;
            --fire-color-old-navy-200: #8a9bb2;
            --fire-color-old-navy-300: #657992;
            --fire-color-old-navy-400: #4c5f77;
            --fire-color-old-navy-500: #37475d;
            --fire-color-old-navy-600: #2e3a4d;
            --fire-color-old-navy-700: #262f3d;
            --fire-color-old-navy-800: #19212b;
            --fire-color-old-navy-900: #121820;
            --fire-color-old-navy-a100: #8298bd;
            --fire-color-old-navy-a200: #6883ac;
            --fire-color-old-navy-a400: #3b577d;
            --fire-color-old-navy-a700: #042a4a;
            --fire-color-navy-10: #f6f7f9;
            --fire-color-navy-20: #e5eaf0;
            --fire-color-navy-30: #d4dce7;
            --fire-color-navy-40: #c3cfdd;
            --fire-color-navy-50: #b2c1d4;
            --fire-color-navy-100: #8ea1b9;
            --fire-color-navy-200: #6b829d;
            --fire-color-navy-300: #476282;
            --fire-color-navy-400: #385574;
            --fire-color-navy-500: #2a4865;
            --fire-color-navy-600: #1b3a57;
            --fire-color-navy-700: #0c2d48;
            --fire-color-navy-800: #051e34;
            --fire-color-navy-900: #031525;
            --fire-color-navy: #1b3a57;
            --fire-color-purple-accent-700: #a0f;
            --fire-color-deep-purple-50: #ede7f6;
            --fire-color-deep-purple-500: #673ab7;
            --fire-color-background: #fff;
            --fire-color-canvas-background: #eceff1;
            --fire-color-spinner-background: rgba(255, 255, 255, 0.87);
            --fire-color-overlay-gray-100: rgba(0, 0, 0, 0.04);
            --fire-color-overlay-gray-200: rgba(0, 0, 0, 0.07);
            --fire-color-black-alpha-04: rgba(0, 0, 0, 0.04);
            --fire-color-black-alpha-06: rgba(0, 0, 0, 0.06);
            --fire-color-black-alpha-08: rgba(0, 0, 0, 0.08);
            --fire-color-black-alpha-10: rgba(0, 0, 0, 0.1);
            --fire-color-black-alpha-12: rgba(0, 0, 0, 0.12);
            --fire-color-black-alpha-14: rgba(0, 0, 0, 0.14);
            --fire-color-black-alpha-20: rgba(0, 0, 0, 0.2);
            --fire-color-black-alpha-26: rgba(0, 0, 0, 0.26);
            --fire-color-black-alpha-38: rgba(0, 0, 0, 0.38);
            --fire-color-black-alpha-40: rgba(0, 0, 0, 0.4);
            --fire-color-black-alpha-54: rgba(0, 0, 0, 0.54);
            --fire-color-black-alpha-70: rgba(0, 0, 0, 0.7);
            --fire-color-black-alpha-87: rgba(0, 0, 0, 0.87);
            --fire-color-black-alpha-95: rgba(0, 0, 0, 0.95);
            --fire-color-pure-black: #000;
            --fire-color-white-alpha-04: rgba(255, 255, 255, 0.06);
            --fire-color-white-alpha-12: rgba(255, 255, 255, 0.12);
            --fire-color-white-alpha-14: rgba(255, 255, 255, 0.14);
            --fire-color-white-alpha-15: rgba(255, 255, 255, 0.15);
            --fire-color-white-alpha-20: rgba(255, 255, 255, 0.2);
            --fire-color-white-alpha-30: rgba(255, 255, 255, 0.3);
            --fire-color-white-alpha-38: rgba(255, 255, 255, 0.38);
            --fire-color-white-alpha-50: rgba(255, 255, 255, 0.5);
            --fire-color-white-alpha-70: rgba(255, 255, 255, 0.7);
            --fire-color-white-alpha-87: rgba(255, 255, 255, 0.87);
            --fire-color-white-alpha-95: rgba(255, 255, 255, 0.95);
            --fire-color-pure-white: #fff;
            --fire-color-structural-secondary: #1a73e8;
            --fire-color-structural-secondary-bg: #e8f0fe;
            --fire-color-primary-50: #e8f0fe;
            --fire-color-primary-50-rgb: 232, 240, 254;
            --fire-color-primary-default: #1a73e8;
            --fire-color-primary-default-rgb: 26, 115, 232;
            --fire-color-primary-light: #8ab4f8;
            --fire-color-primary-light-rgb: 138, 180, 248;
            --fire-color-primary-dark: #1967d2;
            --fire-color-primary-dark-rgb: 25, 103, 210;
            --fire-color-black-primary: rgba(0, 0, 0, 0.87);
            --fire-color-black-secondary: rgba(0, 0, 0, 0.54);
            --fire-color-black-icon: rgba(0, 0, 0, 0.54);
            --fire-color-black-note: rgba(0, 0, 0, 0.38);
            --fire-color-black-note-rgb: 0, 0, 0;
            --fire-color-black-disabled: rgba(0, 0, 0, 0.26);
            --fire-color-white-primary: #fff;
            --fire-color-white-secondary: rgba(255, 255, 255, 0.7);
            --fire-color-white-note: rgba(255, 255, 255, 0.5);
            --fire-color-white-disabled: rgba(255, 255, 255, 0.3);
            --fire-color-code-background: rgba(0, 0, 0, 0.06);
            --fire-color-code-default: #37474f;
            --fire-color-code-emphasized: #263238;
            --fire-color-code-deemphasized: #9e9e9e;
            --fire-color-code-highlighted: #d2e3fc;
            --fire-color-black-border: rgba(0, 0, 0, 0.12);
            --fire-color-input-borders: #e0e0e0;
            --fire-color-black-border-10: rgba(0, 0, 0, 0.1);
            --fire-color-black-border-10-1: 1px solid rgba(0, 0, 0, 0.1);
            --fire-color-note: #476282;
            --fire-color-note-rgb: 71, 98, 130;
            --fire-color-note-on-dark: #8ea1b9;
            --fire-color-note-text: #1b3a57;
            --fire-color-note-light: #e5eaf0;
            --fire-color-success: #00796b;
            --fire-color-success-rgb: 0, 121, 107;
            --fire-color-success-on-dark: #00bfa5;
            --fire-color-success-text: #00695c;
            --fire-color-success-light: #e0f2f1;
            --fire-color-error: #d32f2f;
            --fire-color-error-rgb: 211, 47, 47;
            --fire-color-error-on-dark: #ff5252;
            --fire-color-error-text: #c62828;
            --fire-color-error-light: #fbe9e7;
            --fire-color-caution: #ff8f00;
            --fire-color-caution-rgb: 255, 143, 0;
            --fire-color-caution-on-dark: #ffb300;
            --fire-color-caution-text: #bf360c;
            --fire-color-caution-light: #fff3e0;
            --fire-color-tip: var(--fire-color-primary-default);
            --fire-color-tip-rgb: var(--fire-color-primary-default-rgb);
            --fire-color-tip-on-dark: var(--fire-color-primary-default);
            --fire-color-tip-text: var(--fire-color-primary-dark);
            --fire-color-tip-light: var(--fire-color-primary-50);
            --fire-color-badge-bg: #1a73e8;
            --fire-color-badge-font: #fff;
            --fire-color-banner-bg: #476282;
            --fire-color-banner-text: #fff;
            --fire-color-callout: #4c5f77;
            --fire-color-canvas: #f6f7f9;
            --fire-color-canvas-dark: #e5eaf0;
            --fire-color-canvas-heading: #546e7a;
            --fire-color-canvas-link: #1967d2;
            --fire-color-canvas-side-text-xl: #476282;
            --fire-color-canvas-side-text-small: #476282;
            --fire-color-canvas-text: #476282;
            --fire-color-canvas-text-dark: #1b3a57;
            --fire-color-canvasbar-bg: #f6f7f9;
            --fire-color-canvasbar-font: #476282;
            --fire-color-card-actionbar-bg: #f6f7f9;
            --fire-color-card-actionbar-table-bg: #fafafa;
            --fire-color-card-chrome: #fafafa;
            --fire-color-chart-fill: #1a73e8;
            --fire-color-chart-fill-alpha-22: rgba(26, 115, 232, .22);
            --fire-color-chip-app-bg: rgba(0, 0, 0, 0.1);
            --fire-color-chip-filter-bg: #e5eaf0;
            --fire-color-chip-filter-font: #476282;
            --fire-color-chip-filter-hover: #d4dce7;
            --fire-color-chip-filter-icon: #476282;
            --fire-color-chip-font-gray: #4c5f77;
            --fire-color-chip-hover-bg: rgba(0, 0, 0, 0.2);
            --fire-color-chip-plan-bg: rgba(0, 0, 0, 0.1);
            --fire-color-chip-plan-border: rgba(255, 255, 255, 0.5);
            --fire-color-crumbs-bg: #f6f7f9;
            --fire-color-date-picker-hover: #d4dce7;
            --fire-color-date-picker-primary: #476282;
            --fire-color-date-picker-secondary: #6b829d;
            --fire-color-duration-picker: #385574;
            --fire-color-entry-selected: #64b5f6;
            --fire-color-featurebar-bg: transparent;
            --fire-color-featurebar-font: #476282;
            --fire-color-featuretitle-badge-bg: var(--fire-color-primary-default);
            --fire-color-featuretitle-badge-text: #fff;
            --fire-color-filterbar-bg: #f6f7f9;
            --fire-color-footer-bg: #e5eaf0;
            --fire-color-grey-primary: #fafafa;
            --fire-color-header-sm-font: rgba(255, 255, 255, 0.95);
            --fire-color-header-xl-font: #fff;
            --fire-color-history-bg: #e5eaf0;
            --fire-color-listitem-selected: #4285f4;
            --fire-color-master-detail-bg: #fafafa;
            --fire-color-pagination-bg: #fff;
            --fire-color-platform-android: #00bfa5;
            --fire-color-platform-ios: #00b8d4;
            --fire-color-platform-web: #c51162;
            --fire-color-pill-selector-bg: #e5eaf0;
            --fire-color-pill-selector-font: #476282;
            --fire-color-pillar-icon: #476282;
            --fire-color-pillar-hover: #1b3a57;
            --fire-color-pillar-title: #476282;
            --fire-color-popover-bg: #051e34;
            --fire-color-popover-bg-light: #fff;
            --fire-color-primary-footer-bg: #e8eaee;
            --fire-color-primary-header-bg: linear-gradient(to bottom, #1a73e8 50%, #4285f4 100%);
            --fire-color-resource-item-bg: #fafafa;
            --fire-color-resource-item-selected-bg: #e8f0fe;
            --fire-color-resource-item-selected-border: rgba(0, 0, 0, 0.12);
            --fire-color-resource-item-selected-text: #1a73e8;
            --fire-color-resource-selector: #e9ecef;
            --fire-color-resource-selector-bg: #e5eaf0;
            --fire-color-resource-selector-bg-active: #c3cfdd;
            --fire-color-resource-selector-bg-hover: #d4dce7;
            --fire-color-resource-selector-font: #476282;
            --fire-color-resource-selector-icon: #476282;
            --fire-color-search-highlight: #ffd54f;
            --fire-color-sidenav-base: transparent;
            --fire-color-sidenav-bg-default: #051e34;
            --fire-color-sidenav-bg-expanded: #192e40;
            --fire-color-sidenav-bg-selected: rgba(2, 12, 22, .2);
            --fire-color-sidenav-border: #192e40;
            --fire-color-sidenav-expanded-bg: rgba(71, 98, 130, 0.2);
            --fire-color-sidenav-group: transparent;
            --fire-color-sidenav-hover-bg: rgba(71, 98, 130, 0.4);
            --fire-color-sidenav-icontext: #64b5f6;
            --fire-color-sidenav-line: #2a4865;
            --fire-color-sidenav-scrollbar: #051e34;
            --fire-color-sidenav-scrollbarthumb: #6b829d;
            --fire-color-sidenav-scrollbarthumb-hover: #8ea1b9;
            --fire-color-structural-bg: #fafafa;
            --fire-color-tab: #476282;
            --fire-color-tab-selected: var(--fire-color-primary-default);
            --fire-color-tab-underline: #c3cfdd;
            --fire-color-table-bg: #f6f7f9;
            --fire-color-table-footer-bg: #fafafa;
            --fire-color-table-header-bg: #fafafa;
            --fire-color-table-pagination-bg: #fafafa;
            --fire-color-table-row-hover-bg: #eee;
            --fire-color-tablecard-heading: #546e7a;
            --fire-color-titlebadge-bg: #1967d2;
            --fire-color-viewonly-chip-bg: transparent;
            --fire-color-zerostate-canvasheading-font: #476282;
            --fire-color-zerostate-database-bg: #1a73e8;
        }

        .card-body {
            padding: .75rem;
        }

    </style>
</head>

<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/formulario">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos">Listar Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/archivo">Archivos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/image">Fotos</a>
                </li>
            </ul>
        </div>
    </nav>

    <input type="file" id="files" hidden multiple>

    <!-- Header -->
    <div class="col-md-10 offset-md-1 mt-3 bg-transparent">
        <div class="card bg-transparent border-0">
            <div id="upload" class="card-header bg-transparent border-0" style="border-bottom: 0px;">
                <div class="row">
                    <div class="h6 col text-left mt-2">
                        Fotos del proyecto ...
                    </div>
                    <div class="col text-right">
                        <button id="upFile" class="btn btn-primary mr-1"><i class="fa fa-upload"> Subir
                                Foto</i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <a>
                        <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                            src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Franchosdelcaminoreal.files.wordpress.com%2F2013%2F10%2Fdsc_0748.jpg%3Fw%3D1280%26h%3D850&f=1&nofb=1"
                            alt="Card image cap" loading="lazy"></a>
                    <div class="card-body">
                        <p class="card-text text-right mb-1"><small class="text-muted">25/8/2019</small></p>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            <li class="nav-item">
                                <a class="nav-link text-danger" href="#"><i class="fa fa-trash-o"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Descargar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                        src="https://d3te2s0dmhk13a.cloudfront.net/-783662502.jpg" alt="Card image cap" loading="lazy">
                    <div class="card-body">
                        <p class="card-text text-right mb-1"><small class="text-muted">25/8/2019</small></p>
                        <p class="card-text"></p>
                    </div>
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            <li class="nav-item mr-4">
                                <a class="nav-link text-danger" href="#"><i class="fa fa-trash-o"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Descargar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                        src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                        alt="Card image cap" loading="lazy">
                    <div class="card-body">
                        <p class="card-text text-right mb-1"><small class="text-muted">25/8/2019</small></p>
                        <p class="card-text">Lorem ipsum dolor sit amet, Ut enim atrud exercitation ullamco laboris nisi
                            a commodo consequat. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                            et dolore magna aliqua.</p>
                    </div>
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            <li class="nav-item mr-4">
                                <a class="nav-link text-danger" href="#"><i class="fa fa-trash-o"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Descargar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                        src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2F1a%2Fc4%2Fphoto-1410260.jpg!d&f=1&nofb=1"
                        alt="Card image cap" loading="lazy">
                    <div class="card-body">
                        <p class="card-text text-right mb-1"><small class="text-muted">25/8/2019</small></p>
                        <p class="card-text">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                            aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            <li class="nav-item mr-4">
                                <a class="nav-link text-danger" href="#"><i class="fa fa-trash-o"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Descargar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                        src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.nrpSrGiaDnIwpCni3Bo9aQHaMW%26pid%3DApi&f=1"
                        alt="Card image cap" loading="lazy"> 
                    <div class="card-body">
                        <p class="card-text text-right mb-1"><small class="text-muted">25/8/2019</small></p>
                    </div>
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            <li class="nav-item mr-4">
                                <a class="nav-link text-danger" href="#"><i class="fa fa-trash-o"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Descargar</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--Modal Show Image -->
    <div class="modal fade bd-example-modal-lg" id="showImg" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        
        <div class="modal-dialog modal-lg bg-transparent">

            <div class="modal-content bg-transparent border-0">

                <div class="modal-header bg-transparent border-0 p-0 m-0">
                    <button type="button" class="close" style="color: rgb(255, 255, 255);" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close" style="font-size:24px;"></i>
                    </button>
                </div>

                <div class="modal-body text-center border-0">
                    <img id="loadImg" src="" class="img-fluid" alt="Responsive image">
                </div>

            </div>

        </div>
    </div>

    <script>
        /*
        const img = document.getElementsByTagName('img');
        console.log(img);
        [...img].forEach(e => {
            e.addEventListener('dblclick', event => {
                console.log(e.src);
            });
        });
        */
        $(document).ready(function() {
            const imgList = $('img');
            const modal = $('#showImg')
            const imgToModal = $('#loadImg');
            imgList.on('click', event => {
                const clickedImg = $(event.target)[0];
                modal.modal('show');
                imgToModal.attr("src", clickedImg.src);
                console.log(clickedImg.src);
            });
        });

    </script>
</body>

</html>
