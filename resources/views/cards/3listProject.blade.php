<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>🤞 PageLoad.js Example</title>
    <link href="https://www.cssscript.com/wp-includes/css/sticky.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-color: #ccc;
        }

        .container {
            margin: 150px auto;
        }

    </style>
</head>

<body>

    <script defer type="module">
        import PageLoad from "https://cdn.jsdelivr.net/npm/@manz/pageload@1.0.2/lib/pageload.min.js";
        PageLoad.start({
            fakeMode: true,
            fadeIn: false,
            color: "#C4302B",
            spinBarColor: "#C4302B",
            enableProgress: false
        });
        
        document.querySelector(".goto").onclick = () => PageLoad.go(80);
        document.querySelector(".end").onclick = () => PageLoad.done();

    </script>


</body>

</html>
