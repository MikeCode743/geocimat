<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <!---->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icons -->
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

</head>

<body class="bg-light">
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/formulario">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos">Listar Proyectos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/archivo">Archivos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://64.227.86.139/proyectos/image">Fotos</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container mt-3">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-10 m-auto p-0 m-0">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0 p-0 ">
                        <div class="d-flex m-0 p-0">
                            <div>
                                <p class="h3">Repositorio</p>
                            </div>
                            <div class="ml-auto"><button type="button" class="btn btn-primary btn-sm m-0"><i
                                        class="fas fa-upload"></i> Subir Archivo</button></div>
                        </div>
                    </div>
                    <div class="card-body p-0 mt-1">
                        <ul class="nav nav-tabs" style="border-bottom-width: 0px">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Archivos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C3</a>
                            </li>
                        </ul>

                        <ul id="files" class="list-group list-group-flush mt-0">
                            <li class="list-group-item list-group-item-action p-1 pl-2" link="hi carlos4">
                                <div class="d-flex m-0 p-0">
                                    <div class=""><i class="fa fa-book" aria-hidden="true"></i>
                                        Cras justo odio </div>
                                    <div class="delete-file ml-auto"><button type="button" class="btn btn-danger btn-sm m-0"><i
                                                class="far fa-trash-alt"></i></button></div>
                                </div>
                            </li>
                            <li class="list-group-item list-group-item-action p-1 pl-2" link="hi carlos3">
                                <div class="d-flex m-0 p-0">
                                    <div class=""><i class="far fa-file-word"></i>
                                        Cras justo odio </div>
                                    <div class="delete-file ml-auto"><button type="button" class="btn btn-danger btn-sm m-0"><i
                                                class="far fa-trash-alt"></i></button></div>
                                </div>
                            </li>
                            <li class="list-group-item list-group-item-action p-1 pl-2" link="hi carlos2">
                                <div class="d-flex m-0 p-0">
                                    <div class=""><i class="far fa-file-word"></i>
                                        Cras justo odio </div>
                                    <div class="delete-file ml-auto"><button type="button" class="btn btn-danger btn-sm m-0"><i
                                                class="far fa-trash-alt"></i></button></div>
                                </div>
                            </li>
                            <li class="list-group-item list-group-item-action p-1 pl-2" link="hi carlos1">
                                <div class="d-flex m-0 p-0">
                                    <div class=""><i class="far fa-file-word"></i>
                                        Cras justo odio </div>
                                    <div class="delete-file ml-auto"><button type="button" class="btn btn-danger btn-sm m-0"><i
                                                class="far fa-trash-alt"></i></button></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        const fileList = document.getElementById('files');
        for (let i = 0; i < fileList.children.length; i++) {
            const link = fileList.children[i]
            link.addEventListener('click', event => {
                console.log(link.getAttribute('link'));
            });
        }

        const listBtnDelete = document.getElementsByClassName('delete-file');
        for (let i = 0; i < listBtnDelete.length; i++) {
            const btn = listBtnDelete[i];
            btn.addEventListener('click', event => {
                console.log(1);
            });
        }


    </script>

</body>

</html>
