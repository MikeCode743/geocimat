<!DOCTYPE html>
<html>
<head>
    <title>GEOCIMAT - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Scripts -->
    <script src="{{ asset('js\jquery-3.2.1.slim.min.js') }}"></script>
    <script src="{{ asset('js\popper.min.js') }}"></script>
    <script src="{{ asset('js\bootstrap.min.js') }}"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- ICONOS -->
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset ('css\bootstrap.min.css') }}">

    <!-- SELECTPICKER -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <style>
    .jumbotron {
      padding: 25px;
    }
    </style>

</head>
<p></p>
<div class="container-fluid">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ route('home')}}">GEOCIMAT</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- MENU A LA IZQUIERDA -->
    <ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="{{route('personal.home')}}">Gestionar Personal</a>
    </li>
    </ul> 
    <!-- MENU A LA DERECHA -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> Calogero
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar Sesion</a>
        </div>
      </li> 
    </ul>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
    </form>
  </div>
</nav>
<p></p>
<main role="main">
  <div class="jumbotron">
  <div class="card bg-light" style="margin-bottom: 10px">
      <div class="card-header">
      Featured
      </div>  
      <div class="card-body">
        <h5 class="card-title" style="margin-top: 2px">Special title treatment</h5>
        <p class="card-text" style="margin-bottom: 2px">07/24/2018</p>
        <p class="badge badge-pill badge-secondary" style="margin-bottom: 2px">Rocas</p>
        <p class="badge badge-pill badge-secondary" style="margin-bottom: 2px">Pedruscos</p>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Read more</a> <a href="#" class="btn btn-dark">Ver archivos</a>
      </div>
    </div>

  <div class="row">
  <div class="col-sm-6">
    <div class="card bg-light" style="margin-bottom: 10px">
      <div class="card-body">
        <h5 class="card-title" style="margin-bottom: 2px">Special title treatment</h5>
        <p class="card-text" style="margin-bottom: 2px">07/24/2018</p>
        <p class="badge badge-pill badge-secondary" style="margin-bottom: 2px">Rocas</p>
        <p class="badge badge-pill badge-secondary" style="margin-bottom: 2px">Pedruscos</p>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Read more</a> <a href="#" class="btn btn-dark">Ver archivos</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card bg-light" style="margin-bottom: 10px">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <span class="badge badge-pill badge-primary">Primary</span>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
</div>

    </div>
</main>
</div>
</html>

