<div class="col-md-12">
    @if(session()->has('message-store'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message-store') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @elseif(session()->has('message-update'))
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        {{ session()->get('message-update') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @elseif(session()->has('message-info'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session()->get('message-update') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif(session()->has('message-delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session()->get('message-delete') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @elseif(session()->has('message-error'))
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        {{ session()->get('message-error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
</div>

<script type="text/javascript">
    $(".alert").delay(5000).slideUp(200, function() {
                $(this).alert('close');
    });/* tiempo que aparecera la alerts */
</script>