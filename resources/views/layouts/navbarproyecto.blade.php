<!DOCTYPE html>
<html>

<head>
    <title>GEOCIMAT - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ asset('images\favicon-32x32.png') }}" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- ICONOS -->
    <script src="{{ asset('js\icons.js') }}"></script>

    @yield('librerias')


</head>

<div class="container-fluid">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
        <!-- One of the primary actions on mobile is to call a business - This displays a phone button on mobile only -->
        <img src="{{ asset('images/logo-navbar.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        <a class="navbar-brand" href="{{ route('home')}}">GEOCIMAT</a>
        <div class="navbar-toggler-right">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse flex-column " id="navbar">
            {{-- Primer Nivel --}}
            <ul class="navbar-nav mr-auto px-auto">
                @if (Auth::user()->hasPermisos('view-nav-adm'))
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('administracion')}}"><i class='fas fa-cog' style='color:white'></i>
                        Administracion</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('personal.home')}}"><i class='fas fa-user-friends' style='color:white'></i>
                        Personal</a>
                </li>
                @if (Auth::user()->hasPermisos('view-list-all-pj'))
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('proyecto.listado')}}"><i class='fas fa-list' style='color:white'></i> Todos los
                        Proyectos</a>
                </li>
               <li class="nav-item active">
                   <a class="nav-link" href="{{route('proyecto.informes')}}"><i class='fas fa-file-alt' style='color:white'>
                       </i> Informes </a>
               </li>

                @endif
                @endif
                
            </ul>
            {{-- Segundo Nivel --}}
            <ul class="navbar-nav w-100 px-auto">
                {{-- Izquierda --}}
                <ul class="nav navbar-nav mr-auto">
                    @if (Auth::user()->hasPermisos('view-list-pj-me'))
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.crear')}}"><i class='fas fa-map-marker-alt' style='color:white'></i>
                            Nuevo Proyecto</a>
                    </li>
                    @endif
                    @if (Auth::user()->hasPermisos('view-list-pj-me'))
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.misproyectos')}}"><i class='fas fa-archive' style='color:white'></i>
                            Mis proyectos</a>
                    </li>
                    @endif
                    @if (Auth::user()->hasPermisos('view-list-pj'))
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.lista')}}"><i class='far fa-list-alt' style='color:white'></i>
                            Lista de proyectos</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('cronograma.home')}}"><i class='far fa-calendar-alt' style='color:white'></i>
                            Cronograma</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.mapa')}}"><i class='fas fa-map-marked-alt' style='color:white'></i>
                            Mapa de proyectos</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.mensajeria')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-dots" viewBox="0 0 16 16">
                                <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                <path d="M2.165 15.803l.02-.004c1.83-.363 2.948-.842 3.468-1.105A9.06 9.06 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.437 10.437 0 0 1-.524 2.318l-.003.011a10.722 10.722 0 0 1-.244.637c-.079.186.074.394.273.362a21.673 21.673 0 0 0 .693-.125zm.8-3.108a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6c0 3.193-3.004 6-7 6a8.06 8.06 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a10.97 10.97 0 0 0 .398-2z" />
                            </svg>
                            Mensajeria
                        </a>
                    </li>

                    @endif
                </ul>
                {{-- Derecha --}}
                <div class="d-flex justify-content-start">
                    <ul class="navbar-nav px-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('contrasenia.cambiar') }}">Cambiar Contrase&ntilde;a</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar
                                    Sesion</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </ul>
        </div>
    </nav>
    <main role="main">
        <div class="jumbotron bg-transparent p-1">
            @yield('content')
        </div>
    </main>
</div>

</html>