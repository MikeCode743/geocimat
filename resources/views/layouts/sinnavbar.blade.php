<!DOCTYPE html>
<html>

<head>
    <title>GEOCIMAT - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ asset('images\favicon-32x32.png') }}" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- ICONOS -->
    <script src="{{ asset('js\icons.js') }}"></script>

@yield('librerias')


</head>

<div class="container-fluid">
    
    <main role="main">
        <div class="jumbotron bg-transparent p-1">
            @yield('content')
        </div>
    </main>
</div>

</html>