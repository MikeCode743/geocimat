<!DOCTYPE html>
<html>

<head>
    <title>GEOCIMAT - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ asset('images\favicon-32x32.png') }}" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset ('css\bootstrap.min.css') }}">
    <!--Datatable-->
    <link rel="stylesheet" href="{{ asset('css\dataTables.bootstrap4.min.css') }}">
    {{-- SELECTPICKER --}}
    <link rel="stylesheet" href="{{ asset('css\bootstrap-select.min.css') }}">

    @yield('cronograma-css')

    <!-- Scripts -->
    <script src="{{ asset('js\jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('js\popper.min.js') }}"></script>
    <script src="{{ asset('js\bootstrap.min.js') }}"></script>
    <!-- Datatable -->
    <script src="{{ asset('js\jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js\dataTables.bootstrap4.min.js') }}"></script>
    <!-- ICONOS -->
    <script src="{{ asset('js\icons.js') }}"></script>
    <!-- SELECTPICKER -->
    <script src="{{ asset('js\bootstrap-select.min.js') }}"></script>

    @yield('cronograma-js')


</head>

<div class="container-fluid">
    <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
        <!-- One of the primary actions on mobile is to call a business - This displays a phone button on mobile only -->
        <img src="{{ asset('images/logo-navbar.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        <a class="navbar-brand" href="{{ route('home')}}">GEOCIMAT</a>
        <div class="navbar-toggler-right">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse flex-column " id="navbar">
            {{-- Primer Nivel --}}
            <ul class="navbar-nav mr-auto px-auto">
                @if (Auth::user()->hasPermisos('view-nav-adm'))

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('administracion')}}"><i class='fas fa-cog'
                            style='color:white'></i> Administracion</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('personal.home')}}"><i class='fas fa-user-friends'
                            style='color:white'></i> Personal</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('proyecto.listado')}}"><i class='fas fa-list'
                            style='color:white'></i> Todos los Proyectos</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('proyecto.informes')}}"><i class='fas fa-file-alt' style='color:white'>
                        </i> Informes </a>
                </li>
                @endif
            </ul>
            {{-- Segundo Nivel --}}
            <ul class="navbar-nav w-100 px-auto">
                {{-- Izquierda --}}
                <ul class="nav navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.crear')}}"><i class='fas fa-map-marker-alt'

                                style='color:white'></i>
                            Nuevo Proyecto</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.misproyectos')}}"><i class='fas fa-archive'
                                style='color:white'></i>
                            Mis proyectos</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.lista')}}"><i class='far fa-list-alt'
                                style='color:white'></i>
                            Lista de proyectos</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('cronograma.home')}}"><i class='far fa-calendar-alt mr-1'
                                style='color:white'></i>Cronograma</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.mapa')}}"><i class='fas fa-map-marked-alt mr-1' style='color:white'></i>Mapa de proyectos</a>

                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('proyecto.mensajeria')}}"><i class='far fa-comment-dots mr-1' style='color:white'></i>Mensajeria</a>



                    </li>


                </ul>
                {{-- Derecha --}}
                <div class="d-flex justify-content-start">
                    <ul class="navbar-nav px-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('contrasenia.cambiar') }}">Cambiar Contrase&ntilde;a</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar
                                    Sesion</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </ul>
        </div>
    </nav>
    <br>
    <main role="main">
        <div class="jumbotron" style="padding: 25px">
            @yield('content')
        </div>
    </main>
</div>

</html>