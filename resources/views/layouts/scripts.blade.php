<!-- Scripts -->
<script src="{{ asset('js\jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js\popper.min.js') }}"></script>
<script src="{{ asset('js\bootstrap.min.js') }}"></script>
<!-- Datatable -->
<script src="{{ asset('js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js\dataTables.bootstrap4.min.js') }}"></script>
<!-- ICONOS -->
<script src="{{ asset('js\icons.js') }}"></script>
<!-- SELECTPICKER -->
<script src="{{ asset('js\bootstrap-select.min.js') }}"></script>