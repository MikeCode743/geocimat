    {{-- Favicon --}}
    <link rel="icon" type="image/png" href="{{ asset('images\favicon-32x32.png') }}" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset ('css\bootstrap.min.css') }}">
    <!--Datatable-->
    <link rel="stylesheet" href="{{ asset('css\dataTables.bootstrap4.min.css') }}">
    {{-- SELECTPICKER --}}
    <link rel="stylesheet" href="{{ asset('css\bootstrap-select.min.css') }}">

    
