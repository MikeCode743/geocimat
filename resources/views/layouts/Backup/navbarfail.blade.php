<!DOCTYPE html>
<html>

<head>
    <title>GEOCIMAT - @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.css')
    @yield('librerias')
    {{-- <style>
        .jumbotron {
            padding: 25px;
        }
    </style> --}}

    @yield('scriptsimportante')

</head>


<div class="container-fluid">
    <br>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark ">
        <!-- One of the primary actions on mobile is to call a business - This displays a phone button on mobile only -->
        <img src="{{ asset('images/logo-navbar.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
        <a class="navbar-brand" href="{{ route('home')}}">GEOCIMAT</a>
        <div class="navbar-toggler-right">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    
        <div class="collapse navbar-collapse flex-column " id="navbar">
            {{-- Primer Nivel --}}
            <ul class="navbar-nav mr-auto px-auto">

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('administracion')}}"><i class='fas fa-cog' style='color:white'></i> Administracion</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('proyecto.lista')}}"><i class='far fa-list-alt' style='color:white'></i> Lista Proyectos</a>
                </li>

            </ul>
            {{-- Segundo Nivel --}}
            <ul class="navbar-nav w-100 px-auto">
                {{-- Izquierda --}}
                <ul class="nav navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="#">Izquierda <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                {{-- Derecha --}}
                <div class="d-flex justify-content-start">
                    <ul class="navbar-nav px-auto">
                        
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Perfil</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar
                                    Sesion</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </ul>
        </div>
    </nav>
    <br>
    <main role="main">
        <div class="jumbotron" style="padding: 25px">
            @yield('content')
        </div>
    </main>
</div>

@yield('scripts')

@include('layouts.scripts')



</html>