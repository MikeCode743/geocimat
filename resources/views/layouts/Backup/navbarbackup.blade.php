<!DOCTYPE html>
<html>

<head>
  <title>GEOCIMAT - @yield('title')</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset ('css\bootstrap.min.css') }}">
  <!--Datatable-->
  <link rel="stylesheet" href="{{ asset('css\dataTables.bootstrap4.min.css') }}">
  {{-- SELECTPICKER --}}
  <link rel="stylesheet" href="{{ asset('css\bootstrap-select.min.css') }}">

  <!-- Scripts -->
  <script src="{{ asset('js\jquery-3.5.1.js') }}"></script>
  <script src="{{ asset('js\popper.min.js') }}"></script>
  <script src="{{ asset('js\bootstrap.min.js') }}"></script>
  <!-- Datatable -->
  <script src="{{ asset('js\jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js\dataTables.bootstrap4.min.js') }}"></script>
  <!-- ICONOS -->
  <script src="{{ asset('js\icons.js') }}"></script>
  <!-- SELECTPICKER -->
  <script src="{{ asset('js\bootstrap-select.min.js') }}"></script>


  <style>
    .jumbotron {
      padding: 25px;
    }

  </style>

</head>
<p></p>
<div class="container-fluid">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('home')}}">GEOCIMAT</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- MENU A LA IZQUIERDA -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{route('personal.home')}}">Gestionar Personal</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('administracion')}}">Administracion</a>
        </li>
      </ul>
      <!-- MENU A LA DERECHA -->
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> {{ Auth::user()->name }}
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar Sesion</a>
          </div>
        </li>
      </ul>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </div>
  </nav>
  <p></p>
  <main role="main">
    <div class="jumbotron">
      @yield('content')
    </div>
  </main>
</div>

</html>