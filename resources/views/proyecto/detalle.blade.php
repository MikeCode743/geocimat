@extends('layouts.navbarproyecto')

@section('title', 'Detalle')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')

@endsection


@section('content')

@include('layouts.alerts')

<body>
    @if ($proyecto)
    <div class="col-md-8 mx-md-auto mt-3 rounded">

        <div class="card">
            <div class="card-header d-flex">
                <p class="h5 mb-0">{{ $proyecto->nombre }} {{ date_format(date_create($proyecto->fecha_inicio), 'd/m/Y') }}</p>
                <div class="btn-group dropleft ml-auto">
                    <button type="button" class="btn btn-secondary btn-sm dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class='fas fa-wrench'></i>
                    </button>
                    <div class="dropdown-menu">
                        <!-- Dropdown menu links -->
                        <a class="dropdown-item" data-toggle="modal" data-target="#editarModal"><i class='fas fa-edit'></i> Editar Descripcion</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#eliminarModal"><i class='fas fa-trash'></i> Eliminar </a>


                    </div>
                </div>
            </div>
            <div class="card-body pt-2">
                <div class="row text-left">
                    <!-- Columna descripcion proyecto -->
                    <div class="col-md-6">
                        <h5><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">{{ $proyecto->nombres}} {{$proyecto->apellidos}}</span></h5>
                        <h6><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">Tipo: {{ $proyecto->clasificacion }}</span></h6>
                        <h6><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">Etapa: {{ $proyecto->etapa }}</span></h6>
                        <p class="card-text">
                            @if ($proyecto->descripcion)
                            {!! $proyecto->descripcion !!}
                            @else
                            No se a agregado descripci&oacute;n.
                            @endif
                        </p>
                    </div>
                    <!-- Columna imagen -->
                    <div class="coordenada col-md-6 mx-md-auto mt-3 text-right" coordenada="{{ $proyecto->longitud }}, {{ $proyecto->latitud }}">
                        <div class="btn-group btn-group-toggle mb-1 mt-0 p-0" data-toggle="buttons">
                            <button type="button" class="btn btn-primary btn-sm">Satellite</button>
                            <button type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                            <button type="button" class="btn btn-secondary btn-sm">Streets</button>
                        </div>
                        <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{{ $proyecto->longitud }},{{ $proyecto->latitud }},13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg" class="img-fluid" alt="Responsive image">
                    </div>

                </div>
            </div>
            @if (Auth::user()->hasPermisos('view-btn-list'))
            <div class="card-footer text-right">
                <a href="{{ route('proyecto.bitacora', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-info"><i class='fas fa-table mr-2'></i>Bit&aacute;cora</a>
                <a href="{{ route('proyecto.imagen', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-info"><i class='fas fa-images mr-2'></i>Galer&iacute;a</a>
                <a href="{{ route('proyecto.archivos', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-dark"><i class='fas fa-file-archive mr-2'></i>Ver Archivos</a>

            </div>
            @endif
        </div>
    </div>

    <!-- Modal Editar-->
    <div class="modal fade bd-example-modal-lg" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('proyecto.editar',['id' => $proyecto->proyecto_id]) }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ $proyecto->nombre }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- Clasificacion --}}
                        <label class="mt-3">Clasificación</label><sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup>
                        <select name="clasificacion" class="form-control {{$errors->has('clasificacion') ? 'is-invalid' : ''}}" id="clasificacion">
                            @if ($clasificaciones)
                            @foreach ($clasificaciones as $clasificacion)
                            <option value="{{ $clasificacion->clasificacion_id }}" {{($proyecto->clasificacion_id == $clasificacion->clasificacion_id) ? 'selected="true"' : ''}}>
                                {{ $clasificacion->nombre }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('clasificacion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('clasificacion') }}</strong>
                        </span>
                        @endif
                        {{-- Etapa --}}
                        <label class="mt-3">Etapa</label><sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup>
                        <select name="etapa" class="form-control {{$errors->has('etapa') ? 'is-invalid' : ''}}" id="etapa">
                            @if ($etapas)
                            @foreach ($etapas as $etapa)
                            <option value="{{ $etapa->etapa_id }}" {{($proyecto->etapa_id == $etapa->etapa_id) ? 'selected="true"' : ''}}>
                                {{ $etapa->nombre }}</option>
                            @endforeach
                            @endif
                        </select>
                        @if($errors->has('etapa'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('etapa') }}</strong>
                        </span>
                        @endif

                        {{-- Descripcion --}}
                        <div class="form-group  mx-md-auto">
                            <label>Descripci&oacute;n del proyecto</label>
                            <textarea id="informacionAdicional" name="informacionAdicional" class="summernote" value="">{{ $proyecto->descripcion }}</textarea>
                            @if($errors->has('informacionAdicional'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('informacionAdicional') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Eliminar -->
    <div class="modal fade bd-example-modal-lg" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>¡Este proyecto ya no sera visible para los demas!, se eliminara de su lista de proyectos</p>
                        <p>Ya no tendra acceso al contenido del proyecto</p>
                        <p style="color: red">¿Seguro de eliminar el proyecto <b>{{ $proyecto->nombre }}</b> de su lista de proyectos?</p>

                    </div>
                </div>
                <div class="modal-footer">
                    <button id="eliminar" type="button" class="btn btn-danger">Eliminar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
        const Swal = require('sweetalert2');
    </script>
    <script>
        $(() => {
            //Change Image
            const divImage = $(".coordenada");
            divImage.each(function() {
                const coordenada = $(this).attr("coordenada");
                const btnGroup = $(this).children()[0];
                const imgResponsive = $($(this).children()[1]);

                //Click Satellite
                const satelliteBtn = $(btnGroup.children[0]);
                let urlSatellite =
                    "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/" +
                    coordenada +
                    ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                satelliteBtn.click(function() {
                    imgResponsive.attr("src", urlSatellite);
                });

                //Click Outdoors
                const outdoorsteBtn = $(btnGroup.children[1]);
                outdoorsteBtn.click(function() {
                    let urlOutdoors =
                        "https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/static/" +
                        coordenada +
                        ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                    imgResponsive.attr("src", urlOutdoors);
                });

                //Click Streets
                const streetseBtn = $(btnGroup.children[2]);
                streetseBtn.click(function() {
                    let urlStreet =
                        "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" +
                        coordenada +
                        ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                    imgResponsive.attr("src", urlStreet);
                });
            });

            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']]
                    , ['font', ['strikethrough', 'superscript', 'subscript']]
                    , ['fontsize', ['fontsize']]
                    , ['color', ['color']]
                    , ['para', ['ul', 'ol', 'paragraph']]
                    , ['height', ['height']]
                ]
                , placeholder: 'Agregar una descripcion al proyecto'
            , });


        });

        $("#eliminar").click(function() {
            let id = '{{$proyecto->proyecto_id}}';
            const Toast = Swal.mixin({
                toast: true
                , position: 'top-end'
                , showConfirmButton: false
                , timer: 3000
                , timerProgressBar: true
                , didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            axios.post('/proyecto/' + id + '/eliminar', {
                    estado: false
                })
                .then(function(response) {
                    Toast.fire({
                        icon: 'success'
                        , title: response.data.message
                    }).then(function() {
                           window.location.replace('{{ route('proyecto.misproyectos') }}');
                        });
                })
                .catch(e => {
                    console.log('error');
                    Toast.fire({
                        icon: 'error'
                        , title: e.response.data.message
                    });
                });
        });
    </script>
    @else
    <div class="col-md-8 mx-md-auto mt-3 rounded">
        <p class="h3">No hay proyecto</p>
    </div>
    @endif
</body>
@endsection

