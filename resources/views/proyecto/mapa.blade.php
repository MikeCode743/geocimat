@extends('layouts.navbarproyecto')
@section('title', 'Mapa de Proyectos')
@section('librerias')
    @include('proyecto.parcial.librerias.css')
    @include('proyecto.parcial.librerias.script')
@endsection

@section('content')
<div class="col-md-12">
            <div class="row">
                    <h5 class="card-title  float-left" style="color: white">Proyectos GEOCIMAT</h5>
            </div>
            <div id="menu" class="btn-group btn-group-toggle" data-toggle="buttons">
                <button id="satellite" type="button" class="btn btn-primary btn-sm">Satellite</button>
                <button id="outdoors" type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                <button id="streets" type="button" class="btn btn-secondary btn-sm">Streets</button>
            </div>

    <section class="section">
        <b>¡Revisa los proyectos registrados de GEOCIMAT!</b>
        <p>Da clic sobre los marcadores para conocer información general del proyecto:</p><br>    
        <div class="container mb-2" id='map' style='width: 100%; height: 500px;'></div>
    </section>
            
</div>

<script>
    mapboxgl.accessToken = 'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg'
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/satellite-streets-v11',
        center: [-88.85, 13.82], 
        zoom: 8,
    });

    $('#satellite').click(function() {
        map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
     });
    $('#outdoors').click(function() {
         map.setStyle('mapbox://styles/mapbox/outdoors-v11');
    });
    $('#streets').click(function() {
        map.setStyle('mapbox://styles/mapbox/streets-v11');
    });
    
    const listaProyectos = @json($proyectos);
    listaProyectos.forEach(proyecto => {
        const { titulo, nombres, apellidos, clasificacion, etapa, longitud, latitud, descripcion } = proyecto
        new mapboxgl.Marker()
            .setLngLat([longitud, latitud])            
            .setPopup(new mapboxgl.Popup().setHTML(
                "<b>" +titulo+ "</b><br>Encargado: "+nombres+" "+apellidos+"<br>Tipo: "
                +clasificacion+"<br>Etapa: "+etapa+"<br>"+descripcion
                )
                .setMaxWidth("300px")
                )
            .addTo(map);
    })
</script>
@endsection