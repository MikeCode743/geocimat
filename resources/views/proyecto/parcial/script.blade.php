<script>
    $(document).ready(function() {

            const lngInput = document.querySelector('#lng');
            const latInput = document.querySelector('#lat');

            let marker = new mapboxgl.Marker();
            let [lng, lat] = [0, 0];

            const markerInput = () => {

                let [lngValid, latValid] = [false, false];
                let [parseLng, parseLat] = [parseFloat(lngInput.value), parseFloat(latInput.value)]

                //LNG
                if (parseLng == lngInput.value) {
                    if (parseLng >= -180.0 && parseLng <= 180) {
                        lngInput.classList.remove("is-invalid");
                        lngValid = true;
                    } else {
                        lngInput.classList.add("is-invalid");
                    }
                } else {
                    lngInput.classList.add("is-invalid");
                }

                //LAT
                if (parseLat == latInput.value) {
                    if (parseLat >= -90.0 && parseLat <= 90.0) {
                        latInput.classList.remove("is-invalid");
                        latValid = true;
                    } else {
                        latInput.classList.add("is-invalid");
                    }
                } else {
                    latInput.classList.add("is-invalid");
                }

                //Add Marker
                if (lngValid && lngValid) {
                    marker.setLngLat([parseLng, parseLat]).addTo(map);
                } else {
                    marker.remove();
                }

                return lngValid && lngValid;

            }

            //MapBox
            mapboxgl.accessToken =
                'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg';

            let map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/satellite-streets-v11',
                center: [-88.85, 13.82], // starting position
                zoom: 7 // starting zoom
            });

            map.on('click', function(e) {
                latInput.classList.remove("is-invalid");
                lngInput.classList.remove("is-invalid");
                $("#coordenadas").val(JSON.stringify(e.lngLat.wrap()));
                [lng, lat] = [e.lngLat.wrap().lng, e.lngLat.wrap().lat];
                lngInput.value = lng.toFixed(8);
                latInput.value = lat.toFixed(8);
                marker.setLngLat([lng, lat]).addTo(map);
            });

            $('#satellite').click(function() {
                map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
            });
            $('#outdoors').click(function() {
                map.setStyle('mapbox://styles/mapbox/outdoors-v11');
            });
            $('#streets').click(function() {
                map.setStyle('mapbox://styles/mapbox/streets-v11');
            });

            $("#lng").keyup(function(event) {
                markerInput();
            });

            $("#lat").keyup(function() {
                markerInput();
            });

            //Datepicker
            $("#datepicker").datepicker({
                inline: true,
                format: 'yyyy/mm/dd',
                todayBtn: true,
                todayHighlight: true,
                orientation: 'top right',
            }).datepicker("setDate", 'now');

            /* PARA EVITAR QUE EL USUARIO INGRESE UNA FECHA MENOR A LA ACTUAL */
            $("#datepicker").change(function(){
                var fechaActual = new Date();
                var fechaSeleccionada = $("#datepicker").datepicker('getDate');
                
                if(fechaActual > fechaSeleccionada){
                    $("#datepicker").datepicker('setDate', fechaActual);
                }
            });

            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                placeholder: 'Ej. Objetivo del proyecto',
            });

            $('.summernote').summernote('code', '{!!old('informacionAdicional')!!}');

            //SweetAlert
            showMessage = (message, icon) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })
            };

        });
    
        /* PARA QUE NO SE ENVIEN DATOS AL PRESIONAR ENTER */
        document.addEventListener('DOMContentLoaded', () => {
          document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
            if(e.keyCode == 13) {
              e.preventDefault();
            }
          }))
        });

</script>