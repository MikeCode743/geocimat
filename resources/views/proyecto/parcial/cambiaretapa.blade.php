<div class="modal fade" id="modal-cambiar-etapa" tabindex="-1" aria-labelledby="etapaModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambiar Etapa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="cambiar-etapa-form" method="post" action="">
      {{-- <form id="cambiar-etapa-form"> --}}
        <div class="modal-body">
          
            {{ csrf_field() }}
            <div class="form-group">            
              <label for="select-etapas">Seleccione la etapa del proyecto</label>            
              <select id="select-etapas" class="form-control" name="etapa" value="">
              @foreach($etapas as $etapa)            
                <option value="{{$etapa->etapa_id}}">{{$etapa->nombre_corto}}</option>
              @endforeach            
              </select>
            </div>          
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button id="btn-aceptar-etapa" type="submit" class="btn btn-primary" data-ruta="#">Cambiar</button>
          {{-- <button id="btn-aceptar-etapa" type="button" class="btn btn-primary" data-ruta="#">Cambiar</button> --}}
        </div>
      </form>
    </div>
  </div>
</div>