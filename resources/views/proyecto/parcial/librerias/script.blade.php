    {{-- JQuery --}}
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
    {{-- JQuery UI --}}
    <link rel="stylesheet" href="jquery-ui.min.css">
    <script src="external/jquery/jquery.js"></script>
    <script src="jquery-ui.min.js"></script>
    {{-- Poopper --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    {{-- Bootstrap --}}
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    {{-- bootstrap - Datepicker --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="crossorigin="anonymous"></script>
    {{-- summernote --}}
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- Loader  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <!-- Revisar las alertas que muestra en consola -->
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
                new QueryLoader2(document.querySelector("body"), {
                    barColor: "#efefef",
                    backgroundColor: "#111",
                    percentage: true,
                    barHeight: 1,
                    minimumTime: 200,
                    fadeOutTime: 1000
                });
            });
    
    </script>
    <!-- MapBox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js'></script>
    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
    {{-- Datatable --}}
    <script src="{{ asset('js\jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js\dataTables.bootstrap4.min.js') }}"></script>