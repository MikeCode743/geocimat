
    {{--Bootstrap  --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Datepicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
    <!-- Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <!-- MapBox -->
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css' rel='stylesheet' />
    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <!--Datatable-->
    <link rel="stylesheet" href="{{ asset('css\dataTables.bootstrap4.min.css') }}">
    
    <style>
    .note-editable {
        height: 250px;
        background-color: #fff;
    }
    body {
        font-family: "proxima-nova", "proxima nova", "helvetica neue", "helvetica", "arial", sans-serif;
        background-color: rgb(255, 255, 255);
    }
    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #0e2294;
        outline: 0;
        box-shadow: 0 0 0 0rem rgba(0, 123, 255, 0.25);
    }
    </style>