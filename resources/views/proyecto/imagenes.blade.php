@extends('layouts.navbarproyecto')

@section('title', 'Imagenes Proyecto')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')

@endsection


@section('content')

<body>
    <!-- Header -->
    <div class="col-md-10 offset-md-1 mt-3 bg-transparent">
        <div class="card bg-transparent border-0">
            <div id="upload" class="card-header bg-transparent border-0" style="border-bottom: 0px;">
                <div class="row">
                    <div class="h6 col text-left mt-2">
                        <p class="font-weight-bold">Fotos del proyecto {{ $id }}</p>
                    </div>
                    <div class="col text-right">
                        @if (Auth::user()->proyectoarchivos($id))
                        <a class="btn btn-light" href="{{ route('imagen.papelera', ['id' => $id]) }}" role="button">
                            <i class='fas fa-trash-restore'></i> Papelera</a>
                        @endif
                        @if (Auth::user()->hasPermisos('view-doc-pj'))
                        <a class="btn btn-secondary" href="{{ route('proyecto.archivos', ['id' => $id]) }}"
                            role="button">Documentos</a>
                        @endif
                        @if (Auth::user()->proyectoarchivos($id))
                            @if (Auth::user()->hasPermisos('add-img-pj'))
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#photoModal">
                                Subir Fotografía
                                </button>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.alerts')

    @if (Auth::user()->hasPermisos('add-img-pj'))
    <!-- POST Agregar Imagen -->
    <div class="modal fade bd-example-modal-lg" id="photoModal" tabindex="-1" role="dialog"
        aria-labelledby="photoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Fotografía</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input id="idProyecto" type="hidden" name="idProyecto" value="{{ $id }}">
                    <div class="form-group">
                        <input id="ArrayImagenes" name="file[]" type="file" class="form-control-file" multiple>
                    </div>
                    <div class="form-group mt-3">
                        <label for="exampleFormControlTextarea1">Descripci&oacute;n</label>
                        <textarea class="form-control" id="descripcion" rows="3" name="descripcion"></textarea>
                    </div>
                </div>
                <div id="progress" class="progress">
                    <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%"
                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="modal-footer">
                    <button id="cancelarPost" type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cerrar</button>
                    <button id="subirImagenesPost" type="button" class="btn btn-primary">Agregar</button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if (Auth::user()->hasPermisos('del-img-pj'))
    <!-- POST Eliminar Imagen -->
    <div class="modal fade bd-example-modal-lg" id="eliminarImagen" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('proyecto.imagenes.eliminar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label>¿Esta seguro de eliminar la imagen?</label>
                        <input type="hidden" name="idProyecto" value="{{ $id }}">
                        <input type="hidden" id="rutaEliminar" name="nombreimagen" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    <div class="container">
        @if(Session::has('error_eliminar_imagen'))
            <div class="alert alert-danger col-md-8 mx-md-auto" role="alert">{{ Session::get('error_eliminar_imagen') }}
            </div>
        @endif
        <div class="row">            
            @if (count($listaImagenes) === 0)
                <div class="col">
                    <p>No hay imagenes en la papelera.</p>
                </div>
            @else
                @foreach ($listaImagenes as $imagen)
                    <div class="col-sm-6 col-lg-3 py-2">
                        <div class="card h-100">
                            <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                                src="{{ route('repository', ['ruta' => $imagen->ruta_imagen]) }}" alt="Card image cap"
                                loading="lazy">
                            @if ($imagen->descripcion)
                                <div class="card-body p-2">
                                    <p class="card-text">{{ $imagen->descripcion }}</p>
                                </div>
                            @else
                                <div class="card-body p-0">
                                </div>
                            @endif
                            <div class="card-footer px-0 py-0">
                                <ul class="nav justify-content-between">
                                    @if (Auth::user()->proyectoarchivos($id))
                                    <li class="listaEliminar nav-item" nombreimagen="{{ $imagen->nombre }}">
                                        <a class="nav-link text-danger" data-toggle="modal"
                                            data-target="#eliminarImagen"><i class="far fa-trash-alt"></i></a>
                                    </li>
                                    @endif
                                        <a class="nav-link"
                                            href="{{ route('repository', ['ruta' => $imagen->ruta_imagen]) }}"
                                            download>Descargar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <!--Modal Show Image -->
    <div class="modal fade bd-example-modal-lg" id="showImg" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg bg-transparent">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-header bg-transparent border-0 p-0 m-0">
                    <button type="button" class="close" style="color: rgb(255, 255, 255);" data-dismiss="modal"
                        aria-label="Close">
                        <i class="fas fa-window-close" style="font-size:24px;"></i>
                    </button>
                </div>
                <div class="modal-body text-center border-0">
                    <img id="loadImg" src="" class="img-fluid" alt="Responsive image" loading="lazy">
                </div>
            </div>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        $(document).ready(function() {
            $("#progress").hide();
            const imgList = $('img');
            const modal = $('#showImg')
            const imgToModal = $('#loadImg');
            imgList.on('click', event => {
                const clickedImg = $(event.target)[0];
                modal.modal('show');
                imgToModal.attr("src", clickedImg.src);
                console.log(clickedImg.src);
            });

            //direccion eliminar
            const listaEliminar = $('.listaEliminar');
            listaEliminar.each(function(index) {
                $(this).click(function() {
                    const relativa = $(this).attr("nombreimagen");
                    $('#rutaEliminar').val(relativa);
                });
            });

            //submit form
            $('#subirImagenesPost').click(function() {
                $("#progress").show();
                $("#subirImagenesPost").attr('disabled', true);
                const token = document.getElementsByName('_token')[0].value;
                const idProyecto = document.getElementsByName('idProyecto')[0].value;
                const arrayImagenes = document.getElementById('ArrayImagenes');
                const descripcion = document.getElementById('descripcion').value;
                let bodyData = new FormData();
                bodyData.append('_token', token);
                bodyData.append('idProyecto', idProyecto);
                bodyData.append('descripcion', descripcion);
                for (const file of arrayImagenes.files) {
                    bodyData.append('file[]', file, file.name);
                }

                axios.post("{{ route('proyecto.imagenes.crear') }}", bodyData, {
                        onUploadProgress: uploadEvent => {
                            let progress = Math.round(uploadEvent.loaded /
                                uploadEvent.total * 100);
                            $("#progressbar").css("width", progress + '%');
                        }
                    })
                    .then(res => {
                        const status = res.status;
                        const data = res.data;
                        if (status == 200) {
                            notificacion(data.message +
                                ', para visualizar los cambios debe actualizar la página',
                                'success');
                        } else if (status == 500) {
                            notificacion(data.message, 'error');
                        } else {
                            notificacion("Error Inesperado", 'error');
                        }
                    }).catch(function(error) {
                        console.log(error);
                        notificacion("Error Inesperado revisar log", 'error');
                    }).then(function() {
                        $("#progress").hide();
                        document.getElementById('ArrayImagenes').value = "";
                        document.getElementById('descripcion').value = "";
                        arrayImagenes.value = "";
                        $("#subirImagenesPost").removeAttr('disabled');
                        $("#progressbar").css("width", '0%');
                    });

            });
            $('#cancelarPost').click(function() {
                document.getElementById('ArrayImagenes').value = "";
                document.getElementById('descripcion').value = "";
            });
            const notificacion = (mensaje, icono) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icono,
                    text: mensaje,
                }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
            }
        });

    </script>
</body>

@endsection