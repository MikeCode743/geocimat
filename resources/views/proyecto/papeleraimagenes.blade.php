@extends('layouts.navbarproyecto')

@section('title', 'Papelera Imagenes')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')

@endsection


@section('content')

<body>
    <!-- Header -->
    <div class="col-md-10 offset-md-1 mt-3 bg-transparent">
        <div class="card bg-transparent border-0">
            <div id="upload" class="card-header bg-transparent border-0" style="border-bottom: 0px;">
                <div class="row">
                    <div class="h6 col text-left mt-2">
                        <p class="font-weight-bold">Fotos del proyecto {{ $id }}</p>
                    </div>
                    <div class="col text-right">
                        @if (Auth::user()->hasPermisos('view-img-pj'))
                        <a class="btn btn-secondary" href="{{ route('proyecto.imagen', ['id' => $id]) }}" role="button">Imagenes</a>
                        @endif
                        @if (Auth::user()->hasPermisos('view-doc-pj'))
                        <a class="btn btn-secondary" href="{{ route('proyecto.archivos', ['id' => $id]) }}" role="button">Documentos</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
    @if (Auth::user()->hasPermisos('del-img-pj'))
    <!-- POST Eliminar Imagen -->
    <div class="modal fade bd-example-modal-lg" id="eliminarImagen" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('imagen.papelera.eliminar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label>¿Esta seguro de eliminar la imagen?</label>
                        <input type="hidden" name="idProyecto" value="{{ $id }}">
                        <input type="hidden" id="rutaEliminar" name="nombreimagen" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    @if (Auth::user()->hasPermisos('del-img-pj'))
    <!-- POST Restaurar Imagen -->
    <div class="modal fade bd-example-modal-lg" id="restaurarImagen" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelrestaurar">Restaurar Imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('imagen.papelera.restaurar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label>¿Esta seguro de restaurar la imagen?</label>
                        <input type="hidden" name="idProyecto" value="{{ $id }}">
                        <input type="hidden" id="rutaEliminarRestaurar" name="nombreimagenRestaurar" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Restaurar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    <div class="container">
        <div class="row">
            @if (count($listaImagenes) === 0)
            <div class="col">
                <p>A&uacute;n no se han agregado imagenes.</p>
            </div>
            @else
            @foreach ($listaImagenes as $imagen)
            <div class="col-sm-6 col-lg-3 py-2">
                <div class="card h-100">
                    <img class="card-img-top img-fluid" style="width: auto; height: 15rem;"
                        src="{{ route('repository', ['ruta' => $imagen->ruta_imagen]) }}" alt="Card image cap"
                        loading="lazy">
                    @if ($imagen->descripcion_image)
                    <div class="card-body p-2">
                        <p class="card-text">{{ $imagen->descripcion }}</p>
                    </div>
                    @else
                    <div class="card-body p-0">
                    </div>
                    @endif
                    <div class="card-footer px-0 py-0">
                        <ul class="nav justify-content-between">
                            @if (Auth::user()->proyectoarchivos($id))
                            <li class="listaEliminar nav-item" nombreimagen="{{ $imagen->nombre }}">
                                <a class="nav-link text-danger" data-toggle="modal" data-target="#eliminarImagen"><i
                                        class="far fa-trash-alt"></i></a>
                            </li>
                            <li class="listaRestaurar nav-item" nombreimagenRestaurar="{{ $imagen->nombre }}">
                                <a class="nav-link text-success" data-toggle="modal" data-target="#restaurarImagen"><i
                                        class="fas fa-trash-restore"> Recuperar</i></a>
                            </li>
                            @endif
                            
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>

    <!--Modal Show Image -->
    <div class="modal fade bd-example-modal-lg" id="showImg" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg bg-transparent">
            <div class="modal-content bg-transparent border-0">
                <div class="modal-header bg-transparent border-0 p-0 m-0">
                    <button type="button" class="close" style="color: rgb(255, 255, 255);" data-dismiss="modal"
                        aria-label="Close">
                        <i class="fas fa-window-close" style="font-size:24px;"></i>
                    </button>
                </div>
                <div class="modal-body text-center border-0">
                    <img id="loadImg" src="" class="img-fluid" alt="Responsive image" loading="lazy">
                </div>
            </div>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        $(document).ready(function() {
            $("#progress").hide();
            const imgList = $('img');
            const modal = $('#showImg')
            const imgToModal = $('#loadImg');
            imgList.on('click', event => {
                const clickedImg = $(event.target)[0];
                modal.modal('show');
                imgToModal.attr("src", clickedImg.src);
                console.log(clickedImg.src);
            });

            //direccion eliminar
            const listaEliminar = $('.listaEliminar');
            listaEliminar.each(function(index) {
                $(this).click(function() {
                    const relativa = $(this).attr("nombreimagen");
                    $('#rutaEliminar').val(relativa);
                });
            });
            //direccion restaurar
            const listaRestaurar = $('.listaRestaurar');
            listaRestaurar.each(function(index) {
                $(this).click(function() {
                    const relativa = $(this).attr("nombreimagenRestaurar");
                    $('#rutaEliminarRestaurar').val(relativa);
                });
            });


            const notificacion = (mensaje, icono) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icono,
                    text: mensaje,
                }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
            }
        });

    </script>
</body>

@endsection