@extends('layouts.sinnavbar')

@section('title', 'Mapa de Proyectos')

@section('librerias')
<link rel="stylesheet" href="{{ asset ('css\bootstrap.min.css') }}">
<script src="{{ asset('js\jquery-3.5.1.js') }}"></script>
<script src="{{ asset('js\popper.min.js') }}"></script>
<script src="{{ asset('js\bootstrap.min.js') }}"></script>
<script src="{{ asset('js\icons.js') }}"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('content')

<nav class="navbar navbar-light bg-dark">
    <a href="{{ route('login') }}" class="align-middle">
        <img src="{{ asset('images/logo-navbar.png') }}" width="35" height="30" class="" alt="">
        <span style="color: white">GEOCIMAT</span>
    </a>
    <a href="{{ route('login') }}">
        
        <i class='fas fa-key' style="color: white"></i>
        <span class="" style="color: white">Acceder</span>
    </a>
</nav>
<div class="col-md-12">
            <div class="row">
                    <h5 class="card-title  float-left" style="color: white">Proyectos GEOCIMAT</h5>
            </div>
            <div id="menu" class="btn-group btn-group-toggle" data-toggle="buttons">
                <button id="satellite" type="button" class="btn btn-primary btn-sm">Satellite</button>
                <button id="outdoors" type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                <button id="streets" type="button" class="btn btn-secondary btn-sm">Streets</button>
            </div>
    <section class="section">
        <b>¡Revisa los proyectos registrados de GEOCIMAT!</b>
        <p>Da clic sobre los marcadores para conocer información general del proyecto:</p><br>    
        <div class="container mb-2" id='map' style='width: 100%; height: 500px;'></div>
    </section>       
</div>

<script>
    mapboxgl.accessToken = 'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg'
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/satellite-streets-v11',
        center: [-88.85, 13.82], 
        zoom: 8,
    });
    $('#satellite').click(function() {
        map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
     });
    $('#outdoors').click(function() {
         map.setStyle('mapbox://styles/mapbox/outdoors-v11');
    });
    $('#streets').click(function() {
        map.setStyle('mapbox://styles/mapbox/streets-v11');
    });    
    const listaProyectos = @json($proyectos);
    listaProyectos.forEach(proyecto => {
        const { titulo, nombres, apellidos, clasificacion, etapa, longitud, latitud, descripcion } = proyecto
        new mapboxgl.Marker()
            .setLngLat([longitud, latitud])            
            .setPopup(new mapboxgl.Popup().setHTML(
                "<b>" +titulo+ "</b><br>Encargado: "+nombres+" "+apellidos+"<br>Tipo: "
                +clasificacion+"<br>Etapa: "+etapa+"<br>"+descripcion
                )
                .setMaxWidth("300px")
                )
            .addTo(map);
    })
</script>

@endsection