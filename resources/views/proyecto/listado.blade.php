@extends('layouts.navbarproyecto')

@section('title', 'Lista Proyecto')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')
@endsection

@section('content')
<body>
    <div class="row col-md-8 mx-md-auto m-0 p-0">
        <div class="col">
            <p class="h1 mt-3">Proyectos</p>
        </div>
    </div>

    <div class="form-row col-md-8 mx-md-auto">
        <div class="col-md-12">
            <label for="inputFiltrar">Buscar proyecto</label>
            <input id="inputFiltrar" name="inputFiltrar" type="text" class="form-control">
            <small class="text-muted">
                Texto o fecha (dd/mm/yyyy)
            </small>
        </div>        
    </div>
    @if ($proyectos)
    @foreach ($proyectos as $proyecto)
    <div class="col-md-8 mx-md-auto mt-3 rounded">
        <div class="card text-center">
            <div class="card-header">
                <p class="h5 mb-0">{{ $proyecto->nombre }}
                    {{ date_format(date_create($proyecto->fecha_inicio), 'd/m/Y') }}</p>
            </div>
            <div class="card-body pt-2">
                <div class="row text-left">
                    <!-- Columna descripcion proyecto -->
                    <div class="col-md-6">
                        <h5><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">{{ $proyecto->nombres}} {{$proyecto->apellidos}}</span></h5>
                        <h6><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">Tipo: {{ $proyecto->clasificacion }}</span></h6>
                        <h6><span class="badge badge-pill badge-light" style="background-color: #CDCDCD">Etapa: {{ $proyecto->etapa }}</span></h6>
                        <p class="card-text">
                            @if ($proyecto->descripcion)
                            {!! $proyecto->descripcion !!}
                            @else
                            No se a agregado descripci&oacute;n.
                            @endif
                        </p>
                    </div>
                    <!-- Columna imagen -->
                    <div class="coordenada col-md-6 mx-md-auto mt-3 text-right" coordenada="{{ $proyecto->longitud }}, {{ $proyecto->latitud }}">
                        <div class="btn-group btn-group-toggle mb-1 mt-0 p-0" data-toggle="buttons">
                            <button type="button" class="btn btn-primary btn-sm">Satellite</button>
                            <button type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                            <button type="button" class="btn btn-secondary btn-sm">Streets</button>
                        </div>
                        <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{{ $proyecto->longitud }},{{ $proyecto->latitud }},13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg" class="img-fluid" alt="Responsive image">
                    </div>

                </div>
            </div>
            @if (Auth::user()->hasPermisos('view-btn-list'))
            <div class="card-footer text-right">
                @if (Auth::user()->proyectoarchivos($proyecto->proyecto_id))
                <a href="{{ route('proyecto.bitacora', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-info"><i class='fas fa-table mr-2'></i>Bit&aacute;cora</a>
                @endif
                <a href="{{ route('proyecto.imagen', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-info"><i class='fas fa-images mr-2'></i>Galer&iacute;a</a>
                <a href="{{ route('proyecto.archivos', ['id' => $proyecto->proyecto_id]) }}" class="btn btn-dark"><i class='fas fa-file-archive mr-2'></i> Archivos</a>
            </div>
            @endif
        </div>
    </div>
    @endforeach
    @else
    <div class="col-md-8 mx-md-auto mt-3 rounded">
        <p class="h3">No hay proyecto</p>
    </div>
    @endif

    <script>
        $(() => {
            $("#inputFiltrar").keyup(function(event) {
                const valorInput = $(this).val();
                const regexp = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$/
                const regexp2 = /\d{2}([/])\d{2}\1\d{4}$/
                const proyectosDisponibles = $('.card')
                proyectosDisponibles.show();

                if (valorInput.match(regexp)) {
                    const [dia, mes, anio] = valorInput.split('/')
                    if (new Date(anio, mes - 1, dia) === 'Invalid Date') {
                        return
                    }
                    proyectosDisponibles.each(function() {
                        const inputDate = new Date(anio, mes - 1, dia)
                        const innerText = $(this).find('.card-header .h5')[0].innerText
                        const [diaCard, mesCard, anioCard] = innerText.match(regexp2)[0].split('/')
                        const fechaCard = new Date(anioCard, mesCard - 1, diaCard)
                        if (fechaCard.getTime() > inputDate.getTime()) {
                            $(this).hide();
                        }
                    });
                    return
                }

                if (valorInput) {
                    const regex = new RegExp(valorInput, 'i')
                    proyectosDisponibles.each(function() {
                        const header = $(this).find('.card-header .h5')[0].innerText
                        const body = $(this).find('.card-body .row .col-md-6')[0].innerText
                        const dataCard = header + ' ' + body
                        if (!regex.test(dataCard)) {
                            $(this).hide();
                        }
                    });
                }
            });

            //Change Image
            const divImage = $(".coordenada");
            divImage.each(function() {
                const coordenada = $(this).attr("coordenada");
                const btnGroup = $(this).children()[0];
                const imgResponsive = $($(this).children()[1]);
                const tokenMapBox = ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                //Click Satellite
                const satelliteBtn = $(btnGroup.children[0]);

                const urlSatellite =
                    "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/" + coordenada + tokenMapBox
                satelliteBtn.click(function() {
                    imgResponsive.attr("src", urlSatellite);
                });

                //Click Outdoors
                const outdoorsteBtn = $(btnGroup.children[1]);
                outdoorsteBtn.click(function() {
                    const urlOutdoors =
                        "https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/static/" + coordenada + tokenMapBox
                    imgResponsive.attr("src", urlOutdoors);
                });

                //Click Streets
                const streetseBtn = $(btnGroup.children[2]);
                streetseBtn.click(function() {
                    const urlStreet =
                        "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" + coordenada + tokenMapBox
                    imgResponsive.attr("src", urlStreet);
                });
            });
        });
    </script>
</body>
@endsection