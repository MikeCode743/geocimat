@extends('layouts.navbarproyecto')

@section('title', 'Archivos de Proyecto')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')
@endsection

@section('content')

<body>

    @if (Auth::user()->hasPermisos('add-doc-pj'))
    <!-- POST AGREGAR ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="archivo" tabindex="-1" role="dialog"
        aria-labelledby="photoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="idProyecto" value="{{ $id }}">
                    <div class="form-group">
                        <input name="file[]" type="file" class="form-control-file" id="ArrayArchivos" multiple>
                    </div>
                    <!--AQUI DEBE IR EL TIPO DE ARCHIVO -->
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Seleccionar Grupo</label>
                        <select class="form-control" id="idGrupo">
                            @foreach ($listaGrupo as $grupo)
                            <option value="{{ $grupo->grupo_id }}">{{ $grupo->coleccion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div id="progress" class="progress">
                    <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%"
                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="modal-footer">
                    <button id="cancelarPost" type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cerrar</button>
                    <button id="subirArchivosPost" type="button" class="btn btn-primary">Agregar</button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if (Auth::user()->hasPermisos('del-doc-pj'))
    <!-- POST Eliminar ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="eliminarDocumento" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('proyecto.archivos.eliminar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label id="mensajeEliminar" class="text-danger">¿Esta seguro de eliminar el
                            documento?</label>
                        <input name="idProyecto" value="{{ $id }}" type="hidden">
                        <input name="nombreDocumento" id="nombreDocumento" value="" type="hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif

    <div class="container mt-3">
        @if(Session::has('error_eliminar_archivo'))
            <div class="alert alert-danger col-md-8 mx-md-auto" role="alert">{{ Session::get('error_eliminar_archivo') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 col-sm-12 col-md-10 m-auto p-0 m-0">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0 p-0 ">
                        <div class="d-flex m-0 p-0">
                            <div>
                            <p class="h3">Repositorio </p>
                            </div>
                            <div class="col text-right">
                                @if (Auth::user()->proyectoarchivos($id))
                                <a class="btn btn-light" href="{{ route('archivo.papelera', ['id' => $id]) }}"
                                    role="button">
                                    <i class='fas fa-trash-restore'></i> Papelera</a>
                                @endif
                                @if (Auth::user()->hasPermisos('view-img-pj'))
                                <a class="btn btn-secondary" href="{{ route('proyecto.imagen', ['id' => $id]) }}"
                                    role="button">Imagenes</a>
                                @endif
                                @if (Auth::user()->proyectoarchivos($id))
                                @if (Auth::user()->hasPermisos('view-list-pj'))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#archivo">
                                    <i class="fas fa-upload"></i> Subir Documento
                                </button>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    @if (Auth::user()->hasPermisos('view-doc-pj'))
                    <!-- Filtro Archivos Subidos -->
                    <ul class="nav nav-tabs" id="filtroLista">
                        <li class="nav-item">
                            <a class="nav-link active bg-light" grupo="-1">Todos</a>
                        </li>
                        @foreach ($listaGrupo as $grupo)
                        <li class="nav-item">
                            <a class="nav-link" grupo="{{ $grupo->grupo_id }}">{{ $grupo->coleccion }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- FIN Filtro Archivos Subidos -->
                    <div class="card-body p-0 mt-2">
                        @if (count($listaArchivos) === 0)
                        <p>A&uacute;n no se han agregado archivos.</p>
                        @else
                        <ul id="listaArchivos" class="list-group list-group-flush mt-0">
                            @foreach ($listaArchivos as $archivo)
                            <li class="list-group-item list-group-item-action p-1 pl-2"
                                grupo="{{ $archivo->grupo_id }}">
                                <div class="d-flex m-0 p-0">
                                    <div class="open-file w-100"
                                        link="{{ route('repository', ['ruta' => $archivo->ruta_archivo]) }}">
                                        @if (isset($listaIconos[$archivo->extension]))
                                        <i class="{{ $listaIconos[$archivo->extension] }}"></i>
                                        @else
                                        <i class="fas fa-file-contract"></i>
                                        @endif
                                        {{ $archivo->nombre }}
                                    </div>
                                    @if (Auth::user()->proyectoarchivos($id))
                                    <div eliminarArchivo="{{ $archivo->nombre }}" class="delete-file ml-auto">
                                        <button type="button" class="btn btn-danger btn-sm m-0"><i
                                                class="far fa-trash-alt"></i></button>
                                    </div>
                                    @endif
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        const fileList = document.getElementsByClassName('open-file');
            for (let i = 0; i < fileList.length; i++) {
                const link = fileList[i]
                link.addEventListener('click', event => {
                    const filelink = link.getAttribute('link')
                    window.open(filelink, '_blank')
                });
            }
            const listBtnDelete = document.getElementsByClassName('delete-file');
            for (let i = 0; i < listBtnDelete.length; i++) {
                const btn = listBtnDelete[i];
                btn.addEventListener('click', event => {
                    const nombreDocumento = btn.getAttribute('eliminarArchivo');
                    const nombreSinFecha = nombreDocumento.slice(nombreDocumento.indexOf("-") + 1);
                    btn.children[0].setAttribute('data-toggle', 'modal');
                    btn.children[0].setAttribute('data-target', '#eliminarDocumento');
                    console.log(btn.getAttribute('eliminarArchivo'));
                    document.getElementById('nombreDocumento').value = nombreDocumento;
                    document.getElementById('mensajeEliminar').innerHTML =
                        `Es posible que esté eliminando datos importantes del proyecto. Después de eliminar ${nombreSinFecha}, no se puede recuperar.`;
                });
            }

            $(document).ready(function() {
                $("#progress").hide();
                $('#subirArchivosPost').click(function() {
                    $("#progress").show();
                    $("#subirArchivosPost").attr('disabled', true);
                    const token = document.getElementsByName('_token')[0].value;
                    const idProyecto = document.getElementsByName('idProyecto')[0].value;
                    const idGrupo = document.getElementById('idGrupo').value;
                    const ArrayArchivos = document.getElementById('ArrayArchivos');
                    let bodyData = new FormData();
                    bodyData.append('_token', token);
                    bodyData.append('idProyecto', idProyecto);
                    bodyData.append('idGrupo', idGrupo);
                    for (const file of ArrayArchivos.files) {
                        bodyData.append('file[]', file, file.name);
                    }
                    //Peticion POST
                    axios.post("{{ route('proyecto.archivos.crear') }}", bodyData, {
                            onUploadProgress: uploadEvent => {
                                let progress = Math.round(uploadEvent.loaded /
                                    uploadEvent.total * 100);
                                $("#progressbar").css("width", progress + '%');
                            }
                        })
                        .then(res => {
                            const status = res.status;
                            const data = res.data;
                            if (status == 200) {
                                notificacion(data.message +
                                    ', para visualizar los cambios debe actualizar la página',
                                    'success');
                            } else if (status == 500) {
                                notificacion(data.message, 'error');
                            } else {
                                notificacion("Error Inesperado", 'error');
                            }
                        }).catch(function(error) {
                            console.log(error);
                            notificacion("Error Inesperado revisar log", 'error');
                        }).then(function() {
                            $("#progress").hide();
                            ArrayArchivos.value = "";
                            $("#subirArchivosPost").removeAttr('disabled');
                            $("#progressbar").css("width", '0%');
                        });
                });

                $('#cancelarPost').click(function() {
                    document.getElementById('ArrayArchivos').value = "";
                });

                const notificacion = (mensaje, icono) => {
                    Swal.fire({
                        position: 'top-end',
                        icon: icono,
                        text: mensaje,
                    }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
                }

                //Filtro Lista Archivos                
                $(document).on('click', '#filtroLista li a', function() {
                    $(this).addClass("active bg-light");
                    $(this).parent().siblings().children().removeClass("active bg-light")
                    const grupoActual = $(this).attr("grupo");
                    $("#listaArchivos li").show();
                    if (grupoActual == -1) {
                        $("#listaArchivos li").show();
                    } else {
                        $("#listaArchivos li").each(function() {
                            if ($(this).attr("grupo") != grupoActual) {
                                $(this).hide();
                            }
                        });
                    }
                });

            });

    </script>
</body>
@endsection