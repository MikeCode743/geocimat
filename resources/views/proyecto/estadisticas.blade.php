@extends('layouts.navbar')
@section('title', 'Estadisticas')


@section('content')


<div class="conteiner-fluid">
    <div class="row d-flex justify-content-center">
        <h5><span class="badge mx-1 badge-pill badge-dark">PROYECTOS CREADOS EN LA APLICACION</span></h5>
        <h5><span class="badge mx-1 badge-pill badge-secondary" style="background-color:#218080">{{ $cantProyectos[0]['nombre']}} {{ $cantProyectos[0]['cantidad'] }}</span></h5>
        <h5><span class="badge mx-1 badge-pill badge-secondary" style="background-color:#4EB29B">{{ $cantProyectos[1]['nombre']}} {{ $cantProyectos[1]['cantidad'] }}</span></h5>
        <h5><span class="badge mx-1 badge-pill badge-secondary" style="background-color:#e94221">{{ $cantProyectos[2]['nombre']}} {{ $cantProyectos[2]['cantidad'] }}</span></h5>
    </div>
    <div class="row">
        <div class="col-md-12 mx-3 my-3">
            <canvas id="clasificaciones"></canvas>
            {{-- <a id="download" download="ChartImage.jpg" href="" class="btn btn-primary float-right bg-flat-color-1"
                title="Descargar Gráfico">
                <!-- Download Icon -->
                <i class="fa fa-download"></i>
            </a> --}}
        </div>
    </div>
    <div class="row mx-3 my-3">
        <div class="col-md-6">
            <canvas id="etapas" style="display: block;"></canvas>
        </div>
        <div class="col-md-6">
            <canvas id="contenido" style="display: block;"></canvas>
        </div>
    </div>
    <div class="row mx-3 my-3">
        <div class="col-md-12">
            <canvas id="fechas" style="display: block;"></canvas>
        </div>
    </div>
</div>





<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js">
    var Chart = require('chart.js');
</script>

<script>
    $(document).ready(function() {
        var clasificaciones = []
            , cantClasificacion = []
            , etapas = []
            , cantEtapa = []
            , contenido = []
            , cantContenido = []
            , fechas=[]
            , cantFechas=[]
            , meses=[];


        var color = 'rgba(7, 57, 76, 1)';
        var coloralt = 'rgba(0, 0, 0, 1)';
        var border = 'rgba(255,255,255,1)';

        var sizeScreen = true;

        if (screen.width < 1024) {
            sizeScreen = false;
        }


        //---------Arreglos para graficas de proyectos  ----------------------------
        @foreach($clasificaciones as $clasi)
        clasificaciones.push('{{ $clasi['nombre'] }}');
        cantClasificacion.push('{{ $clasi['cantidad'] }}');
        @endforeach
        
        @foreach($etapas as $etapa)
        etapas.push('{{$etapa['nombre']}}');
        cantEtapa.push('{{$etapa['cantidad']}}');
        @endforeach

        @foreach($contenido as $cont)
        contenido.push('{{$cont['nombre']}}');
        cantContenido.push('{{$cont['cantidad']}}');
        @endforeach

        @foreach($fechas_proyectos as $proyecto)
        fechas.push('{{$proyecto['fecha']}}');
        @endforeach



        //--------- Grafica de proyectos por clasificacion ----------------------------
        var ctx = document.getElementById('clasificaciones');
        ctx.height = 80;
        var graf_clasif = new Chart(ctx, {
            type: 'bar'
            , data: {
                labels: clasificaciones //Etiquetas
                , datasets: [{
                    barThickness: 40
                    , maxBarThickness: 80
                    , minBarThickness: 20
                    , label: 'cantidad de proyectos'
                    , data: cantClasificacion //datos
                    , backgroundColor: color
                    , borderColor: border

                }]
            }
            , options: {
                legend: {
                    display: false
                },
                title: {
                    display: true
                    , text: 'Cantidad de proyectos por clasificacion'
                    , fontSize: 16
                },
                responsive: true
                , scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1
                            , beginAtZero: true
                        },
                        scaleLabel: {
                            display: true
                            , labelString: 'Cantidad'
                            , fontSize: 16
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: sizeScreen //Para pantallas pequeñas
                            , labelString: 'Clasificaciones'
                            , fontSize: 16
                        }
                    }]
                }
            }
        });

        //--------- Grafica de proyectos por etapas ----------------------------
        var ctx_etapas = document.getElementById('etapas');
        ctx_etapas.height =160;
        var graf_etapas = new Chart(ctx_etapas, {
            type: 'horizontalBar'
            , data: {
                labels: etapas //Etiquetas
                , datasets: [{
                    barThickness: 20
                    , data: cantEtapa //datos
                    , label: 'cantidad de proyectos'
                    , backgroundColor: color
                    , borderColor: border
                }]
            }
            , options: {
                legend: {
                    display: false
                },
                title: {
                    display: true
                    , text: 'Cantidad de proyectos por etapas'
                    , fontSize: 16
                },
                responsive: true
                , scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true
                            , labelString: 'Tipos de etapas'
                            , fontSize: 16
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: sizeScreen //Para pantallas pequeñas
                            , labelString: 'Cantidad'
                            , fontSize: 16
                        }
                    }]
                } 
                
            }
        });


//--------- Grafica de Contenido de los proyectos ----------------------------
        var ctx_contenido = document.getElementById('contenido');
        ctx_contenido.height =160;
        var graf_contenido = new Chart(ctx_contenido, {
            type: 'horizontalBar'
            , data: {
                labels: contenido //Etiquetas
                , datasets: [{
                    barThickness: 20                    
                    , data: cantContenido //datos
                    , label: 'cantidad de proyectos'
                    , backgroundColor: color
                    , borderColor: border
                }]
            }
            , options: {
                legend: {
                    display: false
                }
                ,title: {
                    display: true
                    , text: 'Cantidad de archivos en los proyectos'
                    , fontSize: 16
                }
                ,responsive: true
                , scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: sizeScreen //Para pantallas pequeñas
                            , labelString: 'Tipos de archivos'
                            , fontSize: 16
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display:true
                            , labelString: 'Cantidad'
                            , fontSize: 16
                        }
                    }]
                }
            }
        });
    
//--------- Grafica de fechas de los proyectos ----------------------------
        fechas = contarFechas(fechas);

        fechas.forEach(element => {
            console.log(element.mes,element.cantidad);
            cantFechas.push(element.cantidad);
            meses.push(element.mes);
        });

        var ctxFechas = document.getElementById('fechas');
        var graf_fechas = new Chart(ctxFechas, {
            type: 'line',
            data: {
                labels: meses,
                datasets: [{
                    fill: false,
                    label: 'Cantidad de Proyectos',
                    data: cantFechas,
                    backgroundColor: color,
                    borderColor: color,
                    pointRadius: 5,
                }]
            },
            options: {
                responsive: true,
                tooltips:{
                    mode:'index',
                    intersect: false
                }
                ,title: {
                    display: true
                    , text: 'Cantidad de proyectos por mes'
                    , fontSize: 16
                },
                hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Meses'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                    min: 0, 
                                    beginAtZero: true,
                                    stepSize: 1  
                                    },
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Cantidad'
                            }
                        }]
                }
            }
        });
    
    
    });

    function contarFechas(fechas){

        var cantFechas = [];
        var datosFechas;
        
        var mes_actual = new Date();
        mes_actual.setDate(1);

        var mes_proximo = new Date();
        mes_proximo.setDate(1);
        mes_proximo.setMonth(mes_proximo.getMonth() + 1);

        for (let index = 0; index < 13; index++) {
            
            let contador = 0;
            fechas.forEach(element => {
                fecha = new Date(element);
                if(fecha >= mes_actual && mes_proximo >= fecha){
                    contador += 1;
                }
            });

            cantFechas.push({"mes":mes_actual.getMonth(), "cantidad":contador});
            mes_proximo.setMonth(mes_proximo.getMonth() - 1);
            mes_actual.setMonth(mes_actual.getMonth() - 1);

        }

        datosFechas = obtenerMes(cantFechas.reverse());
        return datosFechas;
    }


    function obtenerMes(fechas){

        var items = [
         {'mes': 'Enero', value:0 },
         {'mes': 'Febrero', value:1 },
         {'mes': 'Marzo', value:2},
         {'mes': 'Abril', value:3 },
         {'mes': 'Mayo', value:4 },
         {'mes': 'Junio', value:5 },
         {'mes': 'Julio', value:6 },
         {'mes': 'Agosto', value:7 },
         {'mes': 'Septiembre', value:8 },
         {'mes': 'Octubre', value:9 },
         {'mes': 'Noviembre', value:10 },
         {'mes': 'Diciembre', value:11 },
        ];

        fechas.forEach(element => {
            items.forEach(item => {
                if (element.mes == item.value) {
                    element.mes = item.mes;
                }
            });
        });

        return fechas;
    }


</script>



@endsection