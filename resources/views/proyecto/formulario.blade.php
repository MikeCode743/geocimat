@extends('layouts.navbarproyecto')

@section('title', 'Nuevo Proyecto')

@section('librerias')
    @include('proyecto.parcial.librerias.css')
    @include('proyecto.parcial.librerias.script')

@endsection

@section('content')
    <div class="loading" hidden>
        <div class="d-flex justify-content-center text-info mt-5">
            <div class="spinner-grow" style="width: 8rem; height: 8rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
<body>    
<div class="row bg-light">

    <div class="col-12 col-sm-12 col col-md-12 mx-auto mx-md-auto mt-3">
        <div class="card bg-transparent border-0">
            <div class="card-header mx-auto bg-transparent border-0">
                <p class="h3">Agregar proyecto</p>
            </div>
            <div class="card-body ">
                @if(Session::has('error_existe'))
                <div class="alert alert-danger col-md-8 mx-md-auto" role="alert">{{ Session::get('error_existe') }}
                </div>
                @endif

                <form method="POST" action="{{ route('proyecto.store') }}" id="configform">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-8 mx-md-auto">
                            <label>Título del proyecto</label><sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup>
                            <input name="titulo" type="text"
                                class="form-control {{$errors->has('titulo') ? 'is-invalid' : ''}}" id="nombre"
                                placeholder="" value="{{old('titulo')}}">
                            @if($errors->has('titulo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('titulo') }}</strong>
                            </span>
                            @endif
                            <label class="mt-3">Clasificación</label><sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup>
                            <select name="clasificacion"
                                class="form-control {{$errors->has('clasificacion') ? 'is-invalid' : ''}}"
                                id="clasificacion">
                                @if ($listaclasificacion)
                                @foreach ($listaclasificacion as $clasificacion)
                                <option value="{{ $clasificacion->clasificacion_id }}"
                                    {{(old('clasificacion') == $clasificacion->clasificacion_id) ? 'selected="true"' : ''}}>
                                    {{ $clasificacion->nombre }}</option>
                                @endforeach
                                @endif
                            </select>
                            @if($errors->has('clasificacion'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('clasificacion') }}</strong>
                            </span>
                            @endif


                            <label class="mt-3">Fecha de inicio</label><sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup>
                            <input name="fecha_inicio" type="text"
                                class="form-control d-flex justify-content-center {{$errors->has('fecha_inicio') ? 'is-invalid' : ''}}"
                                id="datepicker" value="">
                            @if($errors->has('fecha_inicio'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('fecha_inicio') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group col-md-8 mx-md-auto">
                            <label>Coordenadas </label><sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text px-3" id="basic-addon1">Longitud</span>
                                        </div>
                                        <input name="lng" id="lng" type="text"
                                            class="form-control {{$errors->has('lng') ? 'is-invalid' : ''}}"
                                            placeholder="-180 a 180" value="{{old('lng')}}">
                                        @if($errors->has('lng'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('lng') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <div class="input-group mb-3 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text px-3" id="basic-addon1">Latitud</span>
                                        </div>
                                        <input name="lat" id="lat" type="text"
                                            class="form-control {{$errors->has('lat') ? 'is-invalid' : ''}}"
                                            placeholder="-90 a 90" value="{{old('lat')}}">
                                        @if($errors->has('lat'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('lat') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div id="menu" class="btn-group btn-group-toggle" data-toggle="buttons">
                                <button id="satellite" type="button" class="btn btn-primary btn-sm">Satellite</button>
                                <button id="outdoors" type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                                <button id="streets" type="button" class="btn btn-secondary btn-sm">Streets</button>
                            </div>
                            <div id="map" style="width: 100%; height: 450px; margin-top: 5px;"></div>
                            <small class="form-text text-muted">
                                Click en el mapa para agregar las coordenadas.
                            </small>
                        </div>
                        <div class="form-group col-md-8 mx-md-auto">
                            <label>Descripci&oacute;n del proyecto</label>
                            <textarea id="informacionAdicional" name="informacionAdicional"
                                class="summernote {{$errors->has('nombres') ? 'is-invalid' : ''}}" value=""></textarea>
                            @if($errors->has('informacionAdicional'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('informacionAdicional') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="text-right col-md-8 mx-md-auto">
                            <button type="button" id="close" class="btn btn-secondary">Cancelar</button>
                            <button type="submit" id="add" class="btn btn-primary">Agregar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

            const lngInput = document.querySelector('#lng');
            const latInput = document.querySelector('#lat');

            let marker = new mapboxgl.Marker();
            let [lng, lat] = [0, 0];

            const markerInput = () => {

                let [lngValid, latValid] = [false, false];
                let [parseLng, parseLat] = [parseFloat(lngInput.value), parseFloat(latInput.value)]

                //LNG
                if (parseLng == lngInput.value) {
                    if (parseLng >= -180.0 && parseLng <= 180) {
                        lngInput.classList.remove("is-invalid");
                        lngValid = true;
                    } else {
                        lngInput.classList.add("is-invalid");
                    }
                } else {
                    lngInput.classList.add("is-invalid");
                }

                //LAT
                if (parseLat == latInput.value) {
                    if (parseLat >= -90.0 && parseLat <= 90.0) {
                        latInput.classList.remove("is-invalid");
                        latValid = true;
                    } else {
                        latInput.classList.add("is-invalid");
                    }
                } else {
                    latInput.classList.add("is-invalid");
                }

                //Add Marker
                if (lngValid && lngValid) {
                    marker.setLngLat([parseLng, parseLat]).addTo(map);
                } else {
                    marker.remove();
                }

                return lngValid && lngValid;

            }

            //MapBox
            mapboxgl.accessToken =
                'pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg';

            let map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/satellite-streets-v11',
                center: [-88.85, 13.82], // starting position
                zoom: 7 // starting zoom
            });

            map.on('click', function(e) {
                latInput.classList.remove("is-invalid");
                lngInput.classList.remove("is-invalid");
                $("#coordenadas").val(JSON.stringify(e.lngLat.wrap()));
                [lng, lat] = [e.lngLat.wrap().lng, e.lngLat.wrap().lat];
                lngInput.value = lng.toFixed(8);
                latInput.value = lat.toFixed(8);
                marker.setLngLat([lng, lat]).addTo(map);
            });

            $('#satellite').click(function() {
                map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
            });
            $('#outdoors').click(function() {
                map.setStyle('mapbox://styles/mapbox/outdoors-v11');
            });
            $('#streets').click(function() {
                map.setStyle('mapbox://styles/mapbox/streets-v11');
            });

            $("#lng").keyup(function(event) {
                markerInput();
            });

            $("#lat").keyup(function() {
                markerInput();
            });

            //Datepicker
            $("#datepicker").datepicker({
                inline: true,
                format: 'yyyy/mm/dd',
                todayBtn: true,
                todayHighlight: true,
                orientation: 'top right',
            });

            $("#datepicker").datepicker("setDate", 
                @if (old('fecha_inicio'))
                    '{{old('fecha_inicio')}}'
                @else
                    'now'
                @endif
                );

            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                placeholder: 'Ej. Objetivo del proyecto',
            });
            

            
            //SweetAlert
            showMessage = (message, icon) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: message,
                    showConfirmButton: false,
                    timer: 1500
                })
            };

        });
    
        /* PARA QUE NO SE ENVIEN DATOS AL PRESIONAR ENTER */
        document.addEventListener('DOMContentLoaded', () => {
          document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
            if(e.keyCode == 13) {
              e.preventDefault();
            }
          }))          
        });

</script>
<script>
    $('.summernote').summernote('code', `{!!old('informacionAdicional')!!}`);
</script>
</body>
    
@endsection

