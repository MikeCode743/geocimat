@extends('layouts.navbarproyecto')

@section('title', 'Mis Proyectos')

@section('librerias')
{{-- @include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script') --}}

{{--Bootstrap  --}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- Robooto -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
<!--Datatable-->
<link rel="stylesheet" href="{{ asset('css\dataTables.bootstrap4.min.css') }}">
{{-- JQuery --}}
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
{{-- Poopper --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
{{-- Bootstrap --}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<!-- SweetAlert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- Robooto -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
{{-- Datatable --}}
<script src="{{ asset('js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js\dataTables.bootstrap4.min.js') }}"></script>
<!-- Loader  -->
<script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
<!-- Revisar las alertas que muestra en consola -->
<script type="text/javascript">
    window.addEventListener('DOMContentLoaded', function() {
                    new QueryLoader2(document.querySelector("body"), {
                        barColor: "#efefef",
                        backgroundColor: "#111",
                        percentage: true,
                        barHeight: 1,
                        minimumTime: 200,
                        fadeOutTime: 1000
                    });
                });
        
</script>

@endsection


@section('content')

@include('layouts.alerts')


<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-dark">
            <div class="row">
                <div class="col-sm-9">
                    <h5 class="card-title  float-left" style="color: white">Proyectos Registrados</h5>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive-xl">
                <table id="datatable" class="table table-striped responsive nowrap table-hover" style="width:100%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col" width="80%">Nombre del proyecto</th>
                            <th scope="col" width="20%" class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($proyectos)
                        @foreach($proyectos as $proyecto)
                        <tr>
                            <td>{{$proyecto->proyecto_id}}</td>
                            <td>{{$proyecto->nombre}}</td>
                            <td class="text-center">
                                <button onclick="location.href='{{ route('proyecto.detalle',[$proyecto->proyecto_id]) }}'" type="button"
                                class="btn btn-warning btn-detalle btn-sm" data-toggle="tooltip" data-placement="bottom" title="ver Proyecto">
                                <i class='far fa-eye'></i></button>
                                <span data-toggle="modal" data-target="#imgmodal">
                                    <button type="button" id="btn-img" class="btn btn-info btn-img btn-sm"
                                        data-toggle="tooltip" data-placement="bottom" title="Subir Imagen"><i
                                            class='fas fa-images'></i></button>
                                </span>
                                <span data-toggle="modal" data-target="#docmodal">
                                    <button type="button" class="btn btn-success btn-doc btn-sm" data-toggle="tooltip"
                                        data-placement="bottom" title="Subir Documentos"><i
                                            class='far fa-file-alt'></i></button>
                                </span>
                                <button onclick="location.href='{{ route('proyecto.bitacora',[$proyecto->proyecto_id]) }}'" type="button" class="btn btn-secondary btn-bitacora btn-sm"
                                    data-toggle="tooltip" data-placement="bottom" title="Editar Bitacora">
                                    <i class='fas fa-file-signature'></i></button>


                                {{-- <button id="btn-modal-etapa-{{$proyecto->proyecto_id}}" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-cambiar-etapa" data-ruta="{{route('misproyectos.etapa.cambiar',[$proyecto->proyecto_id])}}" onclick="prepararModalEstadoEtapa('{{$proyecto->proyecto_id}}', {{$proyecto->etapa_id}}, 'btn-modal-etapa-', 'cambiar-etapa-form', 'select-etapas')" title="Cambiar Etapa">
                                  <i class='fas fa-list-alt'></i>
                                </button>

                                <button id="btn-modal-estado-{{$proyecto->proyecto_id}}" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-cambiar-estado" data-ruta="{{route('misproyectos.estado.cambiar',[$proyecto->proyecto_id]) }}" onclick="prepararModalEstadoEtapa('{{$proyecto->proyecto_id}}', {{($proyecto->estado) ? 1 : 0 }}, 'btn-modal-estado-', 'cambiar-estado-form', 'select-estados')" title="Cambiar Estado">
                                  <i class='fas fa-exclamation-triangle'></i>
                                </button> --}}

                                
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>


@if (Auth::user()->hasPermisos('add-img-pj'))
<!-- POST Agregar Imagen -->
<div class="modal fade bd-example-modal-lg" id="imgmodal" tabindex="-1" role="dialog" aria-labelledby="photoModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Fotografía</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <input id="idProyectoimg" type="hidden" name="idProyectoimg" value="">
                <div class="form-group">
                    <input id="ArrayImagenes" name="file[]" type="file" class="form-control-file" multiple>
                </div>
                <div class="form-group mt-3">
                    <label for="exampleFormControlTextarea1">Descripci&oacute;n</label>
                    <textarea class="form-control" id="descripcion" rows="3" name="descripcion"></textarea>
                </div>
            </div>
            <div id="progressimg" class="progress">
                <div id="progressbarimg" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="modal-footer">
                <button id="cancelarPostImg" type="button" class="btn btn-secondary"
                    data-dismiss="modal">Cerrar</button>
                <button id="subirImagenesPost" type="button" class="btn btn-primary">Agregar</button>
            </div>
        </div>
    </div>
</div>
@endif
@if (Auth::user()->hasPermisos('add-doc-pj'))
<!-- POST AGREGAR ARCHIVOS -->
<div class="modal fade bd-example-modal-lg" id="docmodal" tabindex="-1" role="dialog" aria-labelledby="photoModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Archivo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <input type="hidden" id="idProyecto" name="idProyecto" value="">
                <div class="form-group">
                    <input name="file[]" type="file" class="form-control-file" id="ArrayArchivos" multiple>
                </div>
                <!--AQUI DEBE IR EL TIPO DE ARCHIVO -->
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Seleccionar Grupo</label>
                    <select class="form-control" id="idGrupo">
                        @foreach ($listaGrupo as $grupo)
                        <option value="{{ $grupo->grupo_id }}">{{ $grupo->coleccion }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="progressdoc" class="progress">
                <div id="progressbardoc" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="modal-footer">
                <button id="cancelarPostDoc" type="button" class="btn btn-secondary"
                    data-dismiss="modal">Cerrar</button>
                <button id="subirArchivosPost" type="button" class="btn btn-primary">Agregar</button>
            </div>
        </div>
    </div>
</div>
@endif

{{-- @include('proyecto.parcial.cambiaretapa')
@include('proyecto.parcial.cambiarestado') --}}

<!-- Agregar Axios -->
<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>

<script type="text/javascript">
    $(document).ready(function (){
        $("#progressimg").hide();
        $("#progressdoc").hide();

        var table = $('#datatable').DataTable({
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron proyectos",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast":"Último",
                "sNext":"Siguiente",
                "sPrevious": "Anterior"
            },
                "sProcessing":"Procesando...",
            },
                "columnDefs": [ { "visible": false, "targets": 0 } ],
                "responsive": "true"
        });
        /*----------------IMAGENES--------------------------------------------------- */
        table.on('click','.btn-img',function(){
            $tr = $(this).closest('tr');
            $('#imgmodal').data('row', $tr);
            if($($tr).hasClass('child')){
                $tr=$tr.prev('.parent');
            }
            $('#imgmodal').modal('show');
        });
        $('#imgmodal').on('shown.bs.modal', function (e){
            var data = table.row($(this).data('row')).data();
            $('#idProyectoimg').val(data[0]);    
        });
        //submit form
        $('#subirImagenesPost').click(function() {
            $("#progressimg").show();
            $("#subirImagenesPost").attr('disabled', true);
            const token = document.getElementsByName('_token')[0].value;
            const idProyecto = document.getElementsByName('idProyectoimg')[0].value;
            const arrayImagenes = document.getElementById('ArrayImagenes');
            const descripcion = document.getElementById('descripcion').value;
            let bodyData = new FormData();
            bodyData.append('_token', token);
            bodyData.append('idProyecto', idProyecto);
            bodyData.append('descripcion', descripcion);
            for (const file of arrayImagenes.files) {
                bodyData.append('file[]', file, file.name);
            }
            axios.post("{{ route('proyecto.imagenes.crear') }}", bodyData, {
                onUploadProgress: uploadEvent => {
                    let progress = Math.round(uploadEvent.loaded /
                    uploadEvent.total * 100);
                    $("#progressbarimg").css("width", progress + '%');
                }
            })
            .then(res => {
                const status = res.status;
                const data = res.data;
                if (status == 200) {
                    notificacion(data.message +
                    ', para visualizar los cambios debe actualizar la página',
                    'success');
                } else if (status == 500) {
                    notificacion(data.message, 'error');
                } else {
                    notificacion("Error Inesperado", 'error');
                }
            }).catch(function(error) {
                console.log(error);
                notificacion("Error Inesperado revisar log", 'error');
            }).then(function() {
                $("#progressimg").hide();
                descripcion.value = "";
                arrayImagenes.value = "";
                $("#subirImagenesPost").removeAttr('disabled');
                $("#progressbar").css("width", '0%');
            });
        });
        $('#cancelarPostImg').click(function() {
            document.getElementById('ArrayImagenes').value = "";
            document.getElementById('descripcion').value = "";
        });
        const notificacion = (mensaje, icono) => {
            Swal.fire({
                position: 'top-end',
                icon: icono,
                text: mensaje,
            }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
        }
        /*----------------FIN - IMAGENES --------------------------------------------------- */
        
        /*---------------- Archivos --------------------------------------------------- */
        table.on('click','.btn-doc',function(){
            $tr = $(this).closest('tr');
            $('#docmodal').data('row', $tr);
            if($($tr).hasClass('child')){
                $tr=$tr.prev('.parent');
            }
            $('#docmodal').modal('show');
        });
        $('#docmodal').on('shown.bs.modal', function (e){
            var data = table.row($(this).data('row')).data();
            $('#idProyecto').val(data[0]);
        });
        
        $('#subirArchivosPost').click(function() {
            $("#progressdoc").show();
            $("#subirArchivosPost").attr('disabled', true);
            const token = document.getElementsByName('_token')[0].value;
            const idProyecto = document.getElementsByName('idProyecto')[0].value;
            const idGrupo = document.getElementById('idGrupo').value;
            const ArrayArchivos = document.getElementById('ArrayArchivos');
            let bodyData = new FormData();
            bodyData.append('_token', token);
            bodyData.append('idProyecto', idProyecto);
            bodyData.append('idGrupo', idGrupo);
            
            for (const file of ArrayArchivos.files) {
                bodyData.append('file[]', file, file.name);
            }
            axios.post("{{ route('proyecto.archivos.crear') }}", bodyData, {
                onUploadProgress: uploadEvent => {
                    let progress = Math.round(uploadEvent.loaded /
                    uploadEvent.total * 100);
                    $("#progressbardoc").css("width", progress + '%');
                }
            })
            .then(res => {
                const status = res.status;
                const data = res.data;
                if (status == 200) {
                    notificacion(data.message +
                    ', para visualizar los cambios debe actualizar la página',
                    'success');
                } else if (status == 500) {
                    notificacion(data.message, 'error');
                } else {
                    notificacion("Error Inesperado", 'error');
                }
            }).catch(function(error) {
                console.log(error);
                notificacion("Error Inesperado revisar log", 'error');
            }).then(function() {
                $("#progressdoc").hide();
                ArrayArchivos.value = "";
                $("#subirArchivosPost").removeAttr('disabled');
                $("#progressbar").css("width", '0%');
            });
        });
        
        $('#cancelarPostDoc').click(function() {
                document.getElementById('ArrayArchivos').value = "";
        });
        /*//para enviar el formulario a partir del modal de etapas comun
        $('#btn-aceptar-etapa').click(function(){
            let url = document.getElementById("btn-aceptar-etapa").getAttribute('data-ruta');
            let formData = new FormData();            
            formData.append('_token', document.forms['cambiar-etapa-form'].elements[0].value);            
            formData.append('idEtapa', document.getElementById('select-etapas').value);
            console.log(url);
            //console.log(token);
            console.log(document.getElementById('select-etapas').value);
            //console.log(formData.getAll());
            axios.post(
                url, formData
                ).then((response) => {
                    if (status == 200) {
                        notificacion(response.data.message,'success')
                    } else if (status == 500) {
                        notificacion(response.data.message,'error')
                    }
                    //notificacion(response.data.message,'success');
                }).catch((error) => {
                    notificacion(response.data.message,'error');
                });
        });*/
    });

    //para pasarle datos al modal
    // function prepararModalEstadoEtapa(id, param, btnmodal, formmodal, selectmodal){
    //     //pasa la ruta especifica
    //     let ruta = document.getElementById(btnmodal+id).getAttribute('data-ruta');        
    //     document.getElementById(formmodal).setAttribute('action', ruta);
    //     //document.getElementById("btn-aceptar-etapa").setAttribute('data-ruta', ruta);        
    //     //pasa la etapa actual
    //     let items = document.getElementById(selectmodal).options;
    //     //console.log(items);
    //     for(let i = 0; i < items.length; i++){
    //         if(items[i].getAttribute('value') == param){
    //             items[i].selected = true;
    //         }
    //     }
    // }

</script>

@endsection