@extends('layouts.navbar')

@section('title', 'Informes')
@section('librerias')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.all.min.js"></script>

@endsection

@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h3>
                <span class="badge badge-secondary">INFORMES</span>
            </h3>
        </div>
        <div class="col-md-6">
            <div class="d-flex justify-content-end">
                <a type="button" href="{{ route('proyecto.estadisticas') }}"  class="btn btn-primary mx-2"><i class='fas fa-chart-bar mr-2'></i>Estadisticas</a>
                <button type="button" class="btn btn-secondary mx-2" id="imprimir" name="imprimir"><i class='fas fa-print mr-2'></i>Imprimir tabla</button>
            </div>
        </div>
    </div>



    @if ($proyectos)
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="clasifi">Seleccionar Clasificacion</label>
                <select class="selectpicker clasi form-control show-tick" id="clasifi" name="clasifi[]" multiple title="Clasificaciones">
                    @foreach ($clasificaciones as $cla)
                    <option value="{{$cla->nombre}}">{{ $cla->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="etapas">Seleccionar Etapas</label>
                <select class="selectpicker form-control show-tick" id="etapas" name="etapas[]" multiple title="Etapas">
                    @foreach ($etapas as $etapa)
                    <option value="{{$etapa->nombre}}">{{ $etapa->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="fecha">Filtrar por Fecha</label>
                <select class="selectpicker form-control show-tick" id="fecha" name="fecha" title="Filtrar por fecha">
                    <option value="0">Ver Todos</option>
                    <option value="1">Este mes</option>
                    <option value="2">Ultimos 3 mes</option>
                    <option value="3">Ultimos 6 mes</option>
                    <option value="4">Año Actual</option>
                    <option value="5">Ultimos 3 Año </option>
                </select>
            </div>
        </div>

    </div>
    @endif
    <div id="pagImprimir">


        <div class="col-md-12">
            <div class="table-responsive">

                <table id="proyectos" class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Proyecto</th>
                            <th scope="col">Encargado</th>
                            <th scope="col">Clasificacion</th>
                            <th scope="col">Etapa</th>
                            <th scope="col" style="width: 12%">Fecha</th>

                        </tr>
                    </thead>
                    <tbody>
                        @if($proyectos)
                        @foreach($proyectos as $proyecto)
                        <tr>
                            <td>{{ $proyecto->titulo }}</td>
                            <td>{{ $proyecto->apellidos }}, {{ $proyecto->nombres }}</td>
                            <td>{{ $proyecto->clasificacion }}</td>
                            <td>{{ $proyecto->etapa }}</td>
                            <td>{{ date_format(date_create($proyecto->fecha), 'd/m/Y ' )}}</td>

                        </tr>
                        @endforeach
                        @else
                        NO HAY PROYECTOS
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>

<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2');

</script>



<script class="text/javascript">
    $(document).ready(function() {

        $('.selectpicker').on('changed.bs.select', function(e) {
            var table, tr, td, i, txtValue, clasificiones_selecionadas = []
                , etapas_selecionadas = []
                , fecha_seleccionada;
            var fecha_actual = new Date();


            clasificiones_selecionadas = $('#clasifi').val();
            etapas_selecionadas = $('#etapas').val();
            fecha_seleccionada = $('#fecha').val();



            if (clasificiones_selecionadas.length == 0) {
                @foreach($clasificaciones as $clasi)
                clasificiones_selecionadas.push('{{ $clasi->nombre }}');
                @endforeach
            }
            if (etapas_selecionadas.length == 0) {
                @foreach($etapas as $etapa)
                etapas_selecionadas.push('{{ $etapa->nombre }}');
                @endforeach
            }
            if (fecha_seleccionada.length == 0) {
                fecha_seleccionada = "0";
            }

            table = document.getElementById("proyectos");
            tr = table.getElementsByTagName("tr");

            var seleccion = clasificiones_selecionadas.concat(etapas_selecionadas);

            switch (fecha_seleccionada) {
                case '0': //Ver todos
                    fecha_actual.setFullYear(0001)
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                case '1': //Este Mes
                    fecha_actual.setDate(1);
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                case '2': //Ultimos 3 meses
                    fecha_actual.setMonth(fecha_actual.getMonth() - 3);
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                case '3': //Ultimos 6 Meses
                    fecha_actual.setMonth(fecha_actual.getMonth() - 6);
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                case '4': //Año Actual
                    fecha_actual.setMonth(1);
                    fecha_actual.setDate(1);
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                case '5': // Ultimos 3 Años
                    fecha_actual.setMonth(1);
                    fecha_actual.setDate(1);
                    fecha_actual.setFullYear(fecha_actual.getFullYear() - 3)
                    filtrar(tr, seleccion, fecha_actual);
                    break;
                default:
                    filtrar(tr, seleccion,fecha_actual);
                    // console.log('no entro al switch')
            }
        });


        function filtrar(tr, seleccion, fecha_actual) {


            console.log(fecha_actual);
            for (i = 0; i < tr.length; i++) {
                td2 = tr[i].getElementsByTagName("td")[2];
                td3 = tr[i].getElementsByTagName("td")[3];
                td4 = tr[i].getElementsByTagName("td")[4];
                if (td2 || td3 || td4) {
                    clasificacion = td2.textContent || td2.innerText;
                    etapa = td3.textContent || td3.innerText;
                    fecha_proyecto = td4.textContent || td4.innerText;
                    dia = fecha_proyecto.split("/")[0]
                    mes = fecha_proyecto.split("/")[1] - 1
                    anio = fecha_proyecto.split("/")[2]
                    var fecha_proyecto = new Date(anio,mes,dia);
                    console.log(fecha_actual,"actual");
                    console.log(fecha_proyecto);


                    if (seleccion.includes(clasificacion) == true && seleccion.includes(etapa) == true && fecha_proyecto >= fecha_actual) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }

            }
            restablecerRegistro(seleccion);
        }



    });



    $("#imprimir").click(function() {
        // console.log(registros);
        Swal.fire({
            title: 'Imprimir Registros'
            , text: "¿Seguro de imprimir los datos de la tabla?"
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'SI, Imprimir'
            , cancelButtonText: 'Cancelar'
        }).then(({isConfirmed})=> {
            if (isConfirmed) {
                var pagImprimir = $("#pagImprimir").html();
                var style = 'table { border-collapse: collapse;  } th, td { padding: 8px; text-align: left; border-bottom: 1px solid #ddd; } tr:hover {background-color:#f5f5f5;} ';
                var printWindow = window.open('', '', 'height=400,width=1000');
                printWindow.document.write('<html><head>');
                printWindow.document.write('<style type="text/css"> ');
                printWindow.document.write(style);
                printWindow.document.write('</style>');
                printWindow.document.write('</head><body>');
                printWindow.document.write('<br><h2 style="display: flex; justify-content: center; align-items: center">INFORME DE PROYECTOS</h2><br>');
                printWindow.document.write(pagImprimir);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                printWindow.print();
            }
        })
    });



    function imprimir(tr, selecciones, fecha) {
        var arreglo = []
            , fila = [];
        for (i = 0; i < tr.length; i++) {
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            if (td2 || td3 || td4) {
                let clasificacion = td2.textContent || td2.innerText;
                let etapa = td3.textContent || td3.innerText;
                var fecha_proyecto = td4.textContent || td4.innerText;
                fecha_proyecto = new Date(fecha_proyecto);
                if (selecciones.includes(clasificacion) == true && selecciones.includes(etapa) == true && fecha_proyecto >= fecha) {
                    td = tr[i].getElementsByTagName("td");

                    for (let index = 0; index < td.length; index++) {
                        let elemento = td[index];
                        elemento = elemento.textContent || elemento.innerText;
                        fila.push(elemento);
                    }
                    arreglo.push(fila);
                    fila = [];
                }
            }

        }

        return arreglo;
    };

    function restablecerRegistro(lista) {
        if (lista.length == 0) {
            for (i = 0; i < tr.length; i++) {
                tr[i].style.display = "";
            }
        }

    }

</script>

@endsection
