@extends('layouts.navbarproyecto')

@section('title', 'Bitacora Proyecto')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')

@endsection


@section('content')

<body>
    <div class="card bg-transparent border-0 p-0 m-0 mt-3">
        <div class="card-header mx-auto bg-transparent border-0 p-0 m-0">
            <p class="h3">Bit&aacute;cora {{ $proyecto }}</p>
        </div>
        <div class="card-body ">
            <form>
                @csrf
                <input id="idProyecto" type="hidden" name="idProyecto" value="{{ $proyecto_id }}">
                <div class="form-row">
                    <div class="form-group col-md-12 mx-md-auto">
                        <textarea id="descripcion" name="descripcion" class="summernote"></textarea>
                    </div>
                    <div class="text-right col-md-12 mx-md-auto">
                        <a class="btn btn-secondary" href="{{ route('proyecto.lista') }}" role="button">Cancelar</a>
                        <button type="button" id="editar" class="btn btn-primary">Actualizar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        $(document).ready(function() {
            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table']
                ],
            });
            $('.summernote').summernote('code', `{!!  $detalle !!}`);

            $('#editar').click(function() {
                const token = document.getElementsByName('_token')[0].value;
                const idProyecto = document.getElementsByName('idProyecto')[0].value;
                const descripcion = document.getElementsByName('descripcion')[0].value;
                let bodyData = new FormData();
                bodyData.append('_token', token);
                bodyData.append('idProyecto', idProyecto);
                bodyData.append('descripcion', descripcion);

                axios.post("{{ route('proyecto.bitacora.editar') }}", bodyData)
                    .then(res => {
                        const status = res.status;
                        const data = res.data;
                        if (status == 200) {
                            notificacion(data.message, 'success');
                        } else {
                            notificacion("Error Inesperado", 'error');
                        }
                    }).catch(function(error) {
                        if (error.response.status == 500) {
                            notificacion(error.response.data.message, 'error');
                        } else {
                            notificacion("Ooopss! Parece que hubo un error, revisa el log",
                                'error');
                        }
                    });
            });
            const notificacion = (mensaje, icono) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icono,
                    text: mensaje,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        });

    </script>
</body>

@endsection
