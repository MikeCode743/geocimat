@extends('layouts.navbarproyecto')

@section('title', 'Papelera de Archivo')

@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')
@endsection

@section('content')

<body>
    @if (Auth::user()->proyectoarchivos($id))
    @if (Auth::user()->hasPermisos('del-doc-pj'))
    <!-- POST restaurar ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="restaurarDocumento" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabelrestore">restaurar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('archivo.papelera.restaurar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label id="mensajerestaurar" class="text-success">¿Esta seguro de restaurar el
                            documento?</label>
                        <input name="idProyecto" value="{{ $id }}" type="hidden">
                        <input name="nombreDocumentorestaurar" id="nombreDocumentorestaurar" value="" type="hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Restaurar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    @if (Auth::user()->hasPermisos('del-doc-pj'))
    <!-- POST Eliminar ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="eliminarDocumento" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabeldelete">Eliminar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('archivo.papelera.eliminar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label id="mensajeeliminar" class="text-danger">¿Esta seguro de restaurar el
                            documento?</label>
                        <input name="idProyecto" value="{{ $id }}" type="hidden">
                        <input name="nombreDocumentoeliminar" id="nombreDocumentoeliminar" value="" type="hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif

    <div class="container mt-3">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-10 m-auto p-0 m-0">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0 p-0 ">
                        <div class="d-flex m-0 p-0">
                            <div>
                                <p class="h3">Papelera de los archivos</p>
                            </div>
                            <div class="col text-right">

                                @if (Auth::user()->hasPermisos('view-img-pj'))
                                <a class="btn btn-secondary" href="{{ route('proyecto.imagen', ['id' => $id]) }}"
                                    role="button">Imagenes</a>
                                @endif
                                @if (Auth::user()->hasPermisos('view-doc-pj'))
                                <a class="btn btn-secondary" href="{{ route('proyecto.archivos', ['id' => $id]) }}"
                                    role="button">Documentos</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if (Auth::user()->hasPermisos('view-doc-pj'))
                    <!-- Filtro Archivos Subidos -->
                    <ul class="nav nav-tabs" id="filtroLista">
                        <li class="nav-item">
                            <a class="nav-link active bg-light" grupo="-1">Todos</a>
                        </li>
                        @foreach ($listaGrupo as $grupo)
                        <li class="nav-item">
                            <a class="nav-link" grupo="{{ $grupo->grupo_id }}">{{ $grupo->coleccion }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- FIN Filtro Archivos Subidos -->
                    <div class="card-body p-0 mt-2">
                        @if (count($listaArchivos) === 0)
                        <p>A&uacute;n no se han agregado archivos.</p>
                        @else
                        <ul id="listaArchivos" class="list-group list-group-flush mt-0">
                            @foreach ($listaArchivos as $archivo)
                            <li class="list-group-item list-group-item-action p-1 pl-2"
                                grupo="{{ $archivo->grupo_id }}">
                                <div class="d-flex m-0 p-0 ">
                                    <div class="open-file col-md-8"
                                        link="{{ route('repository', ['ruta' => $archivo->ruta_archivo]) }}">
                                        @if (isset($listaIconos[$archivo->extension]))
                                        <i class="{{ $listaIconos[$archivo->extension] }}"></i>
                                        @else
                                        <i class="fas fa-file-contract"></i>
                                        @endif
                                        {{ $archivo->nombre }}
                                    </div>
                                    @if (Auth::user()->proyectoarchivos($id))
                                        <div restaurarArchivo="{{ $archivo->nombre }}" class="restore-file mx-auto">
                                            <button type="button" class="btn btn-success btn-sm mr-3" data-toggle="tooltip" data-placement="bottom" title="Restaurar"><i
                                                class="fas fa-trash-restore"></i> Restaurar</button>
                                            </div>
                                            <div eliminarArchivo="{{ $archivo->nombre }}" class="delete-file mx-auto">
                                                <button type="button" class="btn btn-danger btn-sm mr-1" data-toggle="tooltip" data-placement="bottom" title="Eliminar"><i
                                                    class="fas fa-trash"></i> Eliminar </button>
                                                </div>
                                    @endif
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        const fileList = document.getElementsByClassName('open-file');
            for (let i = 0; i < fileList.length; i++) {
                const link = fileList[i]
                link.addEventListener('click', event => {
                    const filelink = link.getAttribute('link')
                    window.open(filelink, '_blank')
                });
            }
            const listBtnRestore = document.getElementsByClassName('restore-file');
            for (let i = 0; i < listBtnRestore.length; i++) {
                const btn = listBtnRestore[i];
                btn.addEventListener('click', event => {
                    const nombreDocumento = btn.getAttribute('restaurarArchivo');
                    const nombreSinFecha = nombreDocumento.slice(nombreDocumento.indexOf("-") + 1);
                    btn.children[0].setAttribute('data-toggle', 'modal');
                    btn.children[0].setAttribute('data-target', '#restaurarDocumento');
                    console.log(btn.getAttribute('restaurarArchivo'));
                    document.getElementById('nombreDocumentorestaurar').value = nombreDocumento;
                    document.getElementById('mensajerestaurar').innerHTML =
                        `Usted esta restaurando el archivo ${nombreSinFecha}, esta usted ¿seguro que desea restaurar este archivo?.`;
                });
            }
            /* ELIMINAR */
            const listBtnDelete = document.getElementsByClassName('delete-file');
            for (let i = 0; i < listBtnDelete.length; i++) {
                const btn = listBtnDelete[i];
                btn.addEventListener('click', event => {
                    const nombreDocumento = btn.getAttribute('eliminarArchivo');
                    const nombreSinFecha = nombreDocumento.slice(nombreDocumento.indexOf("-") + 1);
                    btn.children[0].setAttribute('data-toggle', 'modal');
                    btn.children[0].setAttribute('data-target', '#eliminarDocumento');
                    console.log(btn.getAttribute('eliminarArchivo'));
                    document.getElementById('nombreDocumentoeliminar').value = nombreDocumento;
                    document.getElementById('mensajeeliminar').innerHTML =
                        `Es posible que esté eliminando datos importantes del proyecto. Después de restaurar ${nombreSinFecha}, no se puede recuperar.`;
                });
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $(document).ready(function() {
                const notificacion = (mensaje, icono) => {
                    Swal.fire({
                        position: 'top-end',
                        icon: icono,
                        text: mensaje,
                    }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
                }

                //Filtro Lista Archivos                
                $(document).on('click', '#filtroLista li a', function() {
                    $(this).addClass("active bg-light");
                    $(this).parent().siblings().children().removeClass("active bg-light")
                    const grupoActual = $(this).attr("grupo");
                    $("#listaArchivos li").show();
                    if (grupoActual == -1) {
                        $("#listaArchivos li").show();
                    } else {
                        $("#listaArchivos li").each(function() {
                            if ($(this).attr("grupo") != grupoActual) {
                                $(this).hide();
                            }
                        });
                    }
                });

            });

    </script>
    @endif
</body>
@endsection