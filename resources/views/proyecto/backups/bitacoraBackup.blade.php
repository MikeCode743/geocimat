<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <!-- ICONOS -->
    <script src="{{ asset('js\icons.js') }}"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <style>
        .note-editable {
            height: 500px;
            background-color: #fff;
        }

    </style>
</head>

<body>

    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.crear') }}">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.lista') }}">Listar Proyectos</a>
                </li>
            </ul>
        </div>
    </nav>


    <div class="card bg-transparent border-0 p-0 m-0 mt-3">
        <div class="card-header mx-auto bg-transparent border-0 p-0 m-0">
            <p class="h3">Bit&aacute;cora {{ $proyecto }}</p>
        </div>
        <div class="card-body ">
            <form>
                @csrf
                <input id="idProyecto" type="hidden" name="idProyecto" value="{{ $proyecto_id }}">
                <div class="form-row">
                    <div class="form-group col-md-12 mx-md-auto">
                        <textarea id="descripcion" name="descripcion" class="summernote"></textarea>
                    </div>
                    <div class="text-right col-md-12 mx-md-auto">
                        <button type="button" id="close" class="btn btn-secondary">Cancelar</button>
                        <button type="button" id="editar" class="btn btn-primary">Actualizar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        $(document).ready(function() {
            //WYSIWYG
            const summer = $('.summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table']
                ],
            });
            $('.summernote').summernote('code', '{!!  $detalle !!}');

            $('#editar').click(function() {
                const token = document.getElementsByName('_token')[0].value;
                const idProyecto = document.getElementsByName('idProyecto')[0].value;
                const descripcion = document.getElementsByName('descripcion')[0].value;
                let bodyData = new FormData();
                bodyData.append('_token', token);
                bodyData.append('idProyecto', idProyecto);
                bodyData.append('descripcion', descripcion);

                axios.post("{{ route('proyecto.bitacora.editar') }}", bodyData)
                    .then(res => {
                        const status = res.status;
                        const data = res.data;
                        if (status == 200) {
                            notificacion(data.message, 'success');
                        } else {
                            notificacion("Error Inesperado", 'error');
                        }
                    }).catch(function(error) {
                        if (error.response.status == 500) {
                            notificacion(error.response.data.message, 'error');
                        } else {
                            notificacion("Ooopss! Parece que hubo un error, revisa el log",
                                'error');
                        }
                    });
            });
            const notificacion = (mensaje, icono) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icono,
                    text: mensaje,
                });
            }
        });

    </script>
</body>

</html>
