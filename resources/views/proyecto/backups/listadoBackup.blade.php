<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>
</head>

<body>
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.crear') }}">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.lista') }}">Listar Proyectos</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="row col-md-8 mx-md-auto m-0 p-0">
        <div class="col">
            <p class="h1 mt-3">Proyectos</p>
        </div>
    </div>

    <div class="form-row col-md-8 mx-md-auto">
        <div class="col-md-5">
            <label>Nombre del proyecto</label>
            <input id="title" type="text" class="form-control">
        </div>
        <div class="col-md-5">
            <label>Descripci&oacuten</label>
            <input id="desc" type="text" class="form-control">
        </div>
        <p id="nullFilter" class="h5 mt-2">Resultado de busqueda 0</p>
    </div>

    @if ($proyectos)
        @foreach ($proyectos as $proyecto)
            <div class="col-md-8 mx-md-auto mt-3 rounded">

                <div class="card text-center">
                    <div class="card-header">
                        <p class="h5 mb-0">{{ $proyecto->nombre }}
                            {{ date_format(date_create($proyecto->fecha_inicio), 'd/m/Y') }}</p>
                    </div>
                    <div class="card-body pt-2">
                        <div class="row text-left">
                            <!-- Columna descripcion proyecto -->
                            <div class="col-md-6">
                                <h4><span class="badge badge-pill badge-info">{{ $proyecto->clasificacion }}</span></h4>
                                <p class="card-text">
                                    @if ($proyecto->descripcion)
                                        {!! $proyecto->descripcion !!}
                                    @else
                                        No se a agregado descripci&oacute;n.
                                    @endif
                                </p>
                            </div>
                            <!-- Columna imagen -->
                            <div class="coordenada col-md-6 mx-md-auto mt-3 text-right"
                                coordenada="{{ $proyecto->longitud }}, {{ $proyecto->latitud }}">
                                <div class="btn-group btn-group-toggle mb-1 mt-0 p-0" data-toggle="buttons">
                                    <button type="button" class="btn btn-primary btn-sm">Satellite</button>
                                    <button type="button" class="btn btn-secondary btn-sm">Outdoors</button>
                                    <button type="button" class="btn btn-secondary btn-sm">Streets</button>
                                </div>
                                <img src="https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/{{ $proyecto->longitud }},{{ $proyecto->latitud }},13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                                    class="img-fluid" alt="Responsive image">
                            </div>

                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <a href="{{ route('proyecto.bitacora', ['id' => $proyecto->proyecto_id]) }}"
                            class="btn btn-info">Bit&aacute;cora</a>
                        <a href="{{ route('proyecto.imagen', ['id' => $proyecto->proyecto_id]) }}"
                            class="btn btn-info">Galer&iacute;a</a>
                        <a href="{{ route('proyecto.archivos', ['id' => $proyecto->proyecto_id]) }}"
                            class="btn btn-dark">Ver Archivos</a>
                    </div>

                </div>
            </div>
        @endforeach
    @else
        <div class="col-md-8 mx-md-auto mt-3 rounded">
            <p class="h3">No hay proyecto</p>
        </div>
    @endif

    <script>
        $(() => {
            $("#nullFilter").hide();
            //Get cards
            const cardsAvailable = $('.card');

            // Search by title
            $(":input").keyup(function() {
                $("#nullFilter").hide();
                let inputTitle = $("#title").val().toLowerCase();
                let inputDesc = $("#desc").val().toLowerCase();
                //String empty show card                
                if (!inputDesc && !inputTitle) {
                    cardsAvailable.children().show();
                }
                let hasCard2Show = false;
                //Cards filter
                cardsAvailable.each(i => {
                    $(cardsAvailable[i]).hide();
                    // Card / Card Header / H5
                    const cardHeader = cardsAvailable[i].children[0].children[0].innerText
                        .toLowerCase();
                    // Card / Card Body / Row  
                    const cardBody = cardsAvailable[i].children[1].children[0].innerText
                        .toLowerCase();
                    if (cardHeader.includes(inputTitle) && cardBody.includes(inputDesc)) {
                        $(cardsAvailable[i]).show();
                        hasCard2Show = true;
                    }
                });
                if (!hasCard2Show) {
                    $("#nullFilter").show();
                }
            });

            //Change Image
            const divImage = $(".coordenada");
            divImage.each(function() {
                const coordenada = $(this).attr("coordenada");
                const btnGroup = $(this).children()[0];
                const imgResponsive = $($(this).children()[1]);

                //Click Satellite
                const satelliteBtn = $(btnGroup.children[0]);
                let urlSatellite =
                    "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/static/" +
                    coordenada +
                    ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                satelliteBtn.click(function() {
                    imgResponsive.attr("src", urlSatellite);
                });

                //Click Outdoors
                const outdoorsteBtn = $(btnGroup.children[1]);
                outdoorsteBtn.click(function() {
                    let urlOutdoors =
                        "https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/static/" +
                        coordenada +
                        ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                    imgResponsive.attr("src", urlOutdoors);
                });

                //Click Streets
                const streetseBtn = $(btnGroup.children[2]);
                streetseBtn.click(function() {
                    let urlStreet =
                        "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" +
                        coordenada +
                        ",13.58,0/500x500?access_token=pk.eyJ1Ijoia2VybmVsNTAzIiwiYSI6ImNrZHA5cmhiYTIwamgyeXBkOTgyZmU1cmkifQ.bK_Wbz4134Uf33qBDGklKg"
                    imgResponsive.attr("src", urlStreet);
                });
            });
        });

    </script>
</body>

</html>
