<!DOCTYPE html>
<html>

<head>
    <title>Formulario Proyecto</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />

    <!---->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!-- Robooto -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icons -->
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

    <!-- Loader  -->
    <!-- Revisar las alertas que muestra en consola  -->
    <script src="https://cdn.jsdelivr.net/npm/queryloader2@3.2.3/queryloader2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            new QueryLoader2(document.querySelector("body"), {
                barColor: "#efefef",
                backgroundColor: "#111",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });

    </script>

</head>

<body class="bg-light">
    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <a class="navbar-brand" href="">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.crear') }}">Agregar Proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('proyecto.lista') }}">Listar Proyectos</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- POST AGREGAR ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="archivo" tabindex="-1" role="dialog"
        aria-labelledby="photoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="idProyecto" value="{{ $id }}">
                    <div class="form-group">
                        <input name="file[]" type="file" class="form-control-file" id="ArrayArchivos" multiple>
                    </div>
                    <!--AQUI DEBE IR EL TIPO DE ARCHIVO -->
                </div>
                <div id="progress" class="progress">
                    <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%"
                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="modal-footer">
                    <button id="cancelarPost" type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cerrar</button>
                    <button id="subirArchivosPost" type="button" class="btn btn-primary">Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- POST Eliminar ARCHIVOS -->
    <div class="modal fade bd-example-modal-lg" id="eliminarDocumento" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('proyecto.archivos.eliminar') }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <label id="mensajeEliminar" class="text-danger">¿Esta seguro de eliminar el documento?</label>
                        <input name="idProyecto" value="{{ $id }}" type="hidden">
                        <input name="nombreDocumento" id="nombreDocumento" value="" type="hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container mt-3">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-10 m-auto p-0 m-0">
                <div class="card bg-transparent border-0">
                    <div class="card-header bg-transparent border-0 p-0 ">
                        <div class="d-flex m-0 p-0">
                            <div>
                                <p class="h3">Repositorio</p>
                            </div>
                            <div class="col text-right">
                                <a class="btn btn-light" href="{{ route('proyecto.imagen', ['id' => $id]) }}"
                                    role="button">Imagenes</a>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#archivo">
                                    <i class="fas fa-upload"></i> Subir Documento
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0 mt-2">
                        <!--
                        <ul class="nav nav-tabs" style="border-bottom-width: 0px">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Archivos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">C3</a>
                            </li>
                        </ul>
                    -->
                        @if (count($listaArchivos) === 0)
                            <p>A&uacute;n no se han agregado archivos.</p>
                        @else
                            <ul id="listaArchivos" class="list-group list-group-flush mt-0">
                                @foreach ($listaArchivos as $archivo)
                                    <li class="list-group-item list-group-item-action p-1 pl-2">
                                        <div class="d-flex m-0 p-0">
                                            <div class="open-file w-100"
                                                link="{{ route('repository', ['ruta' => $archivo->ruta_archivo]) }}"><i
                                                    class="fa fa-book" aria-hidden="true"></i>
                                                {{ $archivo->nombre }} </div>
                                            <div eliminarArchivo="{{ $archivo->nombre }}"
                                                class="delete-file ml-auto">
                                                <button type="button" class="btn btn-danger btn-sm m-0"><i
                                                        class="far fa-trash-alt"></i></button>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Agregar Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js">
        const axios = require('axios');

    </script>
    <script>
        const fileList = document.getElementsByClassName('open-file');
        for (let i = 0; i < fileList.length; i++) {
            const link = fileList[i]
            link.addEventListener('click', event => {
                const filelink = link.getAttribute('link')
                window.open(filelink, '_blank')
            });
        }
        const listBtnDelete = document.getElementsByClassName('delete-file');
        for (let i = 0; i < listBtnDelete.length; i++) {
            const btn = listBtnDelete[i];
            btn.addEventListener('click', event => {
                const nombreDocumento = btn.getAttribute('eliminarArchivo');
                const nombreSinFecha = nombreDocumento.slice(nombreDocumento.indexOf("-") + 1);
                btn.children[0].setAttribute('data-toggle', 'modal');
                btn.children[0].setAttribute('data-target', '#eliminarDocumento');
                console.log(btn.getAttribute('eliminarArchivo'));
                document.getElementById('nombreDocumento').value = nombreDocumento;
                document.getElementById('mensajeEliminar').innerHTML =
                    `Es posible que esté eliminando datos importantes del proyecto. Después de eliminar ${nombreSinFecha}, no se puede recuperar.`;
            });
        }

        $(document).ready(function() {
            $("#progress").hide();
            $('#subirArchivosPost').click(function() {
                $("#progress").show();
                $("#subirArchivosPost").attr('disabled', true);
                const token = document.getElementsByName('_token')[0].value;
                const idProyecto = document.getElementsByName('idProyecto')[0].value;
                const ArrayArchivos = document.getElementById('ArrayArchivos');
                let bodyData = new FormData();
                bodyData.append('_token', token);
                bodyData.append('idProyecto', idProyecto);
                for (const file of ArrayArchivos.files) {
                    bodyData.append('file[]', file, file.name);
                }
                axios.post("{{ route('proyecto.archivos.crear') }}", bodyData, {
                        onUploadProgress: uploadEvent => {
                            let progress = Math.round(uploadEvent.loaded /
                                uploadEvent.total * 100);
                            $("#progressbar").css("width", progress + '%');
                        }
                    })
                    .then(res => {
                        const status = res.status;
                        const data = res.data;
                        if (status == 200) {
                            notificacion(data.message +
                                ', para visualizar los cambios debe actualizar la página',
                                'success');
                        } else if (status == 500) {
                            notificacion(data.message, 'error');
                        } else {
                            notificacion("Error Inesperado", 'error');
                        }
                    }).catch(function(error) {
                        console.log(error);
                        notificacion("Error Inesperado revisar log", 'error');
                    }).then(function() {
                        $("#progress").hide();
                        ArrayArchivos.value = "";
                        $("#subirArchivosPost").removeAttr('disabled');
                        $("#progressbar").css("width", '0%');
                    });
            });

            $('#cancelarPost').click(function() {
                document.getElementById('ArrayArchivos').value = "";
            });
            const notificacion = (mensaje, icono) => {
                Swal.fire({
                    position: 'top-end',
                    icon: icono,
                    text: mensaje,
                }).then(result => result.isConfirmed ? location.reload() : console.log("pass"));
            }
        });

    </script>
</body>

</html>
