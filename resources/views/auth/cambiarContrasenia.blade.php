@extends('layouts.navbar')

@section('title', 'Cambiar Contraseña')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Cambiar Contraseña') }}</div>

                <div class="card-text col-form-label-lg text-center">Cambio de contraseña</div>
                <div class="card-body">
                    <form method="" aria-label="{{ __('Cambiar Contraseña') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña Actual ') }}<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>


                            <div class="col-md-6">
                                <input id="password_actual" type="password" class="form-control" name="password_actual" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña ') }}<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>


                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmed" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña ') }}<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>


                            <div class="col-md-6">
                                <input id="password_confirmed" type="password" class="form-control" name="password_confirmed" required>
                            </div>
                        </div>
                        <small class="form-text text-muted text-center">
                            Los campos con asterisco ( <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup> ) son requeridos.
                        </small>
                        <br>
                        <div class="form-group row mx-3">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" id="cambiar" name="cambiar" class="btn btn-primary ">
                                    {{ __('Cambiar Contraseña') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2');

</script>

<script type="text/javascript">
    $(document).ready(function() {
        console.log("ready!");
        $("#password_actual").focus();
        $('input[type="password"]').focus(function() {
            $("#password_actual").removeClass("is-invalid");
            $("#password").removeClass("is-invalid");
            $("#password_confirmed").removeClass("is-invalid");

        });
    });

    $("#cambiar").click(function() {
        var actual = $("#password_actual").val();
        var password = $("#password").val();
        var confirmed = $("#password_confirmed").val();
        const Toast = Swal.mixin({
            toast: true
            , position: 'top-end'
            , showConfirmButton: false
            , timer: 3000
            , timerProgressBar: true
            , didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })


        if (password.length >= 6 && confirmed.length >= 6 && actual.length >= 6) {

            if (password != confirmed) {
                $("#password").addClass("is-invalid");
                $("#password_confirmed").addClass("is-invalid");
                Toast.fire({
                    icon: 'error'
                    , title: 'La contraseñas no coinciden'
                })


            } else {
                Swal.fire({
                    title: 'Cambiar Contraseña'
                    , text: "¿Seguro de cambiar la contraseña?"
                    , showCancelButton: true
                    , confirmButtonColor: '#3085d6'
                    , cancelButtonColor: '#d33'
                    , confirmButtonText: 'SI, Seguro'
                    , cancelButtonText: 'Cancelar'
                }).then(function(isConfirm) {
                    if (isConfirm.value === true) {
                        axios.post('{{route('contrasenia.cambiar')}}'
                                , {
                                    actual: actual
                                    , password: password
                                    , confirmed: confirmed
                                })
                            .then(function(response) {
                                Toast.fire({
                                    icon: 'success'
                                    , title: response.data.message
                                });
                                $('input[type="password"]').val('');
                            })
                            .catch(e => {
                                console.log('error');
                                Toast.fire({
                                    icon: 'error'
                                    , title: e.response.data.message
                                });
                                $("#password_actual").addClass("is-invalid");
                                $('input[type="password"]').val('');
                            });
                    }
                }, function(dismiss) {
                    if (dismiss === 'cancel' || dismiss === 'close') {
                        // ignore
                    }
                })
            }



        } else {
            if (actual.length < 6) {
                $("#password_actual").addClass("is-invalid");
                Toast.fire({
                    icon: 'error'
                    , title: 'La contraseña actual debe tener almenor 6 caracteres'
                })

            }
            if (password.length < 6) {
                $("#password").addClass("is-invalid");
                Toast.fire({
                    icon: 'error'
                    , title: 'La contraseña debe tener almenor 6 caracteres'
                })

            }
            if (confirmed.length < 6) {
                $("#password_confirmed").addClass("is-invalid");
                Toast.fire({
                    icon: 'error'
                    , title: 'La contraseña debe tener almenor 6 caracteres'
                })

            }


        }


    });

</script>


@endsection
