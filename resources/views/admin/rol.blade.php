@extends('layouts.navbar')
@section('title', 'Administracion - Roles')

@section('content')

@include('admin.parcial.hasError')
@include('layouts.alerts')

<div class="col-md-12 mx-auto my-3">
  <div class="card">
    <div class="card-header bg-dark">
      <div class="row justify-content-between">
          <h5 class="card-title  float-left" style="color: white">Roles registrados</h5>
          <a href="#add_Modal" class="btn btn-success" data-toggle="modal"><i class="fas fa-plus"></i> <span>Agregar
              Rol</span></a>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col" width="20%">Nombre corto</th>
              <th scope="col" width="25%">Nombre</th>
              <th scope="col" width="40&">Descripcion</th>
              <th scope="col" width="10%">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @if($roles)
            @foreach($roles as $rol)
            <tr>
              <td>{{$rol->rol_id}}</td>
              <td>{{$rol->nombre_corto}}</td>
              <td>{{$rol->nombre}}</td>
              <td>{{$rol->descripcion}}</td>
              <td class="text-center">
                <button type="button" class="btn btn-primary btn-edit btn-sm" data-toggle="tooltip"
                  data-placement="bottom" title="Editar"><i class='fas fa-edit'></i></button>
                <button type="button" class="btn btn-danger btn-delete btn-sm" data-toggle="tooltip"
                  data-placement="bottom" title="Eliminar"><i class='fas fa-trash'></i></button>
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<!-- ADD Modal HTML -->
<div id="add_Modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="{{route('rol.store')}}" method="post" id="form_add">
        @csrf
        <div class="modal-header">
          <h4 class="modal-title">Agregar Rol</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {{-- Nombre Corto --}}
            <label>Nombre Corto del rol <sup><i class='fas fa-asterisk'
                  style='font-size:6px;color:red'></i></sup></label>

            <input minlength="6"  type="text" class="form-control {{$errors->has('nombre_corto') ? 'is-invalid' : ''}}" id="nombre_corto"
              name="nombre_corto" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')">
              @if($errors->has('nombre_corto'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('nombre_corto') }}</strong>
              </span>
              @endif
            {{-- Nombre --}}
            <label>Nombre del rol <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
            <input minlength="6"  type="text" class="form-control {{$errors->has('nombre') ? 'is-invalid' : ''}}" id="nombre"
              name="nombre" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')">

            @if($errors->has('nombre'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('nombre') }}</strong>
            </span>
            @endif
            {{-- Descripcion --}}
            <label>Descripcion del rol <sup><i class='fas fa-asterisk'
                  style='font-size:6px;color:red'></i></sup></label>
     <textarea minlength="6" class="form-control {{$errors->has('descripcion') ? 'is-invalid' : ''}}" id="descripcion" name="descripcion" rows="3" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')"></textarea>
            @if($errors->has('descripcion'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('descripcion') }}</strong>
            </span>
            @endif
            <br>
            <small class="form-text text-muted">
              Los campos con asterisco ( * ) son requeridos.
            </small>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar <i class="fa">&#xf00d;</i></button>
          <button type="submit" class="btn btn-primary">Agregar <i class="fa fa-plus-circle"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Modal HTML -->
<div id="modal-edit" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="" id="form-edit">
        @csrf
        @method('PATCH')
        <div class="modal-header">
          <h4 class="modal-title">Editar Rol</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {{-- Nombre corto --}}
            <label>Nombre del Rol</label>

            <input minlength="6" type="text" class="form-control {{$errors->has('edit_nombre_corto') ? 'is-invalid' : ''}}"
              id="edit_nombre_corto" name="edit_nombre_corto" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')">
              @if($errors->has('edit_nombre_corto'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('edit_nombre_corto') }}</strong>
              </span>
              @endif
            {{-- Nombre --}}
            <label>Nombre del Rol</label>
            <input  minlength="6"  type="text" class="form-control {{$errors->has('edit_nombre') ? 'is-invalid' : ''}}" id="edit_nombre" name="edit_nombre" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')">
            @if($errors->has('edit_nombre'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('edit_nombre') }}</strong>
            </span>
            @endif
            {{-- Descripcion --}}
            <label>Descripcion</label>
            <textarea minlength="6"  class="form-control {{$errors->has('edit_descripcion') ? 'is-invalid' : ''}}"  id="edit_descripcion" name="edit_descripcion" rows="3" required="" oninvalid="this.setCustomValidity('No se puede dejar vacío')" oninput="setCustomValidity('')"></textarea>
            @if($errors->has('edit_descripcion'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('edit_descripcion') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar <i class="fa">&#xf00d;</i></button>
          <button type="submit" class="btn btn-primary">Guardar <i class='far fa-save'></i></button>
        </div>
      </form>
    </div>
  </div>
</div>

{{-- Eliminar Modal --}}
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST" id="form-delete">
        @csrf
        @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title" id="">Eliminar Rol</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="_method" value="DELETE">
          <p>
            Esta seguro de querer eliminar el rol seleccionado?
          </p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="submit">Si, Seguro</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function (){
     var table = $('#datatable').DataTable({ 
      "language": {
                  "lengthMenu": "Mostrar _MENU_ registros",
                  "zeroRecords": "No se encontraron resultados",
                  "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                  "sSearch": "Buscar:",
                  "oPaginate": {
                      "sFirst": "Primero",
                      "sLast":"Último",
                      "sNext":"Siguiente",
                      "sPrevious": "Anterior"
  			     },
  			     "sProcessing":"Procesando...",
              },
      "columnDefs": [    { "visible": false, "targets": 0 }  ],
      "responsive": "true"        
     });

    $('#add_Modal').on('shown.bs.modal', function (e){
      $('#form_add').get(0).reset();
      $('#nombre_corto').focus();
    });
  
    $('#modal-edit').on('shown.bs.modal', function (e){
        var data = table.row($(this).data('row')).data();
        $('#form-edit').attr('action', window.location.pathname+'/'+data[0]);
        $('#edit_nombre_corto').val(data[1]).focus();
        $('#edit_nombre').val(data[2]);
        $('#edit_descripcion').val(data[3]);
        $('#form-edit').attr('method', 'post');      
    }); 
  
    table.on('click','.btn-edit',function(){
      $('#form-edit').get(0).reset();
      $tr = $(this).closest('tr');
      $('#modal-edit').data('row', $tr);
      if($($tr).hasClass('child')){
          	$tr=$tr.prev('.parent');
      }
      $('#modal-edit').modal('show');
    });
  
    table.on('click','.btn-delete',function(){    
      $tr = $(this).closest('tr');
      $('#modal-delete').data('row', $tr);
      if($($tr).hasClass('child')){
          	$tr=$tr.prev('.parent');
      }
      var data = table.row($tr).data();  
      $('#form-delete').attr('action', window.location.pathname+'/'+data[0]);
      $('#modal-delete').modal('show');
     });
    });
  
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })  
</script>


@endsection