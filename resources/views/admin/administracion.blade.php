@extends('layouts.navbar')
@section('title', 'Administracion')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="list-group">
                <a href="" class="list-group-item bg-dark list-group-item-action active"> <i class='fas fa-tools mr-2' style='font-size:16px'></i>Administraci&oacute;n</a>
                <a href="{{ route('clasificacion.home') }}" class="list-group-item list-group-item-action"> <i class='far fa-newspaper mr-2' style='font-size:16px'></i>Clasificaci&oacute;n</a>

                <a href="{{ route('home.especialidades') }}" class="list-group-item list-group-item-action"> <i class='fas fa-chalkboard-teacher mr-2' style='font-size:16px'></i>Especialidades</a>

                <a href="{{ route('etapa.home') }}" class="list-group-item list-group-item-action"> <i class='fas fa-project-diagram mr-2' style='font-size:16px'></i>Etapas</a>

                <a href="{{ route('grupo.home') }}" class="list-group-item list-group-item-action"> <i class='fas fa-archive mr-2' style='font-size:16px'></i>Grupos</a>

                @if(Auth::user()->hasRole('admin'))
                <a href="{{ route('rol.home') }}" class="list-group-item list-group-item-action"> <i class='far fa-id-card mr-2' style='font-size:16px'></i>Roles</a>
                <a href="{{ route('permisos.home') }}" class="list-group-item list-group-item-action"> <i class='fas fa-shield-alt mr-2' style='font-size:16px'></i>Permisos</a>
                <a href="{{ route('permisosrol.home') }}" class="list-group-item list-group-item-action"> <i class='fas fa-user-shield mr-2' style='font-size:16px'></i>Permisos & Roles</a>
                @endif

                <a href="{{ route('personal.home') }}" class="list-group-item list-group-item-action"> <i class='fas fa-users mr-2' style='font-size:16px'></i>Personal</a>

            </div>
        </div>
    </div>
</div>
@endsection
