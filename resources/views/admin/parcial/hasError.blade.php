
@if($errors->has('nombre_corto'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Nombre Corto - Upps, Ocurrio un error! &nbsp;{{ $errors->first('nombre_corto') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>

@elseif($errors->has('nombre'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Nombre - Upps, Ocurrio un error!&nbsp;{{ $errors->first('nombre') }}.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@elseif($errors->has('descripcion'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Descripcion - Upps, Ocurrio un error!&nbsp;{{ $errors->first('descripcion') }}.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if($errors->has('edit_nombre_corto'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Editar Nombre Corto - Upps, Ocurrio un error!&nbsp;{{ $errors->first('edit_nombre_corto') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@elseif($errors->has('edit_nombre'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Editar Nombre - Upps, Ocurrio un error!&nbsp;{{ $errors->first('edit_nombre') }}.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@elseif($errors->has('edit_descripcion'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Campo: Editar Descripcion - Upps, Ocurrio un error!&nbsp;{{ $errors->first('edit_descripcion') }}.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif