@extends('layouts.navbar')
@section('title', 'Permisos-Roles')

@section('content')
@include('layouts.alerts')
<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-dark">
            <div class="row justify-content-between">
                    <h5 class="card-title " style="color: white">Permisos registrados</h5>
                    <a href="#add_permiso" class="btn btn-primary" data-toggle="modal"><i
                        class="fas fa-plus mr-2"></i><span>Agregar permiso</span></a>    
                    <a href="#asig_permiso" class="btn btn-success" data-toggle="modal"><i class="fas fa-plus"></i>
                        <span>Asignar permisos</span></a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive-xl">
                <table id="datatable" class="table table-striped responsive nowrap table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>id Rol</th>
                            <th>id permiso</th>
                            <th scope="col" width="20%">Rol</th>
                            <th scope="col" width="20%">Permiso</th>
                            <th scope="col" width="20%">Nombre del Permiso</th>
                            <th scope="col" width="30%">Descripcion</th>
                            <th scope="col" width="10%">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($permisos_roles)
                        @foreach($permisos_roles as $permiso)
                        <tr>
                            <td>{{ $permiso->rol_id }}</td>
                            <td>{{ $permiso->permiso_id }}</td>
                            <td>{{ $permiso->rol_nombre }}</td>
                            <td>{{ $permiso->permiso_nombre_corto }}</td>
                            <td>{{ $permiso->permiso_nombre }}</td>
                            <td>{{ $permiso->permiso_descripcion }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger btn-delete btn-sm" data-toggle="tooltip"
                                        data-placement="bottom" title="Eliminar"><i class='fas fa-trash'></i></button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <h2>No Hay permisos</h2>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>`
<!-- ADD Modal HTML -->
<div id="add_permiso" name="add_permiso" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('permisos.store') }}" method="post" id="form_add_permiso" name="form_add_permiso">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Permisos</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input name="_method" type="hidden" value="POST">
                    <div class="form-group">
                        <label>Nombre Corto del permiso <sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup></label>
                        <input type="text" class="form-control {{$errors->has('permiso') ? 'is-invalid' : ''}}"
                            id="nombre_corto" name="nombre_corto" maxlength="50">
                        @if($errors->has('nombrecorto'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nombrecorto') }}</strong>
                        </span>
                        @endif
                        <label>Nombre del permiso <sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup></label>
                        <input type="text" class="form-control {{$errors->has('nombre') ? 'is-invalid' : ''}}"
                            id="nombre" name="nombre" maxlength="50">

                        @if($errors->has('nombre'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                        @endif
                        <label>Descripcion <sup><i class='fas fa-asterisk'
                                    style='font-size:6px;color:red'></i></sup></label>
                        <textarea class="form-control {{$errors->has('Descripcion') ? 'is-invalid' : ''}}"
                            name="descripcion" id="descripcion" cols="30" rows="10" maxlength="255"></textarea>
                        <small class="form-text text-muted">
                            Los campos con asterisco ( * ) son requeridos.
                        </small>
                        @if($errors->has('Descripcion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('Descripcion') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar <i class="fa">&#xf00d;</i></button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="asig_permiso" name="asig_permiso" class="modal fade ">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('permisosrol.store') }}" method="post" id="form_add_permiso" name="form_add_permiso">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Permisos a Roles</h4>
                </div>
                <div class="modal-body">
                    <input name="_method" type="hidden" value="POST">
                    <div class="form-group">
                        @if($roles and $permisos)
                        <label for="rol" id="rol" name="rol">lista de permisos:</label>
                        <select class="selectpicker form-control show-tick" data-live-search="true" id="rol" name="rol">
                            @foreach ($roles as $rol)
                            <option value="{{$rol->rol_id}}">{{ $rol->nombre }}</option>
                            @endforeach
                        </select> <br>
                        <label for="permisos">lista de permisos:</label>
                        <select class="selectpicker form-control show-tick" data-live-search="true" id="permisos"
                            name="permisos[]" multiple>
                            @foreach ($permisos as $permiso)
                            <option value="{{$permiso->permiso_id}}">{{ $permiso->nombre }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar <i class="fa">&#xf00d;</i></button>
                    <button type="submit" class="btn btn-primary">Guardar <i class='far fa-save'></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Eliminar Modal --}}
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="POST" id="form-delete">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="">Eliminar Especialidad</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_method" value="DELETE">
                    <p>
                        ¿Está seguro de querer eliminar el permiso del rol seleccionado?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="submit">Sí, seguro</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
        $('.rol').selectpicker('deselectAll');
        $('.permiso').selectpicker('deselectAll');
        var table = $('#datatable').DataTable({ 
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        "columnDefs": [    
            {"visible": false, "targets": [0,1]}
        ],
        "responsive": "true"        
    });


  $('#add_Modal_Permiso').on('shown.bs.modal', function (e){
    $('#form_add_permiso').get(0).reset();
    $('#nombre_corto').focus();
  });


  table.on('click','.btn-delete',function(){    
    $tr = $(this).closest('tr');
    $('#modal-delete').data('row', $tr);
    if($($tr).hasClass('child')){
        	$tr=$tr.prev('.parent');
    }
    var data = table.row($tr).data();  
    $('#form-delete').attr('action', window.location.pathname+'/'+data[0]+'/'+data[1]);
    $('#modal-delete').modal('show');
   });
  });



  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })  
</script>
@endsection