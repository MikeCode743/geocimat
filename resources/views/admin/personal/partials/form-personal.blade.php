
@if($formulario_crear)
<!-- Crear -->
<form action="{{route('personal.store')}}" method="post">
@else
<!-- Actualizar -->
<form action="{{route('personal.update', [$integrante->personal_id])}}" method="post">
@endif
  @csrf
  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Nombre <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{old('nombres')}}">
        @else
        <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{$integrante->nombres}}">
        @endif
    @if($errors->has('nombres'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('nombres') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Apellido <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{old('apellidos')}}">
        @else
        <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{$integrante->apellidos}}">
        @endif
    @if($errors->has('apellidos'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('apellidos') }}</strong>
      </span>    
    @endif
      </div>
  </div>
  
  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Email<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
        @if($formulario_crear)
        <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{old('email')}}">
        @else
        <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{$usuario->email}}">
        @endif
        
        @if($errors->has('email'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
          </span>    
        @endif
        
        @if(Session::has('error_email_existe'))
            <span class="invalid-feedback  d-block" role="alert">
              <strong>{{Session::get('error_email_existe')}}</strong>
            </span>    
        @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Password</label>
        <div class="input-group mb-3">
        @if($formulario_crear)
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="{{!empty(old('password')) ? old('password'): 'cimat1234'}}">
        @else
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="" placeholder="(Opcional) modificar contraseña">
        @endif
        <div class="input-group-append">
        <button class="btn btn-primary" type="button" onclick="mostrarPassword()"><i class='fas fa-eye mr-2'></i>Mostrar</button>
        </div>
        </div>
        @if($errors->has('password'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('password') }}</strong>
      </span>    
    @endif
      </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-6">
        <label for="inputEmail4">Username <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" id="input-usuario" name="name" value="{{old('name')}}">
        @else
        <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" id="input-usuario"  name="name" value="{{$usuario->name}}">
        <input type="hidden" id="input-usuario-comp"  name="namecomp" value="{{$usuario->name}}">
        @endif

        @if($errors->has('name'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
          </span>    
        @endif
        
        @if(Session::has('error_name_existe'))
            <span class="invalid-feedback d-block" role="alert">
              <strong>{!!Session::get('error_name_existe')!!}</strong>
            </span>    
        @endif
      </div>

      <div class="form-group col-md-6">
        <label>Número de contacto <sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('telefono') ? 'is-invalid' : ''}}" name="telefono" id="input-telefono" value="{{old('telefono')}}" >
        @else
        <input type="text" class="form-control {{$errors->has('telefono') ? 'is-invalid' : ''}}" id="input-telefono"  name="telefono" value="{{$integrante->telefono}}">
        @endif
        @if($errors->has('telefono'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('telefono') }}</strong>
        </span>    
      @endif
      </div>
  </div>

  <div class="form-row"> 
  <div class="form-group col-md-6">
  <label for="input-roles">Especialidades:<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
    <select class="selectpicker" id="specSelect" data-width="100%" title="Selección multiple" multiple id="input-roles" name="especialidades[]">
      @if($especialidades)
        @foreach($especialidades as $especialidad)
          <option value="{{$especialidad->especialidad_id}}" {{collect(old('especialidades'))->contains($especialidad->especialidad_id) ? 'selected="true"' : ''}}>{{$especialidad->nombre}}</option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>

    <div class="form-group col-md-6">
    <label for="input-roles">Roles:<sup><i class='fas fa-asterisk' style='font-size:6px;color:red'></i></sup></label>
    <select class="selectpicker" id="rolSelect" data-width="100%" multiple title="Selección multiple" id="input-roles" name="roles[]">
      @if($roles)
        @foreach($roles as $rol)
          @if(!($rol->nombre_corto == 'administrador') || Auth::user()->hasRole('admin') )
          <option value="{{$rol->rol_id}}" {{collect(old('roles'))->contains($rol->rol_id) ? 'selected="true"' : ''}}>{{$rol->nombre}}</option>
          @endif
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>
  </div>
  <br>
  <small class="form-text text-muted">
    Los campos con asterisco ( * ) son requeridos.
  </small>
  <br>
  @if($formulario_crear)
  <button type="submit" class="btn btn-primary"><i class='fas fa-plus mr-2'></i>Registrar</button>
  @else
  <button type="submit" class="btn btn-primary" onClick="mascaraPassword()"><i class='fas fa-plus mr-2'></i>Modificar</button>
  @endif
  <a class="btn btn-secondary" href="{{route('personal.home')}}" role="button"><i class='fas fa-exclamation mr-2'></i>Cancelar</a>
</form>

@if($formulario_crear)
@else
<script type="text/javascript">
  var especialidades = [];
  @foreach($personal_especialidades as $p)
  especialidades.push({{$p->especialidad_id}});
  @endforeach
  var roles = [];
  @foreach($user_roles as $p)
  roles.push({{$p->rol_id}});
  @endforeach
  $('#specSelect').selectpicker('val', especialidades);
  $('#rolSelect').selectpicker('val', roles);
</script>
@endif
