@extends('layouts.navbar')
@section('title', 'Perfil')

@section('content')
<div class="col-md-10 mx-auto my-3">
	<div class="card">
	  	<div class="card-header">
	    	<h5 class="card-title">Datos del Integrante</h5>
	  	</div>
	  	<div class="card-body">
	  		<table class="table table-bordered">		  
			  <tbody>
			  	<tr>
			      <th scope="row">Nombre de usuario:</th>
			      <td>{{$usuario->name}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Email:</th>
			      <td>{{$usuario->email}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Nombres:</th>
			      <td>{{$integrante->nombres}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Apellidos:</th>
			      <td>{{$integrante->apellidos}}</td>
				</tr>
				<tr>
					<th scope="row">Telefono:</th>
					<td>{{$integrante->telefono}}</td>
				  </tr>
			    <tr>
					<th scope="row">Especialidad:</th>
					<td>
						@if($especialidades)
							@foreach($especialidades as $esp)
								<span class="badge badge-pill badge-success">{{$esp->nombre}}</span>
							@endforeach
						@endif
					</td>			    
				</tr>
			    <tr>
			      <th scope="row">Roles:</th>
			      <td>
			      	@if($roles)
				      	@foreach($roles as $rol)
				      		<span class="badge badge-pill badge-success">{{$rol->nombre}}</span>
				      	@endforeach
			      	@endif
			      </td>
			    </tr>
			  </tbody>
			</table>			
		</div>
		<div class="card-footer">
			<a class="btn btn-primary" href="{{route('personal.home')}}" role="button">Aceptar</a>
		</div>
	</div>
</div>

@endsection