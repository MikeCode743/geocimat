@extends('layouts.navbar')
@section('title')
	Personal
@endsection


@section('content')
<div class="col-md-10 mx-auto my-3">
	<div class="card">
	  	<div class="card-header">
	    	<h5 class="card-title">Modificar Integrante</h5>
	  	</div>
	  	<div class="card-body">
	  		@include('admin.personal.partials.form-personal')
		</div>
	</div>
</div>

<script type="text/javascript">
	function mostrarPassword(){
      var input_password = document.getElementById("input-password");
      if(input_password.getAttribute("type") == "password"){
          input_password.setAttribute("type", "text");
      }else{
          input_password.setAttribute("type", "password");
      }
  	}

  	function mascaraPassword(){
  		var input_password = document.getElementById("input-password");
  		if(input_password.getAttribute("value") == ""){ input_password.setAttribute("value", "000000"); }
  	}
</script>

@endsection
