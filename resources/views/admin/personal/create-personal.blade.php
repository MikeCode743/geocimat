@extends('layouts.navbar')
@section('title', 'Personal')

@section('content')
<div class="col-md-12 mx-auto my-3">
	<div class="card">
	  	<div class="card-header">
	    	<h5 class="card-title">Registrar Integrante</h5>
	  	</div>
	  	<div class="card-body">
	  		@include('admin.personal.partials.form-personal') 		
		</div>
	</div>
</div>

<script type="text/javascript">
	function mostrarPassword(){
      var input_password = document.getElementById("input-password");
      if(input_password.getAttribute("type") == "password"){
          input_password.setAttribute("type", "text");
      }else{
          input_password.setAttribute("type", "password");
      }
  }
</script>

@endsection