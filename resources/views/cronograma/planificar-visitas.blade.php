@extends('layouts.navbar')

@section('cronograma-css')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.css">
@endsection

@section('cronograma-js')
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.js"></script>	
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/locales-all.min.js"></script>
	<script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
@endsection

@section('title', 'Asignar Visita')


<style type="text/css">
	.border-test{
		border: 1px dotted;
	}

	#external-events {	  
	  position: relative;
	  width: 350px;
	  padding: 0 10px;
	  border: 1px solid #ccc;
	  background: #eee;	  
	  
	  overflow-x: scroll;
	  overflow-y: scroll;		
	  min-width: 50px;	  
	  margin: 0px auto;
	  min-height: 200px;
	  max-height: 600px;
	}

	#external-events .fc-event {
	  cursor: move;
	  margin: 3px 0;	 
	}

	#external-events .fc-event .fc-event-main{
		overflow: hidden;		
	}

	.en-lista{
		width: 500px;
	}

	#calendar-container {
	  position: relative;
	  z-index: 1;
	  margin: auto auto;
	}

	#calendar {
	  max-width: 800px;	  
	  margin: auto auto;
	}

	html, body{
	  display: flex;
	  flex-direction: column;
	  height:100vh;
	  margin 0;
	}
</style>

@section('content')
<div class="">
	<div class="card">
	  	<div class="card-header">
	    	<h5 class="card-title float-left">Cronograma de visitas</h5>
	    	<a href="{{route('cronograma.home')}}" class="btn btn-info btn-md active float-right mx-2" role="button" aria-pressed="true">Terminar</a>
	    	<a href="{{route('cronograma.visitas.planificar')}}" class="btn btn-secondary btn-md active float-right mx-2" role="button" aria-pressed="true">Descartar</a>
	    	<a id="btn-agregar-visitas" class="btn btn-success btn-md active float-right mx-2" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Guardar</a>	    	
	  	</div>
	  	<div class="card-body">
			<div class="row mb-3">			  		
	  			<div class=" mx-auto"><a href="" type="button" class="btn btn-primary"></a> Todas</div>
			  	<div class=" mx-auto"><a href="" type="button" class="btn btn-success"></a> Terminadas</div>
			  	<div class=" mx-auto"><a href="" type="button" class="btn btn-info"></a> En Proceso</div>				  	
			  	<div class=" mx-auto"><a href="" type="button" class="btn btn-secondary"></a> A Visitar</div>
			  	<div class=" mx-auto"><a href="" type="button" class="btn btn-danger"></a> Canceladas</div>
			</div><!---->

			<div class="row" style="height: 100%">
		  		<div id='external-events' class="">
				  <p>
				    <strong>Proyectos</strong>
				  </p>
				  @foreach($proyectos as $proyecto)
					  <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event en-lista'>
					    <div class='fc-event-main'>{{$proyecto->proyecto_id}}</div>
					  </div>
				  @endforeach
				</div>

				<div id='calendar-container' class="col-md-8">
				  <div id='calendar'></div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-form-visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Visita (<span id="modal-title-nombre"></span>)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-visita">
        	<input id="modal-textfield-id-visita" type="text" name="id" hidden="true">
        	<div class="form-group">
				<label for="modal-select-etapa-visita">Etapa:</label>
				<select class="form-control" id="modal-select-etapa-visita" value="-1">
					<option value="Terminada">Terminada</option>
					<option value="En Proceso">En Proceso</option>
					<option value="A Visitar" selected="true">A Visitar</option>
					<option value="Cancelada">Cancelada</option>				
				</select>
			</div>
			<div class="form-group">
				<label for="modal-textarea-descripcion">Descripción</label>
    			<textarea class="form-control" id="modal-textarea-descripcion" rows="3"></textarea>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button id="btn-modal-aceptar" type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!-- Agregar Axios -->
<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>

<script type="text/javascript">
	let listaEventosModificados = [];
	let listaEventosRecienCreados = [];
	let eventosCalendario = [];
	let datosVisitas = {!!$datosVisitas!!};
	let fechaFinControl = {!!$fechaFinControl!!};
	
	//funcion que agrega los datos del elemento modificado o agregado a la lista que sera enviada para almacenar
	function agregarEnListas(eventoTemp){		
		let idTemp = parseInt(eventoTemp.id, 10);
		//comprueba si el id es string(nuevo)
		if(isNaN(idTemp)){
			idTemp = eventoTemp.id;
		}

		switch(typeof(idTemp)){
			case "string"://si el id tiene formato: 'id_proyecto|numero'				
				let creadoMod = listaEventosRecienCreados[idTemp.split("|")[1]];
				creadoMod.start = eventoTemp.startStr;
				creadoMod.end = eventoTemp.endStr;
				
				creadoMod.etapa_visita = eventoTemp.extendedProps.etapa_visita;
				creadoMod.descripcion_visita = eventoTemp.extendedProps.descripcion_visita;
				
				listaEventosRecienCreados[idTemp.split("|")[1]] = creadoMod;
				break;
			case "number"://si el id es un numero
				console.log("id es numero, evento a actualizar", idTemp);
				let encontrado = false
				//busca en los eventos modificados, por si se le hace otra modificacion al mismo
				for(let i = 0; i < listaEventosModificados.length; i++){
					if(listaEventosModificados[i].idTemporal == idTemp){
						let modificado = listaEventosModificados[i];
						modificado.start = eventoTemp.startStr;
						modificado.end = eventoTemp.endStr;
						
						modificado.etapa_visita = eventoTemp.extendedProps.etapa_visita;
						modificado.descripcion_visita = eventoTemp.extendedProps.descripcion_visita;
						
						listaEventosModificados[i] = modificado;
						encontrado = true;
						console.log("encontrado");
						break;
					}
				}
				//si no lo encontro lo agrega
				if(!encontrado){
					listaEventosModificados.push({
						idTemporal: idTemp,
						title: eventoTemp.title,
						start: eventoTemp.startStr,
						end: eventoTemp.endStr,
						
						etapa_visita: eventoTemp.extendedProps.etapa_visita,
						descripcion_visita: eventoTemp.extendedProps.descripcion_visita
					});
				}
				break;
		}

	}

	document.addEventListener('DOMContentLoaded', function() {
		var Calendar = FullCalendar.Calendar;	
		var Draggable = FullCalendar.Draggable;

		var containerEl = document.getElementById('external-events');//panel izquierdo
		var calendarEl = document.getElementById('calendar');//el calendario		

		// inicializa el comportamiento de los eventos del panel de la izquierda
		// -----------------------------------------------------------------
		new Draggable(containerEl, {			
			itemSelector: '.fc-event',
			eventData: function(eventEl) {
			  return {
			  	//id tempooral de los eventos nuevos para que no se confunda con los que vienen de la base de datos
			  	id: eventEl.innerText + "|" + listaEventosRecienCreados.length,
			    title: eventEl.innerText,//el id del proyecto			    
			  };
			}			
		});

		//preparar los eventos a mostrar en el calendario
		let datosVisitas = {!!$datosVisitas!!};
		let fechaFinControl = {!! $fechaFinControl !!};//contiene las fechas de finalizacion modificadas para que el calendario las muestre bien
		let eventosCalendario = [];//las visitas almacenadas que se mostraran en el calendario
		let i = 0;
		//preparar los eventos 
		datosVisitas.forEach((datoVisita) => {
			let clases = [];			
			switch(datoVisita.etapa_visita){
				case "Terminada": clases = ['bg-success']; break; // terminado verde success
				case "En Proceso": clases = ['bg-info']; break; // en Proceso celeste info
				case "A Visitar": clases = ['bg-secondary']; break; // a visitar gris secondary    <>
				case "Cancelada": clases= ['bg-danger']; break; // Cancelado rojo danger
			}
			eventosCalendario.push({
				id: datoVisita.visita_id,
				title: datoVisita.proyecto_id,
				start: datoVisita.fecha_inicio,
				end: fechaFinControl[i],//fecha fin que asigno fullcalendar
				classNames: clases,
				//informacion adicional de la visita
				extendedProps: {
					//nombre_proyecto: datoVisita.nombre,
					fecha_fin: datoVisita.fecha_fin, //fecha fin real a mostrar
					etapa_visita: datoVisita.etapa_visita,
					descripcion_visita: datoVisita.descripcion
				}
			});
			i++;
		});

		// initialize the calendar
		// -----------------------------------------------------------------
		var calendar = new Calendar(calendarEl, {
			locale: 'es',
			headerToolbar: {			  
			  left: 'title',
			  right: 'prev,next today'
			},
			editable: true,
			droppable: false, // this allows things to be dropped onto the calendar
			selectable:true,
			events: eventosCalendario,			
			dateClick: function(info) {/*cuando se le da click a una casilla*/},
			eventClick(eventClickInfo){
				//prepara el formulario con los valores anteriormente ingresados(si los hay)
				$("#modal-title-nombre").html(eventClickInfo.event.title.substring(0,eventClickInfo.event.title.length-6));
				$("#modal-textfield-id-visita").val(eventClickInfo.event.id);				
				$("#modal-select-etapa-visita").val(eventClickInfo.event.extendedProps.etapa_visita);
				$("#modal-textarea-descripcion").val(eventClickInfo.event.extendedProps.descripcion_visita);
				$("#modal-form-visita").modal("show");
			},			
			drop: function(info) {/*cuando pone un nuevo evento en el calendario */},
			//recibe el nuevo evento recien agregado para crearlo y lo mete en la lista respectiva
			eventReceive: function(info){
				listaEventosRecienCreados.push({
					idTemporal: info.event.id,
					title: info.draggedEl.innerText,//trae el id del proyectog
					start: info.event.startStr,
					end: info.event.endStr,
					etapa_visita: "",
					descripcion_visita: ""
				});				
			},	
			//fullcalendar lo ejecuta cuando se suelta el evento en una casilla diferente para cambiarlo de fecha		
			eventDrop: function(eventDropInfo){
				agregarEnListas(eventDropInfo.event);				
			},
			//fullcalendar lo ejecuta y actualiza la fecha bien cuando se redimensiona, para cambiar su duracion						
			eventResize: function(eventResizeInfo){
				agregarEnListas(eventResizeInfo.event);
			},//eventDragStop: function(info){},eventResizeStop: function(info){}
		});

		console.log("eventos iniciales traidos de la base", calendar.getEvents());

		calendar.render();		
		
		//accion del boton para funcion de agregar las visitas(eventos) agregadas al calendario
		let btnAgregarVisitas = document.getElementById('btn-agregar-visitas');
		btnAgregarVisitas.addEventListener('click', function() {
			let visitasJSON = JSON.stringify({
				eventosCrear: listaEventosRecienCreados,
				eventosModificar: listaEventosModificados
			});

			axios.post(
				'{{route('cronograma.visitas.agregar')}}',
				visitasJSON
			).then(function(res) {
				if(res.status==200) {
			  		location.reload();
				}else{}
				console.log(res);
			}).catch(function(err) {
				console.log(err);
			});
		});

		//accion boton para agregar la descripcion y la etapa de las visitas		
		$('#btn-modal-aceptar').click(function(){
			let eventoTemp;
			let idVisita = $("#modal-textfield-id-visita").val();
			let etapaVisita = $("#modal-select-etapa-visita").val();
			let descripcionVisita = $("#modal-textarea-descripcion").val();

			eventoTemp = calendar.getEventById(idVisita);
			eventoTemp.setExtendedProp("etapa_visita", etapaVisita);
			eventoTemp.setExtendedProp("descripcion_visita", descripcionVisita);

			agregarEnListas(eventoTemp);
			$("#modal-form-visita").modal("hide");
		});
	});
	
</script>

@endsection