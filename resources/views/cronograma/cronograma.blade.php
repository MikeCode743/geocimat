@extends('layouts.navbar')
@section('cronograma-css')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.css">
@endsection

@section('cronograma-js')
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.js"></script>	
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/locales-all.min.js"></script>	
@endsection

@section('title', 'Asignar Visita')


<style type="text/css">
	#calendar {
	  max-width: 1100px;
	  margin: 20px auto;
	}
</style>

@section('content')
<div class="col-md-12 mx-auto my-3">
	<div class="card">
	  	<div class="card-header">
	  		<div class="">
	    	<h5 class="card-title float-left">Cronograma de visitas</h5>	    	
	    	@if(Auth::user()->hasAnyRole('admin') || Auth::user()->hasAnyRole('investigador'))	    		
	    		<a href="{{route('proyecto.lista')}}" class="btn btn-secondary btn-md active float-right mx-2" role="button" aria-pressed="true">Regresar</a>	    		    		
	    		<a href="{{route('cronograma.visitas.planificar')}}" class="btn btn-success btn-md active float-right mx-2" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Planificar Visitas</a>	    	
	    	@endif
	    	</div>
	  	</div>
	  	<div class="card-body">
	  		@if(Auth::user()->hasAnyRole('admin'))
		  		<div class="row">
			  		<div class="btn-group mx-auto" role="group" aria-label="Basic example">
			  			<a href="{{route('cronograma.home')}}" type="button" class="btn btn-primary">Todas</a>
					  	<a href="{{route('cronograma.visitas.terminadas')}}" type="button" class="btn btn-success">Terminadas</a>
					  	<a href="{{route('cronograma.visitas.proceso')}}" type="button" class="btn btn-info">En Proceso</a>					  	
					  	<a href="{{route('cronograma.visitas.visitar')}}" type="button" class="btn btn-secondary">A visitar</a>
					  	<a href="{{route('cronograma.visitas.canceladas')}}" type="button" class="btn btn-danger">Canceladas</a>
					</div>
				</div>				
			@else
				<div class="row">			  		
		  			<div class=" mx-auto"><a href="" type="button" class="btn btn-primary"></a> Todas</div>
				  	<div class=" mx-auto"><a href="" type="button" class="btn btn-success"></a> Terminadas</div>
				  	<div class=" mx-auto"><a href="" type="button" class="btn btn-info"></a> En Proceso</div>				  	
				  	<div class=" mx-auto"><a href="" type="button" class="btn btn-secondary"></a> A Visitar</div>
				  	<div class=" mx-auto"><a href="" type="button" class="btn btn-danger"></a> Canceladas</div>
				</div>
			@endif

			<div id='calendar-container'>
			  <div id='calendar'></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-event-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      
      <div class="modal-body">      		
			<div class="card">				
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<h5 class="card-title">Visita:</h5>
						<span id="text-modal-etapa-visita" class="badge badge-info"></span>
						<span id="text-modal-fechas" class="badge badge-secondary"></span>
						<p class="card-text"><span id="text-modal-descripcion-visita"></span></p>
					</li>

					<li class="list-group-item">
						<h5 class="card-title"><span id="text-modal-nombre-proyecto"></span></h5>					    
					    <span id="text-modal-encargado" class="badge badge-secondary"></span>
					    <span id="text-modal-estado" class=""></span>
					    <span id="text-modal-clasificacion" class="badge badge-primary"></span>
					    <span id="text-modal-etapa-proyecto" class="badge badge-info"></span>
					    <p class="card-text"><span id="text-modal-descripcion-proyecto"></span></p>
					</li>
				</ul>			  	
			</div>     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary mx-auto col-md-4" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function() {
		var Calendar = FullCalendar.Calendar;		
		
		var calendarEl = document.getElementById('calendar');//el elemento que contendra al calendario
		
		//preparar los eventos a mostrar en el calendario
		let datosVisitas = {!!$datosVisitas!!};
		let fechaFinControl = {!! $fechaFinControl !!};//contiene las fechas de finalizacion modificadas para que fullcalendar las muestre bien
		let eventosCalendario = [];
		let i = 0;			
		datosVisitas.forEach((datoVisita) => {
			let clases = [];			
			switch(datoVisita.etapa_visita){
				case "Terminada": clases = ['bg-success']; break; // terminado verde success
				case "En Proceso": clases = ['bg-info']; break; // en Proceso celeste info
				case "A Visitar": clases = ['bg-secondary']; break; // a visitar gris secondary    <>
				case "Cancelada": clases= ['bg-danger']; break; // Cancelado rojo danger
			}
			eventosCalendario.push({
				id: datoVisita.visita_id,
				title: datoVisita.nombre,
				start: datoVisita.fecha_inicio,
				end: fechaFinControl[i],
				classNames: clases,
				// informacion a mostrar al darle click al proyecto
				extendedProps: {
					nombre_proyecto: datoVisita.nombre,
					encargado: datoVisita.nombres + " " + datoVisita.apellidos,
					estado: datoVisita.estado,
					clasificacion: datoVisita.clasificacion,
					etapa_proyecto: datoVisita.etapa_proyecto,
					descripcion_proyecto: datoVisita.descripcion_proyecto,					
					fecha_fin: datoVisita.fecha_fin, //fecha fin real a mostrar
					etapa_visita: datoVisita.etapa_visita,
					descripcion_visita: datoVisita.descripcion
				}
			});
			i++;
		});

		var calendar = new Calendar(calendarEl, {
			locale: 'es',
			headerToolbar: {			  
			  left: 'title',
			  right: 'prev,next today'
			},
			editable: false,
			droppable: false, // this allows things to be dropped onto the calendar
			events: eventosCalendario,
			eventClick: function(eventClickInfo){				
				let evento = eventClickInfo.event;
				let texto = "";
				
				//visita
				$('#text-modal-etapa-visita').text(evento.extendedProps.etapa_visita);				
				texto = (evento.startStr == evento.extendedProps.fecha_fin) ? evento.startStr : "del " + evento.startStr + " al " + evento.extendedProps.fecha_fin;
				$('#text-modal-fechas').text(texto);
				$('#text-modal-descripcion-visita').text(evento.extendedProps.descripcion_visita);
				//proyectos
				$('#text-modal-nombre-proyecto').text("Proyecto: " + evento.extendedProps.nombre_proyecto);
				$('#text-modal-encargado').text("Encargado: " + evento.extendedProps.encargado);
				
				texto = (evento.extendedProps.estado) ? "Estado: Activo": "Estado Cancelado";
				$('#text-modal-estado').text(texto);
				texto = (evento.extendedProps.estado) ? "badge badge-success" : "badge badge-danger";
				$('#text-modal-estado').attr("class", texto);

				$('#text-modal-clasificacion').text("Clasificacíón: " + evento.extendedProps.clasificacion);
				$('#text-modal-etapa-proyecto').text("Etapa: " + evento.extendedProps.etapa_proyecto);
				//acordate de cambiar que la descripcion viene con html
				$('#text-modal-descripcion-proyecto').html(evento.extendedProps.descripcion_proyecto);
				$('#modal-event-data').modal('show');
			},			
		});

		calendar.render();
		
	});
	
</script>

@endsection