@extends('layouts.navbar')
@section('title', 'Personal')

@section('content')
<div class="col-md-12 mx-auto my-3">
	<div class="card">
	  	<div class="card-header">
	    	<h5 class="card-title  float-left">Personal Registrado</h5>
	    	<a class="btn btn-success float-right" href="{{ route('personal.create') }}" role="button">Registrar +</a>	
	  	</div>
	  	<div class="card-body">
			<table class="table responsive nowrap table-hover">
			  <thead>
			    <tr>
			      <th scope="col" width="65%">Integrantes</th>
			      <th scope="col">Acciones</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if($integrantes)
			  	@foreach($integrantes as $integrante)
				    <tr>
				    	<td  width="65%">{{$integrante->nombres}} {{$integrante->apellidos}}</td>
				      	<td>
				      		<div class="d-flex justify-content-center">
					      		<a class="btn btn-primary mx-2" href="{{route('personal.show', [$integrante->personal_id])}}" role="button">Ver Datos</a>
					      		<a class="btn btn-secondary mx-2" href="{{route('personal.edit', [$integrante->personal_id])}}" role="button">Modificar</a>
					      		<button id="btn-delete-personal-{{$integrante->personal_id}}" type="button" class="btn btn-danger mx-2" data-toggle="modal" data-target="#delete-personal-modal" onclick='pasar_ruta("btn-delete-personal-{{$integrante->personal_id}}")' data-ruta="{{route('personal.destroy', [$integrante->personal_id])}}">Eliminar</button>
				      		</div>
				      	</td>
				  	</tr>
			    @endforeach
			    @endif
			  </tbody>
			</table>			
		</div>
	</div>
</div>
@include('personal.partials.delete-personal')

<script type="text/javascript">	
	function pasar_ruta(id){		
		var ruta = document.getElementById(id).getAttribute('data-ruta');
		document.getElementById("btn-confirm-delete-personal").setAttribute('href', ruta);
	}
</script>

@endsection



