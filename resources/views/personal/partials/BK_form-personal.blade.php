@if($formulario_crear)
<!-- Crear -->
<form action="{{route('personal.store')}}" method="post">
  @csrf

  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Nombre</label>
        <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{old('nombres')}}">
    @if($errors->has('nombres'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('nombres') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Apellido</label>
        <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{old('apellidos')}}">
    @if($errors->has('apellidos'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('apellidos') }}</strong>
      </span>    
    @endif
      </div>
  </div>
  
  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Email</label>
        <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{old('email')}}">
    @if($errors->has('email'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('email') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Password</label>
        <div class="input-group mb-3">
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="{{!empty(old('password')) ? old('password'): 'cimat1234'}}">
        <div class="input-group-append">
        <button class="btn btn-primary" type="button" onclick="mostrarPassword()">Mostrar</button>
        </div>
        </div>
        @if($errors->has('password'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('password') }}</strong>
      </span>    
    @endif
      </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-6">
        <label for="inputEmail4">Username</label>
        <input type="text" class="form-control {{$errors->has('usuario') ? 'is-invalid' : ''}}" id="input-usuario" name="usuario" value="{{old('usuario')}}">
    @if($errors->has('usuario'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('usuario') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label>Número de contacto</label>
        <input type="text" class="form-control {{$errors->has('telefono') ? 'is-invalid' : ''}}" name="telefono" id="input-telefono" value="{{old('telefono')}}" >
        @if($errors->has('telefono'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('telefono') }}</strong>
        </span>    
      @endif
      </div>
  </div>

  <div class="form-row"> 
  <div class="form-group col-md-6">
  <label for="input-roles">Especialidades:</label>
    <select class="selectpicker" data-width="100%" title="Selección multiple" multiple id="input-roles" name="especialidades[]">
      @if($especialidades)
        @foreach($especialidades as $especialidad)
          <option value="{{$especialidad->id}}" {{collect(old('especialidades'))->contains($especialidad->id) ? 'selected="true"' : ''}}>{{$especialidad->name}}</option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>

    <div class="form-group col-md-6">
    <label for="input-roles">Roles:</label>
    <select class="selectpicker" data-width="100%" multiple title="Selección multiple" id="input-roles" name="roles[]">
      @if($roles)
        @foreach($roles as $rol)
          <option value="{{$rol->id}}" {{collect(old('roles'))->contains($rol->id) ? 'selected="true"' : ''}}>{{$rol->nombre}}</option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Registrar</button>
  <a class="btn btn-secondary" href="{{route('personal.home')}}" role="button">Cancelar</a>
</form>


@else

<!-- Actualizar -->
<form action="{{route('personal.update', [$integrante->id])}}" method="post">
  @csrf
  <div class="form-group">
    <label for="input-usuario">Nombre de usuario:</label>
    <input type="text" class="form-control {{$errors->has('usuario') ? 'is-invalid' : ''}}" id="input-usuario"  name="usuario" value="{{$usuario->name}}">
    @if($errors->has('usuario'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('usuario') }}</strong>
      </span>    
    @endif
  </div>

  <div class="form-group">
    <label for="input-email">Email:</label>
    <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{$usuario->email}}">
    @if($errors->has('email'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('email') }}</strong>
      </span>    
    @endif
  </div>

  <div class="form-group">
    <label for="input-password">Password</label>    
    <div class="form-row">
      <div class="input-group mb-2">        
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="" placeholder="digite la nueva contraseña, si la hay">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button" onclick="mostrarPassword()">mostrar</button>
        </div>        
      </div>
    </div>
    @if($errors->has('password'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('password') }}</strong>
      </span>    
    @endif
  </div>

  <div class="form-group">
    <label for="input-nombres">Nombres:</label>
    <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{$integrante->nombres}}">
    @if($errors->has('nombres'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('nombres') }}</strong>
      </span>    
    @endif
  </div>

  <div class="form-group">
    <label for="input-apellidos">Apellidos:</label>
    <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{$integrante->apellidos}}">
    @if($errors->has('apellidos'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('apellidos') }}</strong>
      </span>    
    @endif
  </div>

  <div class="form-group">
    <label for="input-roles">Especialidad:</label>
    <select multiple class="form-control" id="input-roles" name="especialidades[]">
      @if($especialidades)
        @foreach($especialidades as $esp)
          <option value="{{$esp->id}}"
            @if($personal_especialidades)
              @if($personal_especialidades->contains($esp->id) ? 'selected="true"' : '')
                selected="true"
              @endif
            @endif>
            {{$esp->name}}
          </option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
  </div>

  <div class="form-group">
    <label for="input-roles">Roles:</label>
    <select multiple class="form-control" id="input-roles" name="roles[]">
      @if($roles)
        @foreach($roles as $rol)
          <option value="{{$rol->id}}"
            @if($user_roles)
              @if($user_roles->contains($rol->id) ? 'selected="true"' : '')
                selected="true"
              @endif
            @endif>
            {{$rol->nombre}}
          </option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
  </div>
  <button type="submit" class="btn btn-primary" onClick="mascaraPassword()">Modificar</button>
  <a class="btn btn-secondary" href="{{route('personal.home')}}" role="button">Cancelar</a>
</form>
@endif