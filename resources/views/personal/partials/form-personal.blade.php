@if($formulario_crear)
<!-- Crear -->
<form action="{{route('personal.store')}}" method="post">
@else
<!-- Actualizar -->
<form action="{{route('personal.update', [$integrante->personal_id])}}" method="post">
@endif
  @csrf
  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Nombre</label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{old('nombres')}}">
        @else
        <input type="text" class="form-control {{$errors->has('nombres') ? 'is-invalid' : ''}}" id="input-nombres"  name="nombres" value="{{$integrante->nombres}}">
        @endif
    @if($errors->has('nombres'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('nombres') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Apellido</label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{old('apellidos')}}">
        @else
        <input type="text" class="form-control {{$errors->has('apellidos') ? 'is-invalid' : ''}}" id="input-apellidos" name="apellidos" value="{{$integrante->apellidos}}">
        @endif
    @if($errors->has('apellidos'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('apellidos') }}</strong>
      </span>    
    @endif
      </div>
  </div>
  
  <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">Email</label>
        @if($formulario_crear)
        <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{old('email')}}">
        @else
        <input type="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="input-email" name="email" value="{{$usuario->email}}">
        @endif
    @if($errors->has('email'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('email') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label for="inputPassword4">Password</label>
        <div class="input-group mb-3">
        @if($formulario_crear)
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="{{!empty(old('password')) ? old('password'): 'cimat1234'}}">
        @else
        <input type="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" id="input-password" name="password" value="" placeholder="(Opcional) modificar contraseña">
        @endif
        <div class="input-group-append">
        <button class="btn btn-primary" type="button" onclick="mostrarPassword()">Mostrar</button>
        </div>
        </div>
        @if($errors->has('password'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('password') }}</strong>
      </span>    
    @endif
      </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-6">
        <label for="inputEmail4">Username</label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('usuario') ? 'is-invalid' : ''}}" id="input-usuario" name="usuario" value="{{old('usuario')}}">
        @else
        <input type="text" class="form-control {{$errors->has('usuario') ? 'is-invalid' : ''}}" id="input-usuario"  name="usuario" value="{{$usuario->name}}">
        @endif
    @if($errors->has('usuario'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('usuario') }}</strong>
      </span>    
    @endif
      </div>

      <div class="form-group col-md-6">
        <label>Número de contacto</label>
        @if($formulario_crear)
        <input type="text" class="form-control {{$errors->has('telefono') ? 'is-invalid' : ''}}" name="telefono" id="input-telefono" value="{{old('telefono')}}" >
        @else
        <input type="text" class="form-control {{$errors->has('usuario') ? 'is-invalid' : ''}}" id="input-telefono"  name="telefono" value="{{$integrante->telefono}}">
        @endif
        @if($errors->has('telefono'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('telefono') }}</strong>
        </span>    
      @endif
      </div>
  </div>

  <div class="form-row"> 
  <div class="form-group col-md-6">
  <label for="input-roles">Especialidades:</label>
    <select class="selectpicker" id="specSelect" data-width="100%" title="Selección multiple" multiple id="input-roles" name="especialidades[]">
      @if($especialidades)
        @foreach($especialidades as $especialidad)
          <option value="{{$especialidad->especialidad_id}}" {{collect(old('especialidades'))->contains($especialidad->especialidad_id) ? 'selected="true"' : ''}}>{{$especialidad->nombre}}</option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>

    <div class="form-group col-md-6">
    <label for="input-roles">Roles:</label>
    <select class="selectpicker" id="rolSelect" data-width="100%" multiple title="Selección multiple" id="input-roles" name="roles[]">
      @if($roles)
        @foreach($roles as $rol)
          <option value="{{$rol->rol_id}}" >{{$rol->nombre}}</option>
        @endforeach
      @else
          <option value=""></option>
      @endif
    </select>
    </div>
  </div>

  @if($formulario_crear)
  <button type="submit" class="btn btn-primary">Registrar</button>
  @else
  <button type="submit" class="btn btn-primary" onClick="mascaraPassword()">Modificar</button>
  @endif
  <a class="btn btn-secondary" href="{{route('personal.home')}}" role="button">Cancelar</a>
</form>

@if($formulario_crear)
@else
<script type="text/javascript">
  var especialidades = [];
  @foreach($personal_especialidades as $p)
  especialidades.push({{$p->especialidad_id}});
  @endforeach
  var roles = [];
  @foreach($user_roles as $p)
  roles.push({{$p->rol_id}});
  @endforeach
  $('#specSelect').selectpicker('val', especialidades);
  $('#rolSelect').selectpicker('val', roles);
</script>
@endif
