<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Anotaciones</title>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <style>
        .anyClass {
            height: 450px;
            overflow-y: scroll;
            display: flex;
            flex-direction: column-reverse;
        }

        .swal2-popup {
            font-size: 0.6rem !important;
        }
    </style>
</head>
@extends('layouts.navbarproyecto')
@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')
@endsection
@section('content')

<body>
    <div id="app">
        <div class="container">
            <div class="jumbotron bg-transparent p-4">
                <div class="d-flex justify-content-between">
                    <h3 class="ml-2">@{{id_proyecto}}</h3>

                </div>
                <div class="d-flex flex-row-reverse">
                    <div class="ml-2">
                        <button @click="agregar" type="button" class="btn btn-sm btn-primary">Agregar</button>
                    </div>
                    <div>
                        <form enctype="multipart/form-data" novalidate>
                            <div class="custom-file py-0 my-0">
                                <input type="file" accept="image/*" @change="onFileChange" :name="uploadFieldName" class="custom-file-input" lang="es">
                                <label class="custom-file-label" for="customFileLang">@{{nombreArchivo}}</label>
                            </div>
                        </form>
                    </div>
                    <button v-show="!!nombreArchivo" @click="nombreArchivo = ''" type="button" class="btn btn-sm text-white mr-2" style="background-color: #D32F2F;"><i class="fas fa-trash"></i></button>
                </div>

                <div id="progress" class="progress mt-2" v-show="showProgress">
                    <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>

                <ul class="list-unstyled my-4 anyClass">
                    <template v-for="item in historial">

                        <!--DERECHA-->
                        <template v-if="item.id_usuario == id_usuario">
                            <li class="media border-bottom border-gray">
                                <div class="media-body col-md-10 ml-auto">
                                    <div class="d-flex mt-2">
                                        <strong class="d-block text-gray-dark">@{{ item.autor }}</strong>
                                        <p class="text-muted mb-0 ml-1">- @{{ item.fecha }}</p>
                                        <p class="text-muted mb-0 ml-2" v-if="item.editado">(Editado)</p>
                                    </div>
                                    <template v-if="item.url">
                                        <div class="d-flex flex-row-reverse my-2 ">
                                            <img :src=item.contenido class="img-fluid w-50" alt="Imagen del proyecto">
                                        </div>
                                        <div class="d-flex flex-row-reverse my-2 ">
                                            <button type="button" class="btn btn-sm text-white" @click="eliminar(item)" style="background-color: #D32F2F;"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </template>
                                    <template v-else>
                                        @{{item.contenido}}
                                        <div class="d-flex flex-row-reverse my-2 ">
                                            <button type="button" class="btn btn-sm text-white ml-1" @click="editar(item)" style="background-color: #0D47A1;"><i class="fas fa-edit"></i></button>
                                            <button type="button" class="btn btn-sm text-white" @click="eliminar(item)" style="background-color: #D32F2F;"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </template>
                                </div>
                            </li>
                        </template>

                        <!--IZQUIERDA -->
                        <template v-else>
                            <li class="media border-bottom border-gray">
                                <div class="media-body col-md-9 mb-2 ">
                                    <div class="d-flex mt-2 my-2 ">
                                        <strong class="d-block text-gray-dark">@{{ item.autor }}</strong>
                                        <p class="text-muted mb-0 ml-2">@{{ item.fecha }}</p>
                                        <p class="text-muted mb-0 ml-2" v-if="item.editado">(Editado)</p>
                                    </div>
                                    <template v-if="item.url">
                                        <img :src=item.contenido class="img-fluid w-50" alt="Imagen del proyecto">
                                    </template>
                                    <template v-else>
                                        @{{item.contenido}}
                                    </template>
                                </div>
                            </li>
                        </template>
                    </template>
                </ul>
            </div>
        </div>

        <!-- Agregar - Editar anotacion -->
        <div class="modal fade show" tabindex="-1" :style="{display:block}" style="padding-right: 17px; padding-top:50px;background-color: rgba(189, 189, 189, 0.7);">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@{{title}}</h5>
                        <button type="button" class="close" @click="cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea v-model="areaTexto" :class="{'is-invalid': hasError }" class="form-control" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button @click="cerrar" type="button" class="btn btn-secondary">Cerrar</button>
                        <button @click="operacion" type="button" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Eliminar anotacion -->
        <div class="modal fade show" tabindex="-1" :style="{display:block2}" style="padding-right: 17px; padding-top:50px;background-color: rgba(189, 189, 189, 0.7);">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header border-bottom border-0">
                        <h5 class="modal-title">@{{title}}</h5>
                        <button type="button" class="close" @click="cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer border-bottom border-0">
                        <button @click="eliminarComentario" type="button" class="btn btn-danger">Confirmar</button>
                        <button @click="cerrar" type="button" class="btn btn-secondary">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>

<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2')
</script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            id_proyecto: "{{$id_proyecto}}",
            id_usuario: "{{$id_usuario}}",
            indice: -1,
            id_anotacion: -1,
            title: '',
            block: 'none',
            block2: 'none',
            historial: [],
            areaTexto: '',
            hasError: false,
            mensaje: '',
            uploadFieldName: 'image',
            uploadedFiles: [],
            nombreArchivo: '',
            showProgress: false,
        },
        created() {
            this.cargarDatos()
        },
        methods: {
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                this.nombreArchivo = files[0].name
                this.uploadedFiles = files[0]
            },
            subirArchivo() {
                let bodyData = new FormData();
                bodyData.append('operacion', 4);
                bodyData.append('proyecto', this.id_proyecto)
                bodyData.append('usuario', this.id_usuario)
                bodyData.append('file[]', this.uploadedFiles, this.uploadedFiles.name)
                axios.post("/proyecto/anotacion", bodyData, {
                        onUploadProgress: uploadEvent => {
                            let progress = Math.round(uploadEvent.loaded /
                                uploadEvent.total * 100);
                            console.log(progress)
                        }
                    })
                    .then(res => {
                        console.log(res)
                        this.notificar(res.data.mensaje, 'success')
                        this.nombreArchivo = ""
                        this.cargarDatos()
                    }).catch(function(error) {
                        console.log(error);
                        this.notificar(res.data.mensaje, 'error')
                    }).then(function() {

                    });
            },
            cargarDatos() {
                axios.get('/proyecto/anotacion', {
                        params: {
                            id: this.id_proyecto,
                        },
                    })
                    .then((response) => {
                        console.log(response)
                        this.historial = response.data.data
                        this.historial.reverse()
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            },
            agregar() {
                if (this.nombreArchivo) {
                    this.subirArchivo()
                    return
                }
                this.title = "Agregar comentario"
                this.block = 'block'
                this.indice = -1
            },
            editar(i) {
                this.title = "Editar comentario"
                this.block = 'block'
                this.indice = this.historial.indexOf(i)
                this.areaTexto = this.historial[this.indice].contenido
                this.id_anotacion = this.historial[this.indice].anotacion_id
            },
            eliminar(i) {
                this.title = "Eliminar comentario"
                this.block2 = 'block'
                this.indice = this.historial.indexOf(i)
                this.id_anotacion = this.historial[this.indice].anotacion_id
            },
            cerrar() {
                this.block = 'none'
                this.block2 = 'none'
                this.areaTexto = ''
                this.hasError = false
                this.indice = -1
            },
            operacion() {
                if (!this.areaTexto) {
                    this.hasError = true
                    return
                }
                this.hasError = false
                if (this.indice == -1) {
                    axios.post('/proyecto/anotacion', {
                            operacion: 0,
                            proyecto: this.id_proyecto,
                            usuario: this.id_usuario,
                            contenido: this.areaTexto,
                        })
                        .then(response => {
                            this.block = 'none'
                            this.areaTexto = ''
                            this.cargarDatos()
                            this.notificar(response.data.mensaje, 'success')
                        })
                        .catch(error => {
                            console.log(error);
                        });
                } else {
                    axios.post('/proyecto/anotacion', {
                            operacion: 1,
                            id_anotacion: this.id_anotacion,
                            contenido: this.areaTexto,
                        })
                        .then(response => {
                            this.indice = -1
                            this.block = 'none'
                            this.areaTexto = ''
                            this.cargarDatos()
                            this.notificar(response.data.mensaje, 'success')
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }
            },
            eliminarComentario() {
                axios.post('/proyecto/anotacion', {
                        operacion: 2,
                        id_anotacion: this.id_anotacion,
                    })
                    .then(response => {
                        this.indice = -1
                        this.id_anotacion = -1
                        this.block2 = 'none'
                        this.cargarDatos()
                        this.notificar(response.data.mensaje, 'success')
                    })
                    .catch(error => {
                        console.log(error);
                    });
            },
            notificar(mensaje, icon) {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: mensaje,
                    showConfirmButton: false,
                    timer: 1500,
                    width: '300px',
                })
            }
        },
    });
</script>
@endsection

</html>