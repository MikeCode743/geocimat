<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Anotaciones</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <style>
        body {
            min-height: 100vh;
        }

        .swal2-popup {
            font-size: 0.6rem !important;
        }

        ::-webkit-scrollbar {
            width: 5px;
        }

        ::-webkit-scrollbar-track {
            width: 5px;
            background: #f5f5f5;
        }

        ::-webkit-scrollbar-thumb {
            width: 1em;
            background-color: #ddd;
            outline: 1px solid slategrey;
            border-radius: 1rem;
        }

        .text-small {
            font-size: 0.9rem;
        }

        .messages-box,
        .chat-box {
            height: calc(85vh - 95px);
            overflow-y: scroll;
        }

        .chat-box {
            display: flex;
            flex-direction: column-reverse;
        }

        .rounded-lg {
            border-radius: 0.5rem;
        }

        input::placeholder {
            font-size: 0.9rem;
            color: #999;
        }

        .pointer {
            cursor: pointer;
        }

        .img-fluid {
            cursor: zoom-in;
        }
    </style>
</head>
@extends('layouts.navbarproyecto')
@section('librerias')
@include('proyecto.parcial.librerias.css')
@include('proyecto.parcial.librerias.script')
@endsection
@section('content')

<body>
    <div id="app" class="container py-2 px-0">
        <div class="row rounded-lg overflow-hidden shadow">
            <!-- Users box-->
            <div class="col-4 px-0">
                <div class="bg-white">
                    <div class="bg-gray px-4 py-2 bg-light">
                        <input type="text" v-model.trim="inputFiltro" class="form-control" placeholder="Buscar proyecto">
                    </div>
                    <div class="messages-box">
                        <div class="list-group rounded-0">
                            <template v-for="(item,index) in proyectosFiltrados">
                                <div :key="index" class="list-group-item list-group-item-action rounded-0" v-bind:class="[item.active ? 'bg-primary text-white' : 'list-group-item-light']" @click="setActivo(index)">
                                    <div class="media">
                                        <div class="media-body ml-4">
                                            <div class="d-flex align-items-center justify-content-between mb-1">
                                                <h6 class="mb-0">@{{item.name}}</h6>
                                                <small class="small font-weight-bold">@{{item.fecha_inicio}}</small>
                                            </div>
                                            <p class="font-italic mb-0 text-small" v-bind:class="[item.active ? 'bg-primary text-white' : ' text-muted']">
                                                @{{item.nombre}}</p>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Chat Box-->
            <div class="col-8 px-0">
                <div class="px-4 py-5 chat-box bg-white">
                    <template v-for="mensaje in mensajeProyecto">
                        <template v-if="mensaje.propietario">
                            <!-- Reciever Message-->
                            <div class="media w-50 ml-auto mb-3">
                                <div class="media-body">
                                    <template v-if="mensaje.url">
                                        <img @click="mostrarImagenTab(mensaje.contenido)" :src=mensaje.contenido class="img-fluid w-100 mb-2" alt="Imagen del proyecto">
                                    </template>
                                    <template v-else>
                                        <div class="bg-primary rounded py-2 px-3 mb-2">
                                            <p class="text-small mb-0 text-white">@{{mensaje.contenido}}</p>
                                        </div>
                                    </template>
                                    <p class="small text-muted">@{{mensaje.autor}} @{{mensaje.fecha}}
                                        <svg style="margin-bottom:5px" @click="eliminarMensaje(mensaje)" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class=" pointer bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </p>
                                </div>
                            </div>
                        </template>
                        <template v-else>
                            <!-- Sender Message-->
                            <div class="media w-50 mb-3">
                                <div class="media-body ml-3">
                                    <template v-if="mensaje.url">
                                        <img @click="mostrarImagenTab(mensaje.contenido)" :src=mensaje.contenido class="img-fluid w-100 mb-2" alt="Imagen del proyecto">
                                    </template>
                                    <template v-else>
                                        <div class="bg-primary rounded py-2 px-3 mb-2">
                                            <p class="text-small mb-0 text-white">@{{mensaje.contenido}}</p>
                                        </div>
                                    </template>
                                    <p class="pl-2 font-weight-bold small text-muted">@{{mensaje.autor}}
                                        @{{mensaje.fecha}}</p>
                                </div>
                            </div>
                        </template>
                    </template>
                </div>

                <!-- Typing area -->
                <input type="file" name="file" accept="image/*" @change="guardarImagen" ref="file" :style="{display: 'none'}">

                <div class="d-flex justify-content-between" v-if="proyectoFocus !== null">
                    <button for="file" @click="$refs.file.click()" class="btn btn-light">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-paperclip" viewBox="0 0 16 16">
                            <path d="M4.5 3a2.5 2.5 0 0 1 5 0v9a1.5 1.5 0 0 1-3 0V5a.5.5 0 0 1 1 0v7a.5.5 0 0 0 1 0V3a1.5 1.5 0 1 0-3 0v9a2.5 2.5 0 0 0 5 0V5a.5.5 0 0 1 1 0v7a3.5 3.5 0 1 1-7 0V3z" />
                        </svg>
                    </button>
                    <input v-if="!progressImage.show" name="mensaje" @keypress.enter="guardarMensaje" v-model.trim="inputMensaje" type="text" placeholder="Aa" class="form-control rounded-0 border-0 py-4 bg-muted">
                </div>
                <div class="progress" style="height: 5px;" v-if="progressImage.show">
                    <div class="progress-bar progress-bar-striped" role="progressbar" :style="{width:progressImage.value+'%'}" :aria-valuenow="progressImage.value" aria-valuemin="0" aria-valuemax="100"></div>
                </div>

            </div>
        </div>
    </div>
</body>

<script src="https://unpkg.com/axios/dist/axios.min.js">
    const axios = require('axios');
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    const Swal = require('sweetalert2')
</script>
<script>
    new Vue({
        el: '#app',
        data: {
            contenedorProyectos: [],
            mensajeProyecto: [],
            proyectoFocus: null,
            inputMensaje: '',
            inputFiltro: '',
            progressImage: {
                show: false,
                value: 0
            }
        },
        created() {
            this.obtenerListadoProyectos()
        },
        computed: {
            proyectosFiltrados() {
                const searchRegex = new RegExp(this.inputFiltro, 'i')
                return this.contenedorProyectos.filter(proyecto => {
                    const {
                        name: propietario,
                        nombre: nombreProyecto
                    } = proyecto
                    return searchRegex.test(propietario) || searchRegex.test(nombreProyecto)
                })
            }
        },
        methods: {
            mostrarImagenTab(url) {
                window.open(url, "_blank")
            },
            obtenerListadoProyectos() {
                axios.get('/chat/proyectos')
                    .then((response) => {
                        //console.log(response)
                        this.contenedorProyectos = response.data.reverse()
                    })
                    .catch((error) => {
                        this.contenedorProyectos = []
                        this.notificarUsuario('Error en la solicitud', 'error')
                        console.log(error);
                    })
                this.contenedorProyectos.forEach((element) => {
                    Object.assign(element, {
                        active: false
                    })
                });
            },
            setActivo(index) {
                this.contenedorProyectos.forEach(element => {
                    Object.assign(element, {
                        active: false
                    })
                })
                this.proyectosFiltrados.forEach((element, i) => {
                    if (index === i) {
                        Object.assign(element, {
                            active: true
                        })
                        this.proyectoFocus = element
                    }
                });
                this.contenedorProyectos = [...this.contenedorProyectos]
                this.cargarMensajesProyecto()
            },
            cargarMensajesProyecto() {
                axios.get('/proyecto/anotacion', {
                        params: {
                            id: this.proyectoFocus.proyecto_id,
                        },
                    })
                    .then((response) => {
                        //console.log(response)
                        this.mensajeProyecto = response.data.data.reverse()
                    })
                    .catch((error) => {
                        console.log(error);
                        this.notificarUsuario('Error en la solicitud', 'error')
                    })
            },
            guardarMensaje() {
                if (this.inputMensaje) {
                    axios.post('/proyecto/anotacion', {
                            operacion: 0,
                            proyecto: this.proyectoFocus.proyecto_id,
                            contenido: this.inputMensaje,
                        })
                        .then(response => {
                            //console.log(response)
                            this.inputMensaje = ''
                            this.cargarMensajesProyecto()
                            this.notificarUsuario(response.data.mensaje, 'success')
                        })
                        .catch(error => {
                            console.log(error);
                            this.notificarUsuario('Error en la solicitud', 'error')
                        });
                }
            },
            eliminarMensaje(target) {
                axios.post('/proyecto/anotacion', {
                        operacion: 2,
                        id_anotacion: target.anotacion_id,
                    })
                    .then(response => {
                        //console.log(response)
                        this.cargarMensajesProyecto()
                        this.notificarUsuario(response.data.mensaje, 'success')
                    })
                    .catch(error => {
                        console.log(error);
                        this.notificarUsuario('Error en la solicitud', 'error')
                    });
            },
            guardarImagen(event) {
                console.log(event.target.files)
                let bodyData = new FormData();
                bodyData.append('operacion', 4);
                bodyData.append('proyecto', this.proyectoFocus.proyecto_id)
                bodyData.append('file[]', event.target.files[0], event.target.files[0].name)
                axios.post("/proyecto/anotacion", bodyData, {
                        onUploadProgress: uploadEvent => {
                            let progress = Math.round(uploadEvent.loaded /
                                uploadEvent.total * 100);
                            this.progressImage = {
                                ...this.progressImage,
                                show: true,
                                value: progress
                            }
                            console.log(progress)
                        }
                    })
                    .then(response => {
                        console.log(response)
                        this.notificarUsuario(response.data.mensaje, 'success')
                        this.cargarMensajesProyecto()
                        this.progressImage = {
                            ...this.progressImage,
                            show: false,
                            value: 0
                        }
                    }).catch(function(error) {
                        console.log(error)
                        this.notificarUsuario('Error en la solicitud', 'error')
                    });
            },
            notificarUsuario(mensaje, icon) {
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: mensaje,
                    showConfirmButton: false,
                    timer: 1500,
                    width: '300px',
                })
            }
        },
    })
</script>
@endsection

</html>